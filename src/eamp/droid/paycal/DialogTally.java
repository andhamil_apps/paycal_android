/*
 * DialogTally
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

public class DialogTally extends Dialog
{
	private int mnTallyID;
	private int mnTallyCount;
	private int mnTallyEvery;
	private int mnTallyOn;

	private String mstrTallyFromDate;

	private Context mContext;

	private SharedPreferences mPreferences;
	private SharedPreferences.Editor mPrefEditor;

	public DialogTally(Context context)
	{
		super(context);
		mContext = context;

		mPreferences = mContext.getSharedPreferences(Konstant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		mPrefEditor = mPreferences.edit();

		this.setContentView(R.layout.dialog_tally);
		this.setTitle(R.string.add_tally);
	}

	public void displayDialog(final boolean bEditMode)
	{
		final DatePicker dpFromDate = (DatePicker) findViewById(R.id.dp_tally_from_date);

		final EditText etCount = (EditText) findViewById(R.id.et_tally_every_count);

		final Spinner spnEvery = (Spinner) findViewById(R.id.spn_tally_every);

		final RelativeLayout rlWeeksOn = (RelativeLayout) findViewById(R.id.rl_tally_weeks_on);
		final Spinner spnWeeksOn = (Spinner) findViewById(R.id.spn_tally_weeks_on);

		final RelativeLayout rlMonthsOn = (RelativeLayout) findViewById(R.id.rl_tally_months_on);
		final Spinner spnMonthsOn = (Spinner) findViewById(R.id.spn_tally_months_on);

		final Button btnSave = (Button) findViewById(R.id.btn_tally_save);
		btnSave.setEnabled(false);

		etCount.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
			}

			@Override
			public void afterTextChanged(Editable s)
			{
				try
				{
					int nCount = Integer.parseInt(s.toString());
					if (nCount > 0)
						btnSave.setEnabled(true);
					else
						btnSave.setEnabled(false);
				}
				catch (NumberFormatException e)
				{

				}
			}
		});

		spnEvery.setOnItemSelectedListener(new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3)
			{
				switch (arg2)
				{
				case Konstant.TALLY_EVERY_WEEKS:
					rlWeeksOn.setVisibility(View.VISIBLE);
					rlMonthsOn.setVisibility(View.GONE);
					break;
				case Konstant.TALLY_EVERY_MONTHS:
					rlMonthsOn.setVisibility(View.VISIBLE);
					rlWeeksOn.setVisibility(View.GONE);
					break;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0)
			{
			}
		});

		if (bEditMode)
		{
			this.setTitle(R.string.edit_tally);

			int nYear = Integer.parseInt(mstrTallyFromDate.substring(0, mstrTallyFromDate.indexOf("-")));
			int nMOY = Integer.parseInt(mstrTallyFromDate.substring(mstrTallyFromDate.indexOf("-") + 1, mstrTallyFromDate.lastIndexOf("-")));
			int nDOM = Integer.parseInt(mstrTallyFromDate.substring(mstrTallyFromDate.lastIndexOf("-") + 1));
			dpFromDate.updateDate(nYear, nMOY, nDOM);

			etCount.setText(Integer.toString(mnTallyCount));

			spnEvery.setSelection(mnTallyEvery);

			switch (mnTallyEvery)
			{
			case Konstant.TALLY_EVERY_WEEKS:
				rlWeeksOn.setVisibility(View.VISIBLE);
				rlMonthsOn.setVisibility(View.GONE);
				spnWeeksOn.setSelection(mnTallyOn);
				break;
			case Konstant.TALLY_EVERY_MONTHS:
				rlMonthsOn.setVisibility(View.VISIBLE);
				rlWeeksOn.setVisibility(View.GONE);
				spnMonthsOn.setSelection(mnTallyOn);
				break;
			}
		}
		else
		{
			etCount.setText(Integer.toString(0));
			spnEvery.setSelection(Konstant.TALLY_EVERY_WEEKS);
			rlWeeksOn.setVisibility(View.VISIBLE);
			rlMonthsOn.setVisibility(View.GONE);
			spnWeeksOn.setSelection(0);
		}

		btnSave.setOnClickListener(new android.view.View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				int nYear = dpFromDate.getYear();
				int nMOY = dpFromDate.getMonth();
				int nDOM = dpFromDate.getDayOfMonth();
				String strFromDate = nYear + "-" + nMOY + "-" + nDOM;

				int nCount = Integer.parseInt(etCount.getText().toString());

				int nEvery = spnEvery.getSelectedItemPosition();

				int nOn = 0;
				switch (nEvery)
				{
				case Konstant.TALLY_EVERY_WEEKS:
					nOn = spnWeeksOn.getSelectedItemPosition();
					break;
				case Konstant.TALLY_EVERY_MONTHS:
					nOn = spnMonthsOn.getSelectedItemPosition();
					break;
				}

				if (bEditMode)
				{
					if (DBHelper.getInstance(mContext).updateTally(mnTallyID, strFromDate, nCount, nEvery, nOn))
					{
						ActivitySettings.notifyTalliesChanged();
					}
					else
					{
						Toast.makeText(mContext, mContext.getString(R.string.msg_tally_edit_fail), Toast.LENGTH_SHORT).show();
					}
				}
				else
				{
					if (DBHelper.getInstance(mContext).addTally(strFromDate, nCount, nEvery, nOn))
					{
						mnTallyID = DBHelper.getInstance(mContext).getMaxTallyID();
						ActivitySettings.notifyTalliesChanged();
					}
					else
					{
						Toast.makeText(mContext, mContext.getString(R.string.msg_tally_add_fail), Toast.LENGTH_SHORT).show();
					}
				}

				Integer tallyCalciParams[] = { nYear, nMOY, nDOM, nCount, nEvery, nOn };
				TallyDatesCalculator tallyCalci = new TallyDatesCalculator();
				tallyCalci.execute(tallyCalciParams);

				dismiss();
			}
		});

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		getWindow().setAttributes(lp);
		show();
	}

	public void setTallyDetails(int nID, String strFromDate, int nCount, int nEvery, int nOn)
	{
		mnTallyID = nID;

		mstrTallyFromDate = strFromDate;
		mnTallyCount = nCount;
		mnTallyEvery = nEvery;
		mnTallyOn = nOn;
	}

	private class TallyDatesCalculator extends AsyncTask<Integer, Void, Void>
	{
		TallyDatesCalculator()
		{
		}

		@Override
		protected void onPreExecute()
		{
		}

		@Override
		protected Void doInBackground(Integer... params)
		{
			int nFromYear = params[0];
			int nFromMOY = params[1];
			int nFromDOM = params[2];
			int nEveryCount = params[3];
			int nTimeUnit = params[4];
			int nOnDay = params[5];

			ArrayList<String> alsTallyDates = null;

			// Delete old Piggy dates

			mPrefEditor.putString(Konstant.PREF_KEY_TALLY_DATES_FOR + mnTallyID, "");
			mPrefEditor.commit();

			if (nTimeUnit == Konstant.TALLY_EVERY_WEEKS)
				alsTallyDates = Utils.calculateTallyDatesUsingWeeks(nFromYear, nFromMOY, nFromDOM, nEveryCount, (nOnDay + 1));
			else
				alsTallyDates = Utils.calculateTallyDatesUsingMonths(nFromYear, nFromMOY, nFromDOM, nEveryCount, nOnDay);

			// Save new Piggy dates

			mPrefEditor.putString(Konstant.PREF_KEY_TALLY_DATES_FOR + mnTallyID, TextUtils.join(",", alsTallyDates));
			mPrefEditor.commit();

			return null;
		}

		@Override
		protected void onProgressUpdate(Void... params)
		{
		}

		@Override
		protected void onPostExecute(Void param)
		{
			Utils.msaTallyDates = Utils.getTallyDatesMappedByTallyID(mContext, mPreferences);
		}
	}
}