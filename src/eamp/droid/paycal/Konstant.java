/*
 * Konstant
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

public interface Konstant
{
	public static final String APPLICATION_SKU_FOR_PREMIUM = "premium_paycal_app";
	public static final String APPLICATION_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxWwEwo+4FlYEH+MJLY4lppAlSojNQkV+ODZ9j4w3P1A4ncHJbvJTn55LXkpLAhdGoG7hvwymBfYoVWEhJstFQ28M6eBN40UWJoiNgQaT8i5bQKr9n5u3EwycGn1bbqBUVMTjGc/YpkcAbxg2qZRVmaiE9JmrU6SN5vNLKAmKXKhPLHlWkSId5kFw/cHaK2UBeM7wE8gyd2zoGWizS2wvm8G7aRUeyCpsmXOlyjeDvrbBwNglfvmpWTz3yP1PbBDoLW88rUN8uECkIeW+TcvKIx0hhz5k/SgW4i1EKjf4pbk6nCxbnDWLA/LrrIb6Gs5lzvX8sTfFmqhHVh/KTFRD5QIDAQAB";

	public static final String[] MONTHS = { "January", "Febrauary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

	public static final String[] REMAINDERS = { "0 minutes", "1 minute", "5 minutes", "10 minutes", "15 minutes", "20 minutes", "25 minutes", "30 minutes", "45 minutes", "1 hour", "2 hours",
			"3 hours", "12 hours", "24 hours", "2 days", "1 week" };

	public static final String[] TALLY_EVERY = { "Week(s)", "Month(s)" };

	public static final String[] TALLY_WEEKS_ON = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };

	public static final String[] TALLY_MONTHS_ON = { "First day", "Middle of the month", "Last day" };

	public static final int JANUARY = 0;
	public static final int FEBRAURY = 1;
	public static final int MARCH = 2;
	public static final int APRIL = 3;
	public static final int MAY = 4;
	public static final int JUNE = 5;
	public static final int JULY = 6;
	public static final int AUGUST = 7;
	public static final int SEPTEMBER = 8;
	public static final int OCTOBER = 9;
	public static final int NOVEMBER = 10;
	public static final int DECEMBER = 11;

	public static final int ACTIVITY_SPLASH = 0;
	public static final int ACTIVITY_TUTORIAL = 1;
	public static final int ACTIVITY_HOME = 2;
	public static final int ACTIVITY_SETTINGS = 3;
	
	public static final int TIME_AM = 0;
	public static final int TIME_PM = 1;

	public static final int COLOR_PURPLE = 0;
	public static final int COLOR_CYAN = 1;
	public static final int COLOR_GREEN = 2;
	public static final int COLOR_MUSTARD = 3;
	public static final int COLOR_ORANGE = 4;
	public static final int COLOR_RED = 5;

	public static final int JOB_PAY_UNIT_HOUR = 0;
	public static final int JOB_PAY_UNIT_SHIFT = 1;

	public static final int JOB_PAY_LENGTH_WEEKLY = 0;
	public static final int JOB_PAY_LENGTH_BIWEEKLY = 1;
	public static final int JOB_PAY_LENGTH_QUADWEEKLY = 2;
	public static final int JOB_PAY_LENGTH_MONTHLY = 3;

	public static final int JOB_OVERTIME_UNIT_DAY = 0;
	public static final int JOB_OVERTIME_UNIT_WEEK = 1;

	public static final int DDT_SHIFT = 0;
	public static final int DDT_PIGGY = 1;
	public static final int DDT_TALLY = 2;

	public static final int TALLY_EVERY_WEEKS = 0;
	public static final int TALLY_EVERY_MONTHS = 1;
	
	public static final int DISPLAY_SELECTED_MONTH = 0;
	public static final int DISPLAY_PREVIOUS_MONTH = 1;
	public static final int DISPLAY_NEXT_MONTH = 2;

	// Default Values on the UI

	public static final String DEFAULT_JOB_NAME = "Work";
	public static final int DEFAULT_JOB_COLOR = COLOR_GREEN;
	public static final double DEFAULT_JOB_PAY = 20;
	public static final int DEFAULT_JOB_PAY_UNIT = JOB_PAY_UNIT_HOUR;
	public static final int DEFAULT_JOB_PAY_BEGAN_YEAR = 2013;
	public static final int DEFAULT_JOB_PAY_BEGAN_MOY = 7;
	public static final int DEFAULT_JOB_PAY_BEGAN_DOM = 5;
	public static final int DEFAULT_JOB_PAY_LENGTH = JOB_PAY_LENGTH_WEEKLY;
	public static final int DEFAULT_JOB_PAY_DAY_YEAR = 2013;
	public static final int DEFAULT_JOB_PAY_DAY_MOY = 7;
	public static final int DEFAULT_JOB_PAY_DAY_DOM = 16;
	public static final double DEFAULT_JOB_PAY_DEDUCTION = 0;
	public static final double DEFAULT_JOB_OVERTIME_RATE = 1;
	public static final int DEFAULT_JOB_OT_HRS = 8;
	public static final int DEFAULT_JOB_OT_MINS = 0;
	public static final int DEFAULT_JOB_OVERTIME_UNIT = JOB_OVERTIME_UNIT_DAY;
	public static final boolean DEFAULT_JOB_OVERTIME_ON = false;
	public static final int DEFAULT_NUMBER_OF_YEARS = 10;

	public static final int DEFAULT_SHIFT_TEMPLATE_COLOR = COLOR_GREEN;
	public static final int DEFAULT_SHIFT_TEMPLATE_ICON = COLOR_GREEN;

	public static final int DEFAULT_SHIFT_FROM_HOUR = 9;
	public static final int DEFAULT_SHIFT_FROM_MINUTE = 0;
	public static final int DEFAULT_SHIFT_TO_HOUR = 17;
	public static final int DEFAULT_SHIFT_TO_MINUTE = 0;
	public static final int DEFAULT_SHIFT_UNPAID_BREAK = 0;
	public static final double DEFAULT_SHIFT_SPECIAL_WAGE = 0;
	public static final double DEFAULT_SHIFT_TIP = 0;
	public static final String DEFAULT_SHIFT_NOTES = "";

	// Keys to exchange between Intents

	public static final String KEY_TUTORIAL_LAUNCH_FROM = "TUTORIAL_LAUNCH_FROM";
	
	public static final String KEY_SELECTED_JOB_ID = "SELECTED_JOB_ID";
	public static final String KEY_SELECTED_SHIFT_TEMPLATE_ID = "SELECTED_SHIFT_TEMPLATE_ID";
	public static final String KEY_SELECTED_SII_ID = "SELECTED_SII_ID";
	public static final String KEY_SELECTED_SIB_ID = "SELECTED_SIB_ID";
	public static final String KEY_SELECTED_DATE_YEAR = "SELECTED_DATE_YEAR";
	public static final String KEY_SELECTED_DATE_MOY = "SELECTED_DATE_MOY";
	public static final String KEY_SELECTED_DATE_DOM = "SELECTED_DATE_DOM";

	public static final String KEY_PAYDAY_DATE_YEAR = "PAYDAY_DATE_YEAR";
	public static final String KEY_PAYDAY_DATE_MOY = "PAYDAY_DATE_MOY";
	public static final String KEY_PAYDAY_DATE_DOM = "PAYDAY_DATE_DOM";
	public static final String KEY_PAYDAY_JOB_NAME = "PAYDAY_JOB_NAME";
	public static final String KEY_PAYDAY_DEDUCTION_RATE = "PAYDAY_DEDUCTION_RATE";
	public static final String KEY_PAYDAY_DEDUCTION = "PAYDAY_DEDUCTION";
	public static final String KEY_PAYDAY_TIPS = "PAYDAY_TIPS";
	public static final String KEY_PAYDAY_REGULAR_MINUTES = "REGULAR_MINUTES";
	public static final String KEY_PAYDAY_OVERTIME_MINUTES = "OVERTIME_MINUTES";
	public static final String KEY_PAYDAY_SPECIAL_MINUTES = "SPECIAL_MINUTES";
	public static final String KEY_PAYDAY_REGULAR_AMOUNT = "REGULAR_AMOUNT";
	public static final String KEY_PAYDAY_OVERTIME_AMOUNT = "OVERTIME_AMOUNT";
	public static final String KEY_PAYDAY_SPECIAL_AMOUNT = "SPECIAL_AMOUNT";

	// Shared Preferences Keys

	public static final String SHARED_PREFERENCES_FILE_NAME = "andhamil_paycal_pref_file";

	public static final String PREF_KEY_IS_APP_USER_PREMIUM = "IS_APP_USER_PREMIUM";

	public static final String PREF_KEY_IS_TUTORIAL_DONE = "IS_TUTORIAL_DONE";
	
	public static final String PREF_KEY_TALLY_ON = "TALLY_ON";
	// public static final String PREF_KEY_PASSCODE_ON = "PASSCODE_ON";
	public static final String PREF_KEY_MILITARY_TIME_ON = "MILITARY_TIME_ON";
	public static final String PREF_KEY_DEDUCTIONS_ON = "DEDUCTIONS_ON";
	public static final String PREF_KEY_TIPS_ON = "TIPS_ON";

	public static final String PREF_KEY_APP_LAUNCH_COUNT = "APP_LAUNCH_COUNT";

	public static final String PREF_KEY_CALENDAR_ID = "CALENDAR_ID";

	public static final String PREF_KEY_PASSCODE = "PASSCODE";

	public static final String PREF_KEY_PIGGY_DATES_FOR = "KEY_PIGGY_DATES_FOR_";
	public static final String PREF_KEY_TALLY_DATES_FOR = "KEY_TALLY_DATES_FOR_";

	// Shared Preferences Keys' Default Values

	public static final boolean PREF_DEF_IS_APP_USER_PREMIUM = false;
	
	public static final boolean PREF_DEF_IS_TUTORIAL_DONE = false;
	
	public static final boolean PREF_DEF_TALLY_ON = false;
	// public static final boolean PREF_DEF_PASSCODE_ON = false;
	public static final boolean PREF_DEF_MILITARY_TIME_ON = false;
	public static final boolean PREF_DEF_DEDUCTIONS_ON = true;
	public static final boolean PREF_DEF_TIPS_ON = true;

	public static final int PREF_DEF_APP_LAUNCH_COUNT = 0;

	public static final long PREF_DEF_CALENDAR_ID = -1;

	public static final String PREF_DEF_PASSCODE = "";
}
