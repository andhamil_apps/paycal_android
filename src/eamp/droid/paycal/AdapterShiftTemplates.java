/*
 * AdapterShiftTemplates
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.util.ArrayList;

import eamp.droid.paycal.model.ShiftTemplate;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterShiftTemplates extends ArrayAdapter<ShiftTemplate>
{
	private int mLayoutResourceId;

	private ArrayList<ShiftTemplate> malShiftTemplates;

	private ExistingShiftTemplate mListItemHolder;

	private Context mContext;
	private SharedPreferences mPreferences;

	public AdapterShiftTemplates(Context context, int layoutResourceId, SharedPreferences preferences, ArrayList<ShiftTemplate> alShiftTemplates)
	{
		super(context, layoutResourceId, alShiftTemplates);

		mContext = context;
		mLayoutResourceId = layoutResourceId;

		mPreferences = preferences;
		malShiftTemplates = alShiftTemplates;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		View row = convertView;
		if (row == null)
		{
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			row = inflater.inflate(mLayoutResourceId, parent, false);
			mListItemHolder = new ExistingShiftTemplate();
			mListItemHolder.ivIcon = (ImageView) row.findViewById(R.id.iv_lis_icon);
			mListItemHolder.tvFrom = (TextView) row.findViewById(R.id.tv_lis_from);
			mListItemHolder.tvTo = (TextView) row.findViewById(R.id.tv_lis_to);
			mListItemHolder.tvJobName = (TextView) row.findViewById(R.id.tv_lis_job_name);
			mListItemHolder.tvPaidHours = (TextView) row.findViewById(R.id.tv_lis_paid_hours);
			mListItemHolder.ivDelete = (ImageView) row.findViewById(R.id.iv_lis_delete);
			row.setTag(mListItemHolder);
		}
		else
		{
			mListItemHolder = (ExistingShiftTemplate) row.getTag();
		}

		mListItemHolder.ivDelete.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
				builder.setTitle(R.string.delete_shift_template);
				builder.setMessage(R.string.msg_prompt_shift_template_delete);
				builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int id)
					{
						DBHelper.getInstance(mContext).deleteShiftTemplate(malShiftTemplates.get(position).getID());
						DBHelper.getInstance(mContext).deleteShiftTemplateRemainders(malShiftTemplates.get(position).getID());

						malShiftTemplates.remove(position);

						notifyDataSetChanged();

						ActivityShiftTemplates.notifyShiftTemplatesChanged();
						ActivityHome.setShiftTrayChanged(true);
					}
				});
				builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int id)
					{
					}
				});
				builder.create();
				builder.show();
			}
		});

		row.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(mContext, ActivityShiftTemplatesAdd.class);
				intent.putExtra(Konstant.KEY_SELECTED_SHIFT_TEMPLATE_ID, malShiftTemplates.get(position).getID());
				mContext.startActivity(intent);
			}
		});

		int nColor = R.color.color_green;
		switch (malShiftTemplates.get(position).getColor())
		{
		case Konstant.COLOR_PURPLE:
			nColor = R.color.color_purple;
			break;
		case Konstant.COLOR_CYAN:
			nColor = R.color.color_cyan;
			break;
		case Konstant.COLOR_GREEN:
			nColor = R.color.color_green;
			break;
		case Konstant.COLOR_MUSTARD:
			nColor = R.color.color_mustard;
			break;
		case Konstant.COLOR_ORANGE:
			nColor = R.color.color_orange;
			break;
		case Konstant.COLOR_RED:
			nColor = R.color.color_red;
			break;
		}

		mListItemHolder.ivIcon.setBackgroundResource(nColor);
		mListItemHolder.ivIcon.setImageResource(Utils.getShiftTemplateIcon(malShiftTemplates.get(position).getIcon()));
		mListItemHolder.tvFrom.setText(Utils.constructTime(malShiftTemplates.get(position).getFromHour(), malShiftTemplates.get(position).getFromMinute(), mPreferences));
		mListItemHolder.tvTo.setText(Utils.constructTime(malShiftTemplates.get(position).getToHour(), malShiftTemplates.get(position).getToMinute(), mPreferences));
		mListItemHolder.tvJobName.setText(DBHelper.getInstance(mContext).getJob(malShiftTemplates.get(position).getJobID()).getName());
		mListItemHolder.tvPaidHours.setText(Utils.constructTimeInHrsAndMins(malShiftTemplates.get(position).getPaidMinutes()));
		mListItemHolder.ivDelete.setImageResource(R.drawable.btn_delete);

		return row;
	}

	public void changeDataset(ArrayList<ShiftTemplate> alShiftTemplates)
	{
		malShiftTemplates.clear();
		malShiftTemplates.addAll(alShiftTemplates);

		notifyDataSetChanged();
	}

	static class ExistingShiftTemplate
	{
		ImageView ivIcon;
		TextView tvFrom;
		TextView tvTo;
		TextView tvJobName;
		TextView tvPaidHours;
		ImageView ivDelete;
		boolean bAdded = true;
	}
}
