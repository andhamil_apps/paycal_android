/*
 * DBHelper
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import eamp.droid.paycal.model.DailyPiggyDetails;
import eamp.droid.paycal.model.DailyShiftDetails;
import eamp.droid.paycal.model.Job;
import eamp.droid.paycal.model.ShiftInstance;
import eamp.droid.paycal.model.ShiftInstanceContinuing;
import eamp.droid.paycal.model.ShiftInstanceReminder;
import eamp.droid.paycal.model.ShiftTemplate;
import eamp.droid.paycal.model.ShiftTemplateRemainder;

public class DBHelper extends SQLiteOpenHelper
{
	private static DBHelper mInstance = null;

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "EAMP_DROID_PAYCAL_DB";

	private static final String TABLE_PAYCAL_JOBS = "PAYCAL_JOBS";
	private static final String TABLE_PAYCAL_ST = "PAYCAL_SHIFT_TEMPLATES";
	private static final String TABLE_PAYCAL_STR = "PAYCAL_SHIFT_TEMPLATES_REMINDERS";
	private static final String TABLE_PAYCAL_SI = "PAYCAL_SHIFT_INSTANCES";
	private static final String TABLE_PAYCAL_SIC = "PAYCAL_SHIFT_INSTANCES_CONTINUING";
	private static final String TABLE_PAYCAL_SIR = "PAYCAL_SHIFT_INSTANCES_REMINDERS";
	private static final String TABLE_PAYCAL_TALLIES = "PAYCAL_TALLIES";
	private static final String TABLE_PAYCAL_TALLY_DATES = "PAYCAL_TALLY_DATES";
	private static final String TABLE_PAYCAL_PIGGY_DATES = "PAYCAL_PIGGY_DATES";

	// Jobs Table Columns names
	private static final String JOBS_ID = "id";
	private static final String JOBS_NAME = "name";
	private static final String JOBS_COLOR = "color";
	private static final String JOBS_PAY = "pay";
	private static final String JOBS_PAY_UNIT = "pay_unit";
	private static final String JOBS_PAY_BEGAN_YEAR = "pay_began_year";
	private static final String JOBS_PAY_BEGAN_MOY = "pay_began_moy";
	private static final String JOBS_PAY_BEGAN_DOM = "pay_began_dom";
	private static final String JOBS_PAY_LENGTH = "pay_length";
	private static final String JOBS_PAY_DAY_YEAR = "pay_day_year";
	private static final String JOBS_PAY_DAY_MOY = "pay_day_moy";
	private static final String JOBS_PAY_DAY_DOM = "pay_day_dom";
	private static final String JOBS_PAY_DEDUCTION = "pay_deduction";
	private static final String JOBS_OVERTIME_RATE = "overtime_rate";
	private static final String JOBS_OVERTIME_MINUTES = "overtime_minutes";
	private static final String JOBS_OVERTIME_UNIT = "over_time_unit";
	private static final String JOBS_OVERTIME_ON = "overtime_on";

	// Shift Templates Table Columns names
	private static final String ST_ID = "id";
	private static final String ST_JOB_ID = "job_id";
	private static final String ST_COLOR = "color";
	private static final String ST_ICON = "icon";
	private static final String ST_FROM_HOUR = "from_hour";
	private static final String ST_FROM_MINUTE = "from_minute";
	private static final String ST_TO_HOUR = "to_hour";
	private static final String ST_TO_MINUTE = "to_minute";
	private static final String ST_UNPAID_BREAK = "unpaid_break";
	private static final String ST_SPECIAL_WAGE = "special_wage";
	private static final String ST_TIP = "tip";
	private static final String ST_NOTES = "notes";
	private static final String ST_PAID_MINUTES = "paid_minutes";

	// Shift Templates Reminders Table Columns names
	private static final String STR_ID = "id";
	private static final String STR_ST_ID = "shift_template_id";
	private static final String STR_REMINDER_INDEX = "reminder_index";

	// Shift Instances Table Columns names
	private static final String SI_ID = "id";
	private static final String SI_JOB_ID = "job_id";
	private static final String SI_COLOR = "color";
	private static final String SI_DATE_YEAR = "date_year";
	private static final String SI_DATE_MOY = "date_moy";
	private static final String SI_DATE_DOM = "date_dom";
	private static final String SI_FROM_HOUR = "from_hour";
	private static final String SI_FROM_MINUTE = "from_minute";
	private static final String SI_TO_HOUR = "to_hour";
	private static final String SI_TO_MINUTE = "to_minute";
	private static final String SI_UNPAID_BREAK = "unpaid_break";
	private static final String SI_SPECIAL_WAGE = "special_wage";
	private static final String SI_TIP = "tip";
	private static final String SI_NOTES = "notes";
	private static final String SI_CALENDAR_EVENT_ID = "calendar_event_id";
	private static final String SI_PAID_MINUTES = "paid_minutes";

	// Shift Instances Continuing Table Columns names
	private static final String SIC_ID = "id";
	private static final String SIC_SI_ID = "si_id";
	private static final String SIC_JOB_ID = "job_id";
	private static final String SIC_COLOR = "color";
	private static final String SIC_DATE_YEAR = "date_year";
	private static final String SIC_DATE_MOY = "date_moy";
	private static final String SIC_DATE_DOM = "date_dom";
	private static final String SIC_TO_HOUR = "to_hour";
	private static final String SIC_TO_MINUTE = "to_minute";
	private static final String SIC_AMOUNT = "amount";

	// Shift Instances Reminders Table Columns names
	private static final String SIR_ID = "id";
	private static final String SIR_SI_ID = "shift_instance_id";
	private static final String SIR_REMINDER_INDEX = "reminder_index";

	// Tally Table
	private static final String TALLY_ID = "id";
	private static final String TALLY_FROM_DATE = "from_date";
	private static final String TALLY_EVERY_COUNT = "every_count";
	private static final String TALLY_TIME_UNIT = "time_unit";
	private static final String TALLY_ON_DAY = "on_day";

	public static DBHelper getInstance(Context ctx)
	{
		if (mInstance == null)
		{
			mInstance = new DBHelper(ctx.getApplicationContext());
		}
		return mInstance;
	}

	private DBHelper(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		String CREATE_JOBS_TABLE = "CREATE TABLE " + TABLE_PAYCAL_JOBS + "(" +
		/* 00 */JOBS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
		/* 01 */JOBS_NAME + " TEXT," +
		/* 02 */JOBS_COLOR + " INTEGER," +
		/* 03 */JOBS_PAY + " REAL," +
		/* 04 */JOBS_PAY_UNIT + " INTEGER," +
		/* 05 */JOBS_PAY_BEGAN_YEAR + " INTEGER," +
		/* 06 */JOBS_PAY_BEGAN_MOY + " INTEGER," +
		/* 07 */JOBS_PAY_BEGAN_DOM + " INTEGER," +
		/* 08 */JOBS_PAY_LENGTH + " INTEGER," +
		/* 09 */JOBS_PAY_DAY_YEAR + " INTEGER," +
		/* 10 */JOBS_PAY_DAY_MOY + " INTEGER," +
		/* 11 */JOBS_PAY_DAY_DOM + " INTEGER," +
		/* 12 */JOBS_PAY_DEDUCTION + " REAL," +
		/* 13 */JOBS_OVERTIME_RATE + " REAL," +
		/* 14 */JOBS_OVERTIME_MINUTES + " INTEGER," +
		/* 15 */JOBS_OVERTIME_UNIT + " INTEGER," +
		/* 16 */JOBS_OVERTIME_ON + " TEXT" + ")";
		db.execSQL(CREATE_JOBS_TABLE);

		String CREATE_SHIFT_TEMPLATES_TABLE = "CREATE TABLE " + TABLE_PAYCAL_ST + "(" +
		/* 01 */ST_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
		/* 02 */ST_JOB_ID + " INTEGER," +
		/* 03 */ST_COLOR + " INTEGER," +
		/* 04 */ST_ICON + " INTEGER," +
		/* 05 */ST_FROM_HOUR + " INTEGER," +
		/* 06 */ST_FROM_MINUTE + " INTEGER," +
		/* 07 */ST_TO_HOUR + " INTEGER," +
		/* 08 */ST_TO_MINUTE + " INTEGER," +
		/* 09 */ST_UNPAID_BREAK + " INTEGER," +
		/* 10 */ST_SPECIAL_WAGE + " REAL," +
		/* 11 */ST_TIP + " REAL," +
		/* 12 */ST_NOTES + " TEXT," +
		/* 13 */ST_PAID_MINUTES + " INTEGER," +
		/* Constriants */" FOREIGN KEY (" + ST_JOB_ID + ") REFERENCES " + TABLE_PAYCAL_JOBS + " (" + JOBS_ID + "));";
		db.execSQL(CREATE_SHIFT_TEMPLATES_TABLE);

		String CREATE_STR_TABLE = "CREATE TABLE " + TABLE_PAYCAL_STR + "(" +
		/* 01 */STR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
		/* 02 */STR_ST_ID + " INTEGER," +
		/* 03 */STR_REMINDER_INDEX + " INTEGER," +
		/* Constriants */" FOREIGN KEY (" + STR_ST_ID + ") REFERENCES " + TABLE_PAYCAL_ST + " (" + ST_ID + "));";
		db.execSQL(CREATE_STR_TABLE);

		String CREATE_SHIFT_INSTANCES_TABLE = "CREATE TABLE " + TABLE_PAYCAL_SI + "(" +
		/* 01 */SI_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
		/* 02 */SI_JOB_ID + " INTEGER," +
		/* 03 */SI_COLOR + " INTEGER," +
		/* 04 */SI_DATE_YEAR + " INTEGER," +
		/* 05 */SI_DATE_MOY + " INTEGER," +
		/* 06 */SI_DATE_DOM + " INTEGER," +
		/* 07 */SI_FROM_HOUR + " INTEGER," +
		/* 08 */SI_FROM_MINUTE + " INTEGER," +
		/* 09 */SI_TO_HOUR + " INTEGER," +
		/* 10 */SI_TO_MINUTE + " INTEGER," +
		/* 11 */SI_UNPAID_BREAK + " INTEGER," +
		/* 12 */SI_SPECIAL_WAGE + " REAL," +
		/* 13 */SI_TIP + " REAL," +
		/* 14 */SI_NOTES + " TEXT," +
		/* 15 */SI_CALENDAR_EVENT_ID + " INTEGER," +
		/* 16 */SI_PAID_MINUTES + " INTEGER," +
		/* Constriants */" FOREIGN KEY (" + SI_JOB_ID + ") REFERENCES " + TABLE_PAYCAL_JOBS + " (" + JOBS_ID + "));";
		db.execSQL(CREATE_SHIFT_INSTANCES_TABLE);

		String CREATE_SHIFT_INSTANCES_CONTINUING_TABLE = "CREATE TABLE " + TABLE_PAYCAL_SIC + "(" +
		/* 01 */SIC_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
		/* 02 */SIC_SI_ID + " INTEGER," +
		/* 03 */SIC_JOB_ID + " INTEGER," +
		/* 04 */SIC_COLOR + " INTEGER," +
		/* 05 */SIC_DATE_YEAR + " INTEGER," +
		/* 06 */SIC_DATE_MOY + " INTEGER," +
		/* 07 */SIC_DATE_DOM + " INTEGER," +
		/* 08 */SIC_TO_HOUR + " INTEGER," +
		/* 09 */SIC_TO_MINUTE + " INTEGER," +
		/* 10 */SIC_AMOUNT + " REAL," +
		/* Constriants */" FOREIGN KEY (" + SIC_ID + ") REFERENCES " + TABLE_PAYCAL_SI + " (" + SI_ID + "), " +
		/* Constriants */" FOREIGN KEY (" + SIC_JOB_ID + ") REFERENCES " + TABLE_PAYCAL_JOBS + " (" + JOBS_ID + "));";
		db.execSQL(CREATE_SHIFT_INSTANCES_CONTINUING_TABLE);

		String CREATE_SHIFT_INSTANCE_REMINDERS_TABLE = "CREATE TABLE " + TABLE_PAYCAL_SIR + "(" +
		/* 01 */SIR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
		/* 02 */SIR_SI_ID + " INTEGER," +
		/* 03 */SIR_REMINDER_INDEX + " INTEGER," +
		/* Constriants */" FOREIGN KEY (" + SIR_SI_ID + ") REFERENCES " + TABLE_PAYCAL_SI + " (" + SI_ID + "));";
		db.execSQL(CREATE_SHIFT_INSTANCE_REMINDERS_TABLE);

		String CREATE_TALLY_TABLE = "CREATE TABLE " + TABLE_PAYCAL_TALLIES + "(" +
		/* 01 */TALLY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
		/* 02 */TALLY_FROM_DATE + " TEXT," +
		/* 03 */TALLY_EVERY_COUNT + " INTEGER," +
		/* 04 */TALLY_TIME_UNIT + " INTEGER," +
		/* 05 */TALLY_ON_DAY + " INTEGER" + ");";
		db.execSQL(CREATE_TALLY_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAYCAL_JOBS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAYCAL_ST);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAYCAL_STR);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAYCAL_SI);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAYCAL_SIC);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAYCAL_SIR);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAYCAL_TALLIES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAYCAL_TALLY_DATES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAYCAL_PIGGY_DATES);

		onCreate(db);
	}

	public boolean addJob(Job job)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(JOBS_NAME, job.getName());
		values.put(JOBS_NAME, job.getName());
		values.put(JOBS_COLOR, job.getColor());
		values.put(JOBS_PAY, job.getPay());
		values.put(JOBS_PAY_UNIT, job.getPayUnit());
		values.put(JOBS_PAY_BEGAN_YEAR, job.getPayBeganYear());
		values.put(JOBS_PAY_BEGAN_MOY, job.getPayBeganMOY());
		values.put(JOBS_PAY_BEGAN_DOM, job.getPayBeganDOM());
		values.put(JOBS_PAY_LENGTH, job.getPayLength());
		values.put(JOBS_PAY_DAY_YEAR, job.getPayDayYear());
		values.put(JOBS_PAY_DAY_MOY, job.getPayDayMOY());
		values.put(JOBS_PAY_DAY_DOM, job.getPayDayDOM());
		values.put(JOBS_PAY_DEDUCTION, job.getPayDeduction());
		values.put(JOBS_OVERTIME_RATE, job.getOvertimeRate());
		values.put(JOBS_OVERTIME_MINUTES, job.getOTMinutes());
		values.put(JOBS_OVERTIME_UNIT, job.getOvertimeUnit());
		if (job.isOvertimeOn())
			values.put(JOBS_OVERTIME_ON, "T");
		else
			values.put(JOBS_OVERTIME_ON, "F");

		long lResult = db.insert(TABLE_PAYCAL_JOBS, null, values);

		if (db.isOpen())
			db.close();

		if (lResult < 0)
			return false;

		return true;
	}

	public boolean addShiftTemplate(ShiftTemplate shiftTemplate)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ST_JOB_ID, shiftTemplate.getJobID());
		values.put(ST_COLOR, shiftTemplate.getColor());
		values.put(ST_ICON, shiftTemplate.getIcon());
		values.put(ST_FROM_HOUR, shiftTemplate.getFromHour());
		values.put(ST_FROM_MINUTE, shiftTemplate.getFromMinute());
		values.put(ST_TO_HOUR, shiftTemplate.getToHour());
		values.put(ST_TO_MINUTE, shiftTemplate.getToMinute());
		values.put(ST_UNPAID_BREAK, shiftTemplate.getUnPaidBreak());
		values.put(ST_SPECIAL_WAGE, shiftTemplate.getSpecialWage());
		values.put(ST_TIP, shiftTemplate.getTip());
		values.put(ST_NOTES, shiftTemplate.getNotes());
		values.put(ST_PAID_MINUTES, shiftTemplate.getPaidMinutes());

		long lResult = db.insert(TABLE_PAYCAL_ST, null, values);

		if (db.isOpen())
			db.close();

		if (lResult < 0)
			return false;

		return true;
	}

	public boolean addShiftTemplateRemainder(ShiftTemplateRemainder shiftTemplateRemainder)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(STR_ST_ID, shiftTemplateRemainder.getShiftTemplateID());
		values.put(STR_REMINDER_INDEX, shiftTemplateRemainder.getRemainderTimeIndex());

		long lResult = db.insert(TABLE_PAYCAL_STR, null, values);

		if (db.isOpen())
			db.close();

		if (lResult < 0)
			return false;

		return true;
	}

	public boolean addShiftInstance(ShiftInstance shiftInstance)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(SI_JOB_ID, shiftInstance.getJobID());
		values.put(SI_COLOR, shiftInstance.getColor());
		values.put(SI_DATE_YEAR, shiftInstance.getDateYear());
		values.put(SI_DATE_MOY, shiftInstance.getDateMOY());
		values.put(SI_DATE_DOM, shiftInstance.getDateDOM());
		values.put(SI_FROM_HOUR, shiftInstance.getFromHour());
		values.put(SI_FROM_MINUTE, shiftInstance.getFromMinute());
		values.put(SI_TO_HOUR, shiftInstance.getToHour());
		values.put(SI_TO_MINUTE, shiftInstance.getToMinute());
		values.put(SI_UNPAID_BREAK, shiftInstance.getUnPaidBreak());
		values.put(SI_SPECIAL_WAGE, shiftInstance.getSpecialWage());
		values.put(SI_TIP, shiftInstance.getTip());
		values.put(SI_NOTES, shiftInstance.getNotes());
		values.put(SI_CALENDAR_EVENT_ID, shiftInstance.getCalendarEventID());
		values.put(SI_PAID_MINUTES, shiftInstance.getPaidMinutes());

		long lResult = db.insert(TABLE_PAYCAL_SI, null, values);

		if (db.isOpen())
			db.close();

		if (lResult < 0)
			return false;

		return true;
	}

	public boolean addShiftInstanceContinuing(ShiftInstanceContinuing shiftInstanceContinuing)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(SIC_SI_ID, shiftInstanceContinuing.getShiftInstanceID());
		values.put(SIC_JOB_ID, shiftInstanceContinuing.getJobID());
		values.put(SIC_COLOR, shiftInstanceContinuing.getColor());
		values.put(SIC_DATE_YEAR, shiftInstanceContinuing.getDateYear());
		values.put(SIC_DATE_MOY, shiftInstanceContinuing.getDateMOY());
		values.put(SIC_DATE_DOM, shiftInstanceContinuing.getDateDOM());
		values.put(SIC_TO_HOUR, shiftInstanceContinuing.getToHour());
		values.put(SIC_TO_MINUTE, shiftInstanceContinuing.getToMinute());
		values.put(SIC_AMOUNT, shiftInstanceContinuing.getAmount());

		long lResult = db.insert(TABLE_PAYCAL_SIC, null, values);

		if (db.isOpen())
			db.close();

		if (lResult < 0)
			return false;

		return true;
	}

	public boolean addShiftInstanceReminder(ShiftInstanceReminder shiftInstanceReminder)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(SIR_SI_ID, shiftInstanceReminder.getShiftInstanceID());
		values.put(SIR_REMINDER_INDEX, shiftInstanceReminder.getRemainderTimeIndex());

		long lResult = db.insert(TABLE_PAYCAL_SIR, null, values);

		if (db.isOpen())
			db.close();

		if (lResult < 0)
			return false;

		return true;
	}

	public boolean addTally(String strFromDate, int nCount, int nEvery, int nOn)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(TALLY_FROM_DATE, strFromDate);
		values.put(TALLY_EVERY_COUNT, nCount);
		values.put(TALLY_TIME_UNIT, nEvery);
		values.put(TALLY_ON_DAY, nOn);

		long lResult = db.insert(TABLE_PAYCAL_TALLIES, null, values);

		if (db.isOpen())
			db.close();

		if (lResult < 0)
			return false;

		return true;
	}

	public boolean updateJob(Job job)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(JOBS_NAME, job.getName());
		values.put(JOBS_COLOR, job.getColor());
		values.put(JOBS_PAY, job.getPay());
		values.put(JOBS_PAY_UNIT, job.getPayUnit());
		values.put(JOBS_PAY_BEGAN_YEAR, job.getPayBeganYear());
		values.put(JOBS_PAY_BEGAN_MOY, job.getPayBeganMOY());
		values.put(JOBS_PAY_BEGAN_DOM, job.getPayBeganDOM());
		values.put(JOBS_PAY_LENGTH, job.getPayLength());
		values.put(JOBS_PAY_DAY_YEAR, job.getPayDayYear());
		values.put(JOBS_PAY_DAY_MOY, job.getPayDayMOY());
		values.put(JOBS_PAY_DAY_DOM, job.getPayDayDOM());
		values.put(JOBS_PAY_DEDUCTION, job.getPayDeduction());
		values.put(JOBS_OVERTIME_RATE, job.getOvertimeRate());
		values.put(JOBS_OVERTIME_MINUTES, job.getOTMinutes());
		values.put(JOBS_OVERTIME_UNIT, job.getOvertimeUnit());
		if (job.isOvertimeOn())
			values.put(JOBS_OVERTIME_ON, "T");
		else
			values.put(JOBS_OVERTIME_ON, "F");

		long lResult = db.update(TABLE_PAYCAL_JOBS, values, JOBS_ID + "=" + job.getID(), null);

		if (db.isOpen())
			db.close();

		if (lResult < 0)
			return false;

		return true;
	}

	public boolean updateShiftTemplate(ShiftTemplate shiftTemplate)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(ST_JOB_ID, shiftTemplate.getJobID());
		values.put(ST_COLOR, shiftTemplate.getColor());
		values.put(ST_ICON, shiftTemplate.getIcon());
		values.put(ST_FROM_HOUR, shiftTemplate.getFromHour());
		values.put(ST_FROM_MINUTE, shiftTemplate.getFromMinute());
		values.put(ST_TO_HOUR, shiftTemplate.getToHour());
		values.put(ST_TO_MINUTE, shiftTemplate.getToMinute());
		values.put(ST_UNPAID_BREAK, shiftTemplate.getUnPaidBreak());
		values.put(ST_SPECIAL_WAGE, shiftTemplate.getSpecialWage());
		values.put(ST_TIP, shiftTemplate.getTip());
		values.put(ST_NOTES, shiftTemplate.getNotes());
		values.put(ST_PAID_MINUTES, shiftTemplate.getPaidMinutes());

		long lResult = db.update(TABLE_PAYCAL_ST, values, ST_ID + "=" + shiftTemplate.getID(), null);

		if (db.isOpen())
			db.close();

		if (lResult < 0)
			return false;

		return true;
	}

	public boolean updateShiftInstance(ShiftInstance shiftInstance)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(SI_JOB_ID, shiftInstance.getJobID());
		values.put(SI_COLOR, shiftInstance.getColor());
		values.put(SI_DATE_YEAR, shiftInstance.getDateYear());
		values.put(SI_DATE_MOY, shiftInstance.getDateMOY());
		values.put(SI_DATE_DOM, shiftInstance.getDateDOM());
		values.put(SI_FROM_HOUR, shiftInstance.getFromHour());
		values.put(SI_FROM_MINUTE, shiftInstance.getFromMinute());
		values.put(SI_TO_HOUR, shiftInstance.getToHour());
		values.put(SI_TO_MINUTE, shiftInstance.getToMinute());
		values.put(SI_UNPAID_BREAK, shiftInstance.getUnPaidBreak());
		values.put(SI_SPECIAL_WAGE, shiftInstance.getSpecialWage());
		values.put(SI_TIP, shiftInstance.getTip());
		values.put(SI_NOTES, shiftInstance.getNotes());
		values.put(SI_CALENDAR_EVENT_ID, shiftInstance.getCalendarEventID());
		values.put(SI_PAID_MINUTES, shiftInstance.getPaidMinutes());

		long lResult = db.update(TABLE_PAYCAL_SI, values, SI_ID + "=" + shiftInstance.getID(), null);

		if (db.isOpen())
			db.close();

		if (lResult < 0)
			return false;

		return true;
	}

	public boolean updateShiftInstancePaidMinutes(int nShiftInstanceID, int nPaidMinutes)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(SI_PAID_MINUTES, nPaidMinutes);

		long lResult = db.update(TABLE_PAYCAL_SI, values, SI_ID + "=" + nShiftInstanceID, null);

		if (db.isOpen())
			db.close();

		if (lResult < 0)
			return false;

		return true;
	}

	public boolean updateShiftInstanceContinuingAmount(int nShiftInstanceID, double dAmount)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(SIC_AMOUNT, dAmount);

		long lResult = db.update(TABLE_PAYCAL_SIC, values, SIC_SI_ID + "=" + nShiftInstanceID, null);

		if (db.isOpen())
			db.close();

		if (lResult < 0)
			return false;

		return true;
	}

	public boolean updateTally(int nTallyID, String strFromDate, int nCount, int nEvery, int nOn)
	{
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(TALLY_FROM_DATE, strFromDate);
		values.put(TALLY_EVERY_COUNT, nCount);
		values.put(TALLY_TIME_UNIT, nEvery);
		values.put(TALLY_ON_DAY, nOn);

		long lResult = db.update(TABLE_PAYCAL_TALLIES, values, TALLY_ID + "=" + nTallyID, null);

		if (db.isOpen())
			db.close();

		if (lResult < 0)
			return false;

		return true;
	}

	public void deleteJob(int nJobID)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_PAYCAL_JOBS, JOBS_ID + " = " + nJobID, null);
		db.delete(TABLE_PAYCAL_ST, ST_JOB_ID + " = " + nJobID, null);

		if (db.isOpen())
			db.close();
	}

	public void deleteShiftTemplate(int nShiftTemplateID)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_PAYCAL_ST, ST_ID + " = " + nShiftTemplateID, null);

		if (db.isOpen())
			db.close();
	}

	public void deleteShiftTemplateReminder(int nShiftTemplateReminderID)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_PAYCAL_STR, STR_ID + " = " + nShiftTemplateReminderID, null);

		if (db.isOpen())
			db.close();
	}

	public void deleteShiftTemplateRemainders(int nShiftTemplateID)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_PAYCAL_STR, STR_ST_ID + " = " + nShiftTemplateID, null);

		if (db.isOpen())
			db.close();
	}

	public void deleteShiftInstance(int nShiftInstanceID)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_PAYCAL_SI, SI_ID + " = " + nShiftInstanceID, null);

		if (db.isOpen())
			db.close();
	}

	public void deleteShiftInstanceContinuing(int nShiftInstanceID)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_PAYCAL_SIC, SIC_SI_ID + " = " + nShiftInstanceID, null);

		if (db.isOpen())
			db.close();
	}

	public void deleteShiftInstanceContinuingOfAJob(int nJobID)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_PAYCAL_SIC, SIC_JOB_ID + " = " + nJobID, null);

		if (db.isOpen())
			db.close();
	}

	public void deleteShiftInstanceReminder(int nShiftInstanceReminderID)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_PAYCAL_SIR, SIR_ID + " = " + nShiftInstanceReminderID, null);

		if (db.isOpen())
			db.close();
	}

	public void deleteShiftInstanceReminders(int nShiftInstanceID)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_PAYCAL_SIR, SIR_SI_ID + " = " + nShiftInstanceID, null);

		if (db.isOpen())
			db.close();
	}

	public void deleteTally(int nTallyID)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_PAYCAL_TALLIES, TALLY_ID + " = " + nTallyID, null);

		if (db.isOpen())
			db.close();
	}

	/*
	 * public void deleteTallyDates(int nTallyID) { SQLiteDatabase db =
	 * this.getWritableDatabase(); db.delete(TABLE_PAYCAL_TALLY_DATES,
	 * TALLY_DATES_TALLY_ID + " = " + nTallyID, null);
	 * 
	 * if (db.isOpen()) db.close(); }
	 */

	/*
	 * public void deletePiggyDates(int nJobID) { SQLiteDatabase db =
	 * this.getWritableDatabase(); db.delete(TABLE_PAYCAL_PIGGY_DATES,
	 * PIGGY_DATES_JOB_ID + " = " + nJobID, null);
	 * 
	 * if (db.isOpen()) db.close(); }
	 */

	public ArrayList<ShiftTemplate> getShiftTemplates()
	{
		ArrayList<ShiftTemplate> alReturn = new ArrayList<ShiftTemplate>();

		String strQuery = "SELECT * FROM " + TABLE_PAYCAL_ST;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(strQuery, null);

		if (cursor.moveToFirst())
		{
			do
			{
				ShiftTemplate shiftTemplate = new ShiftTemplate();
				shiftTemplate.setID(cursor.getInt(0));
				shiftTemplate.setJobID(cursor.getInt(1));
				shiftTemplate.setColor(cursor.getInt(2));
				shiftTemplate.setIcon(cursor.getInt(3));
				shiftTemplate.setFromHour(cursor.getInt(4));
				shiftTemplate.setFromMinute(cursor.getInt(5));
				shiftTemplate.setToHour(cursor.getInt(6));
				shiftTemplate.setToMinute(cursor.getInt(7));
				shiftTemplate.setUnPaidBreak(cursor.getInt(8));
				shiftTemplate.setSpecialWage(cursor.getDouble(9));
				shiftTemplate.setTip(cursor.getDouble(10));
				shiftTemplate.setNotes(cursor.getString(11));
				shiftTemplate.setPaidMinutes(cursor.getInt(12));

				alReturn.add(shiftTemplate);
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return alReturn;
	}

	public ArrayList<ShiftTemplateRemainder> getShiftTemplateRemainders(int nShiftTemplateID)
	{
		ArrayList<ShiftTemplateRemainder> alSTR = new ArrayList<ShiftTemplateRemainder>();

		if (nShiftTemplateID != -1)
		{
			SQLiteDatabase db = this.getReadableDatabase();

			Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PAYCAL_STR + " WHERE " + STR_ST_ID + " = " + nShiftTemplateID + ";", null);

			if (cursor.moveToFirst())
			{
				do
				{
					ShiftTemplateRemainder str = new ShiftTemplateRemainder();
					str.setID(cursor.getInt(0));
					str.setShiftTemplateID(cursor.getInt(1));
					str.setRemainderTimeIndex(cursor.getInt(2));
					alSTR.add(str);
				} while (cursor.moveToNext());
			}

			if (!cursor.isClosed())
				cursor.close();

			if (db.isOpen())
				db.close();
		}

		return alSTR;
	}

	public boolean isShiftAlreadyAdded(int nJobID, int nYear, int nMOY, int nDOM, int nFromHour, int nFromMinute, int nToHour, int nToMinute)
	{
		boolean bShiftAlreadyAdded = false;

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PAYCAL_SI + " WHERE " + SI_JOB_ID + " = " + nJobID + " AND " + SI_DATE_YEAR + "=" + nYear + " AND " + SI_DATE_MOY + "=" + nMOY + " AND " + SI_DATE_DOM + "=" + nDOM + " AND " + SI_FROM_HOUR + "=" + nFromHour + " AND " + SI_FROM_MINUTE
				+ "=" + nFromMinute + " AND " + SI_TO_HOUR + "=" + nToHour + " AND " + SI_TO_MINUTE + "=" + nToMinute + ";", null);

		if (cursor.moveToFirst())
		{
			bShiftAlreadyAdded = true;
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return bShiftAlreadyAdded;
	}

	public boolean isShiftAlreadyAdded(int nShiftInstanceID, int nJobID, int nYear, int nMOY, int nDOM, int nFromHour, int nFromMinute, int nToHour, int nToMinute)
	{
		boolean bShiftAlreadyAdded = false;

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.rawQuery("SELECT " + SI_ID + " FROM " + TABLE_PAYCAL_SI + " WHERE " + SI_JOB_ID + " = " + nJobID + " AND " + SI_DATE_YEAR + "=" + nYear + " AND " + SI_DATE_MOY + "=" + nMOY + " AND " + SI_DATE_DOM + "=" + nDOM + " AND " + SI_FROM_HOUR + "=" + nFromHour + " AND "
				+ SI_FROM_MINUTE + "=" + nFromMinute + " AND " + SI_TO_HOUR + "=" + nToHour + " AND " + SI_TO_MINUTE + "=" + nToMinute + ";", null);

		if (cursor.moveToFirst())
		{
			do
			{
				if (cursor.getInt(0) != nShiftInstanceID)
				{
					bShiftAlreadyAdded = true;
					break;
				}
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return bShiftAlreadyAdded;
	}

	public ArrayList<ShiftInstanceContinuing> getShiftInstancesContinuing(int nYear, int nMOY, int nDOM)
	{
		ArrayList<ShiftInstanceContinuing> alReturn = new ArrayList<ShiftInstanceContinuing>();

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PAYCAL_SIC + " WHERE " + SIC_DATE_YEAR + " = " + nYear + " AND " + SIC_DATE_MOY + "=" + nMOY + " AND " + SIC_DATE_DOM + "=" + nDOM + ";", null);

		if (cursor.moveToFirst())
		{
			do
			{
				ShiftInstanceContinuing shiftInstanceContinuing = new ShiftInstanceContinuing();
				shiftInstanceContinuing.setID(cursor.getInt(0));
				shiftInstanceContinuing.setShiftInstanceID(cursor.getInt(1));
				shiftInstanceContinuing.setJobID(cursor.getInt(2));
				shiftInstanceContinuing.setColor(cursor.getInt(3));
				shiftInstanceContinuing.setToHour(cursor.getInt(7));
				shiftInstanceContinuing.setToMinute(cursor.getInt(8));
				shiftInstanceContinuing.setAmount(cursor.getDouble(9));

				alReturn.add(shiftInstanceContinuing);
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return alReturn;
	}

	public ArrayList<ShiftInstanceReminder> getShiftInstanceRemainders(int nShiftInstanceID)
	{
		ArrayList<ShiftInstanceReminder> alShiftInstanceReminders = new ArrayList<ShiftInstanceReminder>();

		if (nShiftInstanceID != -1)
		{
			SQLiteDatabase db = this.getReadableDatabase();

			Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PAYCAL_SIR + " WHERE " + SIR_SI_ID + " = " + nShiftInstanceID + ";", null);

			if (cursor.moveToFirst())
			{
				do
				{
					ShiftInstanceReminder str = new ShiftInstanceReminder();
					str.setID(cursor.getInt(0));
					str.setShiftInstanceID(cursor.getInt(1));
					str.setRemainderTimeIndex(cursor.getInt(2));
					alShiftInstanceReminders.add(str);
				} while (cursor.moveToNext());
			}

			if (!cursor.isClosed())
				cursor.close();

			if (db.isOpen())
				db.close();
		}

		return alShiftInstanceReminders;
	}

	public Job getJob(int nJobID)
	{
		Job job = null;

		if (nJobID != -1)
		{
			SQLiteDatabase db = this.getReadableDatabase();

			Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PAYCAL_JOBS + " WHERE " + JOBS_ID + " = " + nJobID + ";", null);

			if (cursor.moveToFirst())
			{
				job = new Job();
				job.setID(cursor.getInt(0));
				job.setName(cursor.getString(1));
				job.setColor(cursor.getInt(2));
				job.setPay(cursor.getDouble(3));
				job.setPayUnit(cursor.getInt(4));
				job.setPayBeganYear(cursor.getInt(5));
				job.setPayBeganMOY(cursor.getInt(6));
				job.setPayBeganDOM(cursor.getInt(7));
				job.setPayLength(cursor.getInt(8));
				job.setPayDayYear(cursor.getInt(9));
				job.setPayDayMOY(cursor.getInt(10));
				job.setPayDayDOM(cursor.getInt(11));
				job.setPayDeduction(cursor.getDouble(12));
				job.setOvertimeRate(cursor.getDouble(13));
				job.setOTMinutes(cursor.getInt(14));
				job.setOvertimeUnit(cursor.getInt(15));

				boolean bOvertimeOn = false;
				if (cursor.getString(16).equals("T"))
					bOvertimeOn = true;
				job.setOvertimeOn(bOvertimeOn);
			}

			if (!cursor.isClosed())
				cursor.close();

			if (db.isOpen())
				db.close();
		}

		return job;
	}

	public ShiftTemplate getShiftTemplate(int nShiftTemplateID)
	{
		ShiftTemplate shiftTemplate = null;

		if (nShiftTemplateID != -1)
		{
			SQLiteDatabase db = this.getReadableDatabase();

			Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PAYCAL_ST + " WHERE " + ST_ID + " = " + nShiftTemplateID + ";", null);

			if (cursor.moveToFirst())
			{
				shiftTemplate = new ShiftTemplate();
				shiftTemplate.setID(cursor.getInt(0));
				shiftTemplate.setJobID(cursor.getInt(1));
				shiftTemplate.setColor(cursor.getInt(2));
				shiftTemplate.setIcon(cursor.getInt(3));
				shiftTemplate.setFromHour(cursor.getInt(4));
				shiftTemplate.setFromMinute(cursor.getInt(5));
				shiftTemplate.setToHour(cursor.getInt(6));
				shiftTemplate.setToMinute(cursor.getInt(7));
				shiftTemplate.setUnPaidBreak(cursor.getInt(8));
				shiftTemplate.setSpecialWage(cursor.getDouble(9));
				shiftTemplate.setTip(cursor.getDouble(10));
				shiftTemplate.setNotes(cursor.getString(11));
				shiftTemplate.setPaidMinutes(cursor.getInt(12));
			}

			if (!cursor.isClosed())
				cursor.close();

			if (db.isOpen())
				db.close();
		}

		return shiftTemplate;
	}

	public ShiftInstance getShiftInstance(int nShiftInstanceID)
	{
		ShiftInstance shiftInstance = null;

		if (nShiftInstanceID != -1)
		{
			SQLiteDatabase db = this.getReadableDatabase();

			Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PAYCAL_SI + " WHERE " + SI_ID + " = " + nShiftInstanceID + ";", null);

			if (cursor.moveToFirst())
			{
				shiftInstance = new ShiftInstance();
				shiftInstance.setID(cursor.getInt(0));
				shiftInstance.setJobID(cursor.getInt(1));
				shiftInstance.setColor(cursor.getInt(2));
				shiftInstance.setDateYear(cursor.getInt(3));
				shiftInstance.setDateMOY(cursor.getInt(4));
				shiftInstance.setDateDOM(cursor.getInt(5));
				shiftInstance.setFromHour(cursor.getInt(6));
				shiftInstance.setFromMinute(cursor.getInt(7));
				shiftInstance.setToHour(cursor.getInt(8));
				shiftInstance.setToMinute(cursor.getInt(9));
				shiftInstance.setUnPaidBreak(cursor.getInt(10));
				shiftInstance.setSpecialWage(cursor.getDouble(11));
				shiftInstance.setTip(cursor.getDouble(12));
				shiftInstance.setNotes(cursor.getString(13));
				shiftInstance.setCalendarEventID(cursor.getLong(14));
				shiftInstance.setPaidMinutes(cursor.getInt(15));
			}

			if (!cursor.isClosed())
				cursor.close();

			if (db.isOpen())
				db.close();
		}

		return shiftInstance;
	}

	public ArrayList<Integer> getColIDsFromTabJobs()
	{
		ArrayList<Integer> vecJobIDs = new ArrayList<Integer>();

		String selectQuery = "SELECT " + JOBS_ID + " FROM " + TABLE_PAYCAL_JOBS;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst())
		{
			do
			{
				vecJobIDs.add(cursor.getInt(0));
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return vecJobIDs;
	}

	public ArrayList<String> getColNamesFromTabJobs()
	{
		ArrayList<String> alsReturn = new ArrayList<String>();

		String selectQuery = "SELECT " + JOBS_NAME + " FROM " + TABLE_PAYCAL_JOBS;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst())
		{
			do
			{
				alsReturn.add(cursor.getString(0));
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return alsReturn;
	}

	public ArrayList<Integer> getColColorsFromTabJobs()
	{
		ArrayList<Integer> vecJobIDs = new ArrayList<Integer>();

		String selectQuery = "SELECT " + JOBS_COLOR + " FROM " + TABLE_PAYCAL_JOBS;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst())
		{
			do
			{
				vecJobIDs.add(cursor.getInt(0));
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return vecJobIDs;
	}

	public double getDeductionFromTabJobs(int nJobID)
	{
		double dDeduction = 0;

		String selectQuery = "SELECT " + JOBS_PAY_DEDUCTION + " FROM " + TABLE_PAYCAL_JOBS + " WHERE " + JOBS_ID + "=" + nJobID;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst())
		{
			dDeduction = cursor.getDouble(0);
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return dDeduction;
	}

	public ArrayList<Integer> getColIDsFromTabShiftTemplates()
	{
		ArrayList<Integer> alnReturn = new ArrayList<Integer>();

		String selectQuery = "SELECT " + ST_ID + " FROM " + TABLE_PAYCAL_ST;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst())
		{
			do
			{
				alnReturn.add(cursor.getInt(0));
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return alnReturn;
	}

	public ArrayList<Integer> getColColorsFromTabShiftTemplates()
	{
		ArrayList<Integer> alnReturn = new ArrayList<Integer>();

		String selectQuery = "SELECT " + ST_COLOR + " FROM " + TABLE_PAYCAL_ST;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst())
		{
			do
			{
				alnReturn.add(cursor.getInt(0));
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return alnReturn;
	}

	public ArrayList<Integer> getColIconsFromTabShiftTemplates()
	{
		ArrayList<Integer> vecReturn = new ArrayList<Integer>();

		String selectQuery = "SELECT " + ST_ICON + " FROM " + TABLE_PAYCAL_ST;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst())
		{
			do
			{
				vecReturn.add(cursor.getInt(0));
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return vecReturn;
	}

	public int getJobIDFromTabShiftTemplates(int nShiftTemplateID)
	{
		int nReturn = 0;

		SQLiteDatabase db = this.getReadableDatabase();

		String strQuery = "SELECT " + ST_JOB_ID + " FROM " + TABLE_PAYCAL_ST + " WHERE " + ST_ID + "=" + nShiftTemplateID;
		Cursor cursor = db.rawQuery(strQuery, null);
		if (cursor.moveToFirst())
		{
			nReturn = cursor.getInt(0);
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return nReturn;
	}

	public ArrayList<Integer> getColIndexFromTabShiftTemplateReminders(int nShiftTemplateID)
	{
		ArrayList<Integer> alnReminderIndices = new ArrayList<Integer>();

		SQLiteDatabase db = this.getReadableDatabase();

		String strQuery = "SELECT " + STR_REMINDER_INDEX + " FROM " + TABLE_PAYCAL_STR + " WHERE " + STR_ST_ID + "=" + nShiftTemplateID;
		Cursor cursor = db.rawQuery(strQuery, null);
		if (cursor.moveToFirst())
		{
			do
			{
				alnReminderIndices.add(cursor.getInt(0));
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return alnReminderIndices;
	}

	public ArrayList<Integer> getColIDsFromTabTallies()
	{
		ArrayList<Integer> alnTallyIDs = new ArrayList<Integer>();

		SQLiteDatabase db = this.getReadableDatabase();

		String strQuery = "SELECT " + TALLY_ID + " FROM " + TABLE_PAYCAL_TALLIES;
		Cursor cursor = db.rawQuery(strQuery, null);
		if (cursor.moveToFirst())
		{
			do
			{
				alnTallyIDs.add(cursor.getInt(0));
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return alnTallyIDs;
	}

	public ArrayList<String> getColFromDateFromTabTallies()
	{
		ArrayList<String> alsTallyFromDates = new ArrayList<String>();

		SQLiteDatabase db = this.getReadableDatabase();

		String strQuery = "SELECT " + TALLY_FROM_DATE + " FROM " + TABLE_PAYCAL_TALLIES;
		Cursor cursor = db.rawQuery(strQuery, null);
		if (cursor.moveToFirst())
		{
			do
			{
				alsTallyFromDates.add(cursor.getString(0));
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return alsTallyFromDates;
	}

	public ArrayList<Integer> getColEveryCountsFromTabTallies()
	{
		ArrayList<Integer> alnTallyCounts = new ArrayList<Integer>();

		SQLiteDatabase db = this.getReadableDatabase();

		String strQuery = "SELECT " + TALLY_EVERY_COUNT + " FROM " + TABLE_PAYCAL_TALLIES;
		Cursor cursor = db.rawQuery(strQuery, null);
		if (cursor.moveToFirst())
		{
			do
			{
				alnTallyCounts.add(cursor.getInt(0));
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return alnTallyCounts;
	}

	public ArrayList<Integer> getColTimeUnitsFromTabTallies()
	{
		ArrayList<Integer> alnTallyTimeUnits = new ArrayList<Integer>();

		SQLiteDatabase db = this.getReadableDatabase();

		String strQuery = "SELECT " + TALLY_TIME_UNIT + " FROM " + TABLE_PAYCAL_TALLIES;
		Cursor cursor = db.rawQuery(strQuery, null);
		if (cursor.moveToFirst())
		{
			do
			{
				alnTallyTimeUnits.add(cursor.getInt(0));
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return alnTallyTimeUnits;
	}

	public ArrayList<Integer> getColOnDaysFromTabTallies()
	{
		ArrayList<Integer> alnTallyOn = new ArrayList<Integer>();

		SQLiteDatabase db = this.getReadableDatabase();

		String strQuery = "SELECT " + TALLY_ON_DAY + " FROM " + TABLE_PAYCAL_TALLIES;
		Cursor cursor = db.rawQuery(strQuery, null);
		if (cursor.moveToFirst())
		{
			do
			{
				alnTallyOn.add(cursor.getInt(0));
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return alnTallyOn;
	}

	public int getEveryCountFromTabTallies(int nTallyID)
	{
		int nTallyCount = 0;

		SQLiteDatabase db = this.getReadableDatabase();

		String strQuery = "SELECT " + TALLY_EVERY_COUNT + " FROM " + TABLE_PAYCAL_TALLIES + " WHERE " + TALLY_ID + "=" + nTallyID;
		Cursor cursor = db.rawQuery(strQuery, null);
		if (cursor.moveToFirst())
		{
			do
			{
				nTallyCount = cursor.getInt(0);
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return nTallyCount;
	}

	public int getTimeUnitFromTabTallies(int nTallyID)
	{
		int nTallyTimeUnit = 0;

		SQLiteDatabase db = this.getReadableDatabase();

		String strQuery = "SELECT " + TALLY_TIME_UNIT + " FROM " + TABLE_PAYCAL_TALLIES + " WHERE " + TALLY_ID + "=" + nTallyID;
		Cursor cursor = db.rawQuery(strQuery, null);
		if (cursor.moveToFirst())
		{
			do
			{
				nTallyTimeUnit = cursor.getInt(0);
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return nTallyTimeUnit;
	}

	public DailyShiftDetails getDailyShiftDetails(int nYear, int nMOY, int nDOM)
	{
		DailyShiftDetails dailyShiftDetails = new DailyShiftDetails();

		ArrayList<Integer> alnShiftInstanceIDs = new ArrayList<Integer>();

		ArrayList<Integer> alnJobIDs = new ArrayList<Integer>();
		ArrayList<String> alsJobNames = new ArrayList<String>();
		ArrayList<Integer> alnColors = new ArrayList<Integer>();

		ArrayList<Integer> alnFromHours = new ArrayList<Integer>();
		ArrayList<Integer> alnFromMinutes = new ArrayList<Integer>();
		ArrayList<Integer> alnToHours = new ArrayList<Integer>();
		ArrayList<Integer> alnToMinutes = new ArrayList<Integer>();

		ArrayList<Integer> alnRegularMinutes = new ArrayList<Integer>();
		ArrayList<Integer> alnSpecialMinutes = new ArrayList<Integer>();
		ArrayList<Integer> alnOvertimeMinutes = new ArrayList<Integer>();

		ArrayList<Double> aldRegularAmounts = new ArrayList<Double>();
		ArrayList<Double> aldSpecialAmounts = new ArrayList<Double>();
		ArrayList<Double> aldOvertimeAmounts = new ArrayList<Double>();

		ArrayList<Double> aldTips = new ArrayList<Double>();

		ArrayList<Integer> alnAvailableJobIDs = getColIDsFromTabJobs();
		for (int i = 0; i < alnAvailableJobIDs.size(); i++)
		{
			int nJobID = alnAvailableJobIDs.get(i);

			Job job = getJob(nJobID);
			double dRegularPay = job.getPay();
			boolean bOverTimeOn = job.isOvertimeOn();
			int nOTLimit = job.getOTMinutes();
			double dOvertimeRate = job.getOvertimeRate();

			if (bOverTimeOn)
			{
				if (job.getOvertimeUnit() == Konstant.JOB_OVERTIME_UNIT_DAY)
				{
					String selectQuerySII =
					/* SELECT */"SELECT " +
					/* 00 */SI_ID + ", " +
					/* 01 */SI_COLOR + ", " +
					/* 02 */SI_FROM_HOUR + ", " +
					/* 03 */SI_FROM_MINUTE + ", " +
					/* 04 */SI_TO_HOUR + ", " +
					/* 05 */SI_TO_MINUTE + ", " +
					/* 06 */SI_PAID_MINUTES + ", " +
					/* 07 */SI_SPECIAL_WAGE + ", " +
					/* 08 */SI_TIP +
					/* FROM */" FROM " + TABLE_PAYCAL_SI +
					/* WHERE */" WHERE " + SI_JOB_ID + "=" + nJobID +
					/* AND */"  AND " + SI_DATE_YEAR + "=" + nYear +
					/* AND */"  AND " + SI_DATE_MOY + "=" + nMOY +
					/* AND */"  AND " + SI_DATE_DOM + "=" + nDOM + ";";

					SQLiteDatabase db = this.getReadableDatabase();
					Cursor cursor = db.rawQuery(selectQuerySII, null);

					if (cursor.moveToFirst())
					{
						int nTodayPaidMinutes = 0;

						do
						{
							int nShiftInstanceID = cursor.getInt(0);
							int nShiftInstanceColor = cursor.getInt(1);
							int nFromHour = cursor.getInt(2);
							int nFromMinute = cursor.getInt(3);
							int nToHour = cursor.getInt(4);
							int nToMinute = cursor.getInt(5);
							int nPaidMinutes = cursor.getInt(6);
							double dSpecialRate = cursor.getDouble(7);
							double dTip = cursor.getDouble(8);

							alnShiftInstanceIDs.add(nShiftInstanceID);
							alnJobIDs.add(nJobID);
							alnColors.add(nShiftInstanceColor);
							alsJobNames.add(job.getName());

							alnFromHours.add(nFromHour);
							alnFromMinutes.add(nFromMinute);
							alnToHours.add(nToHour);
							alnToMinutes.add(nToMinute);

							if (dSpecialRate > 0)
							{
								// Just go with Special Wage

								alnRegularMinutes.add(0);
								alnOvertimeMinutes.add(0);
								alnSpecialMinutes.add(nPaidMinutes);

								if (job.getPayUnit() == Konstant.JOB_PAY_UNIT_HOUR)
								{
									aldSpecialAmounts.add((dSpecialRate * dRegularPay * nPaidMinutes / 60));
								}
								else
								{
									aldSpecialAmounts.add(dSpecialRate * dRegularPay);
								}

								aldRegularAmounts.add((double) 0);
								aldOvertimeAmounts.add((double) 0);

							}
							else
							{
								nTodayPaidMinutes += nPaidMinutes;
								if (nTodayPaidMinutes > nOTLimit)
								{
									// Shift falls under Overtime

									nTodayPaidMinutes -= nPaidMinutes;
									if (nTodayPaidMinutes > nOTLimit)
									{
										// Shift falls completely under Overtime

										alnRegularMinutes.add(0);
										alnSpecialMinutes.add(0);
										alnOvertimeMinutes.add(nPaidMinutes);

										if (job.getPayUnit() == Konstant.JOB_PAY_UNIT_HOUR)
										{
											aldOvertimeAmounts.add((dOvertimeRate * dRegularPay * nPaidMinutes / 60));
										}
										else
										{
											aldOvertimeAmounts.add(dOvertimeRate * dRegularPay);
										}

										aldRegularAmounts.add((double) 0);
										aldSpecialAmounts.add((double) 0);
									}
									else
									{
										// Shift partially under Regular and
										// partially under Overtime

										int nRegularMinutes = nOTLimit - nTodayPaidMinutes;
										int nOverTimeMinutes = nPaidMinutes - nRegularMinutes;

										alnRegularMinutes.add(nRegularMinutes);
										alnSpecialMinutes.add(0);
										alnOvertimeMinutes.add(nOverTimeMinutes);

										if (job.getPayUnit() == Konstant.JOB_PAY_UNIT_HOUR)
										{
											aldRegularAmounts.add((dRegularPay * nRegularMinutes / 60));
											aldOvertimeAmounts.add((dOvertimeRate * dRegularPay * nOverTimeMinutes / 60));
										}
										else
										{
											aldRegularAmounts.add(dRegularPay);
											aldOvertimeAmounts.add((double) 0);
										}

										aldSpecialAmounts.add((double) 0);
									}

									nTodayPaidMinutes += nPaidMinutes;
								}
								else
								{
									// Shift falls under Regular

									alnRegularMinutes.add(nPaidMinutes);
									alnSpecialMinutes.add(0);
									alnOvertimeMinutes.add(0);

									if (job.getPayUnit() == Konstant.JOB_PAY_UNIT_HOUR)
									{
										aldRegularAmounts.add((dRegularPay * nPaidMinutes / 60));
									}
									else
									// if [job.getPayUnit() ==
									// Konstant.JOB_PAY_UNIT_SHIFT]
									{
										aldRegularAmounts.add(dRegularPay);
									}

									aldOvertimeAmounts.add((double) 0);
									aldSpecialAmounts.add((double) 0);
								}
							}

							aldTips.add(dTip);
						} while (cursor.moveToNext());
					}

					if (!cursor.isClosed())
						cursor.close();

					if (db.isOpen())
						db.close();
				}
				else
				// if [job.getOvertimeUnit() == Konstant.JOB_OVERTIME_UNIT_WEEK]
				{
					try
					{
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CANADA);
						Date dateCurrent = sdf.parse(nYear + "-" + (nMOY) + "-" + (nDOM + 1));

						Calendar calendar = Calendar.getInstance();
						calendar.set(nYear, (nMOY - 1), (nDOM + 1));
						int nDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
						calendar.add(Calendar.DAY_OF_YEAR, (-nDayOfWeek));

						int nTotalPaidMinutes = 0;
						while (calendar.getTime().before(dateCurrent))
						{
							int nTempYear = calendar.get(Calendar.YEAR);
							int nTempMOY = calendar.get(Calendar.MONTH) + 1;
							int nTempDOM = calendar.get(Calendar.DAY_OF_MONTH);
							calendar.add(Calendar.DAY_OF_YEAR, 1);

							String selectQuerySII =
							/* SELECT */"SELECT " +
							/* 00 */SI_ID + ", " +
							/* 01 */SI_COLOR + ", " +
							/* 02 */SI_FROM_HOUR + ", " +
							/* 03 */SI_FROM_MINUTE + ", " +
							/* 04 */SI_TO_HOUR + ", " +
							/* 05 */SI_TO_MINUTE + ", " +
							/* 06 */SI_PAID_MINUTES + ", " +
							/* 07 */SI_SPECIAL_WAGE + ", " +
							/* 08 */SI_TIP +
							/* FROM */" FROM " + TABLE_PAYCAL_SI +
							/* WHERE */" WHERE " + SI_JOB_ID + "=" + nJobID +
							/* AND */"  AND " + SI_DATE_YEAR + "=" + nTempYear +
							/* AND */"  AND " + SI_DATE_MOY + "=" + nTempMOY +
							/* AND */"  AND " + SI_DATE_DOM + "=" + nTempDOM + ";";

							SQLiteDatabase db = this.getReadableDatabase();
							Cursor cursor = db.rawQuery(selectQuerySII, null);

							if (cursor.moveToFirst())
							{
								do
								{
									int nPaidMinutes = cursor.getInt(6);
									double dSpecialWage = cursor.getDouble(7);
									if (dSpecialWage <= 0)
										nTotalPaidMinutes += nPaidMinutes;

									if ((nTempYear == nYear) && (nTempMOY == nMOY) && (nTempDOM == nDOM))
									{
										int nShiftInstanceID = cursor.getInt(0);
										int nShiftInstanceColor = cursor.getInt(1);
										int nFromHour = cursor.getInt(2);
										int nFromMinute = cursor.getInt(3);
										int nToHour = cursor.getInt(4);
										int nToMinute = cursor.getInt(5);
										double dTip = cursor.getDouble(8);

										alnShiftInstanceIDs.add(nShiftInstanceID);
										alnJobIDs.add(nJobID);
										alnColors.add(nShiftInstanceColor);
										alsJobNames.add(job.getName());

										alnFromHours.add(nFromHour);
										alnFromMinutes.add(nFromMinute);
										alnToHours.add(nToHour);
										alnToMinutes.add(nToMinute);

										if (dSpecialWage > 0)
										{
											// Just go with Special Wage

											alnRegularMinutes.add(0);
											alnOvertimeMinutes.add(0);
											alnSpecialMinutes.add(nPaidMinutes);

											if (job.getPayUnit() == Konstant.JOB_PAY_UNIT_HOUR)
											{
												aldSpecialAmounts.add((dSpecialWage * dRegularPay * nPaidMinutes / 60));
											}
											else
											{
												aldSpecialAmounts.add(dSpecialWage * dRegularPay);
											}

											aldRegularAmounts.add((double) 0);
											aldOvertimeAmounts.add((double) 0);

										}
										else
										{
											if (nTotalPaidMinutes > nOTLimit)
											{
												// Shift falls under Overtime

												nTotalPaidMinutes -= nPaidMinutes;
												if (nTotalPaidMinutes > nOTLimit)
												{
													// Shift falls completely
													// under Overtime

													alnRegularMinutes.add(0);
													alnSpecialMinutes.add(0);
													alnOvertimeMinutes.add(nPaidMinutes);

													if (job.getPayUnit() == Konstant.JOB_PAY_UNIT_HOUR)
													{
														aldOvertimeAmounts.add((dOvertimeRate * dRegularPay * nPaidMinutes / 60));
													}
													else
													{
														aldOvertimeAmounts.add(dOvertimeRate * dRegularPay);
													}

													aldRegularAmounts.add((double) 0);
													aldSpecialAmounts.add((double) 0);
												}
												else
												{
													// Shift partially under
													// Regular and
													// partially under Overtime

													int nRegularMinutes = nOTLimit - nTotalPaidMinutes;
													int nOverTimeMinutes = nPaidMinutes - nRegularMinutes;

													alnRegularMinutes.add(nRegularMinutes);
													alnSpecialMinutes.add(0);
													alnOvertimeMinutes.add(nOverTimeMinutes);

													if (job.getPayUnit() == Konstant.JOB_PAY_UNIT_HOUR)
													{
														aldRegularAmounts.add((dRegularPay * nRegularMinutes / 60));
														aldOvertimeAmounts.add((dOvertimeRate * dRegularPay * nOverTimeMinutes / 60));
													}
													else
													{
														aldRegularAmounts.add(dRegularPay);
														aldOvertimeAmounts.add((double) 0);
													}

													aldSpecialAmounts.add((double) 0);
												}

												nTotalPaidMinutes += nPaidMinutes;
											}
											else
											{
												// Shift falls under Regular

												alnRegularMinutes.add(nPaidMinutes);
												alnSpecialMinutes.add(0);
												alnOvertimeMinutes.add(0);

												if (job.getPayUnit() == Konstant.JOB_PAY_UNIT_HOUR)
												{
													aldRegularAmounts.add((dRegularPay * nPaidMinutes / 60));
												}
												else
												{
													aldRegularAmounts.add(dRegularPay);
												}

												aldOvertimeAmounts.add((double) 0);
												aldSpecialAmounts.add((double) 0);
											}
										}

										aldTips.add(dTip);
									}

								} while (cursor.moveToNext());
							}

							if (!cursor.isClosed())
								cursor.close();

							if (db.isOpen())
								db.close();
						}
					}
					catch (ParseException e)
					{
						e.printStackTrace();
					}

				}
			}
			else
			{
				String selectQueryShiftInstance =
				/* SELECT */"SELECT " +
				/* 00 */SI_ID + ", " +
				/* 01 */SI_COLOR + ", " +
				/* 02 */SI_FROM_HOUR + ", " +
				/* 03 */SI_FROM_MINUTE + ", " +
				/* 04 */SI_TO_HOUR + ", " +
				/* 05 */SI_TO_MINUTE + ", " +
				/* 06 */SI_PAID_MINUTES + ", " +
				/* 07 */SI_SPECIAL_WAGE + ", " +
				/* 08 */SI_TIP +
				/* FROM */" FROM " + TABLE_PAYCAL_SI +
				/* WHERE */" WHERE " + SI_JOB_ID + "=" + nJobID +
				/* AND */"  AND " + SI_DATE_YEAR + "=" + nYear +
				/* AND */"  AND " + SI_DATE_MOY + "=" + nMOY +
				/* AND */"  AND " + SI_DATE_DOM + "=" + nDOM + ";";

				SQLiteDatabase db = this.getReadableDatabase();
				Cursor cursor = db.rawQuery(selectQueryShiftInstance, null);

				if (cursor.moveToFirst())
				{
					do
					{
						int nShiftInstanceID = cursor.getInt(0);
						int nShiftInstanceColor = cursor.getInt(1);
						int nFromHour = cursor.getInt(2);
						int nFromMinute = cursor.getInt(3);
						int nToHour = cursor.getInt(4);
						int nToMinute = cursor.getInt(5);
						int nPaidMinutes = cursor.getInt(6);
						double dSpecialRate = cursor.getDouble(7);
						double dTip = cursor.getDouble(8);

						alnShiftInstanceIDs.add(nShiftInstanceID);
						alnJobIDs.add(nJobID);
						alnColors.add(nShiftInstanceColor);
						alsJobNames.add(job.getName());

						alnFromHours.add(nFromHour);
						alnFromMinutes.add(nFromMinute);
						alnToHours.add(nToHour);
						alnToMinutes.add(nToMinute);

						if (dSpecialRate > 0)
						{
							alnRegularMinutes.add(0);
							alnSpecialMinutes.add(nPaidMinutes);

							aldRegularAmounts.add((double) 0);
							if (job.getPayUnit() == Konstant.JOB_PAY_UNIT_HOUR)
							{
								aldSpecialAmounts.add(dSpecialRate * dRegularPay * nPaidMinutes / 60);
							}
							else
							{
								aldSpecialAmounts.add(dSpecialRate * dRegularPay);
							}
						}
						else
						{
							alnRegularMinutes.add(nPaidMinutes);
							alnSpecialMinutes.add(0);

							aldSpecialAmounts.add((double) 0);
							if (job.getPayUnit() == Konstant.JOB_PAY_UNIT_HOUR)
							{
								aldRegularAmounts.add(dRegularPay * nPaidMinutes / 60);
							}
							else
							{
								aldRegularAmounts.add(dRegularPay);
							}
						}
						alnOvertimeMinutes.add(0);
						aldOvertimeAmounts.add((double) 0);

						aldTips.add(dTip);

					} while (cursor.moveToNext());
				}

				if (!cursor.isClosed())
					cursor.close();

				if (db.isOpen())
					db.close();
			}
		}

		dailyShiftDetails.setShiftInstanceIDs(alnShiftInstanceIDs);

		dailyShiftDetails.setJobIDs(alnJobIDs);
		dailyShiftDetails.setJobNames(alsJobNames);
		dailyShiftDetails.setColors(alnColors);

		dailyShiftDetails.setFromHours(alnFromHours);
		dailyShiftDetails.setFromMinutes(alnFromMinutes);
		dailyShiftDetails.setToHours(alnToHours);
		dailyShiftDetails.setToMinutes(alnToMinutes);

		dailyShiftDetails.setRegularMinutes(alnRegularMinutes);
		dailyShiftDetails.setSpecialMinutes(alnSpecialMinutes);
		dailyShiftDetails.setOvertimeMinutes(alnOvertimeMinutes);

		dailyShiftDetails.setRegularAmounts(aldRegularAmounts);
		dailyShiftDetails.setOvertimeAmounts(aldOvertimeAmounts);
		dailyShiftDetails.setSpecialAmounts(aldSpecialAmounts);

		dailyShiftDetails.setTips(aldTips);

		return dailyShiftDetails;
	}

	public DailyPiggyDetails getDailyPiggyDetails(int nYear, int nMOY, int nDOM)
	{
		nMOY -= 1;

		DailyPiggyDetails dailyPiggyDetails = new DailyPiggyDetails();
		dailyPiggyDetails.setPaydayYear(nYear);
		dailyPiggyDetails.setPaydayMOY(nMOY);
		dailyPiggyDetails.setPaydayDOM(nDOM);

		ArrayList<Integer> alnJobIDs = new ArrayList<Integer>();
		ArrayList<String> alsJobNames = new ArrayList<String>();

		ArrayList<Integer> alnRegularMinutes = new ArrayList<Integer>();
		ArrayList<Integer> alnSpecialMinutes = new ArrayList<Integer>();
		ArrayList<Integer> alnOvertimeMinutes = new ArrayList<Integer>();

		ArrayList<Double> aldRegularAmounts = new ArrayList<Double>();
		ArrayList<Double> aldSpecialAmounts = new ArrayList<Double>();
		ArrayList<Double> aldOvertimeAmounts = new ArrayList<Double>();

		ArrayList<Double> aldTips = new ArrayList<Double>();

		ArrayList<Double> aldDeductionRates = new ArrayList<Double>();
		ArrayList<Double> aldDeductions = new ArrayList<Double>();

		String selectQuery = "SELECT " + JOBS_ID + ", " + JOBS_NAME + " FROM " + TABLE_PAYCAL_JOBS + ";";

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		String strCurrentDate = nYear + "-" + (nMOY) + "-" + nDOM;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CANADA);
		Date dateCurrent = null;
		try
		{
			dateCurrent = sdf.parse(nYear + "-" + (nMOY + 1) + "-" + nDOM);

			if (cursor.moveToFirst())
			{
				do
				{
					int nJobID = cursor.getInt(0);
					String strJobName = cursor.getString(1);

					if (Utils.isPiggyDay(strCurrentDate, nJobID))
					{
						ArrayList<String> alsPiggyDates = Utils.msaPiggyDates.get(nJobID);
						if (alsPiggyDates.contains(strCurrentDate))
						{
							int nRegularMinutes = 0;
							int nOvertimeMinutes = 0;
							int nSpecialMinutes = 0;

							double dRegularAmount = 0;
							double dOvertimeAmount = 0;
							double dSpecialAmount = 0;

							int nPreviousIndex = alsPiggyDates.indexOf(strCurrentDate) - 1;
							if (nPreviousIndex >= 0)
							{
								String strPreviousPiggyDate = alsPiggyDates.get(nPreviousIndex);

								int nPreviousYear = Integer.parseInt(strPreviousPiggyDate.substring(0, strPreviousPiggyDate.indexOf("-")));
								int nPreviousMOY = Integer.parseInt(strPreviousPiggyDate.substring(strPreviousPiggyDate.indexOf("-") + 1, strPreviousPiggyDate.lastIndexOf("-")));
								int nPreviousDOM = Integer.parseInt(strPreviousPiggyDate.substring(strPreviousPiggyDate.lastIndexOf("-") + 1));

								Calendar calPreviousPayDay = Calendar.getInstance();
								calPreviousPayDay.set(nPreviousYear, nPreviousMOY, nPreviousDOM);

								alnJobIDs.add(nJobID);
								alsJobNames.add(strJobName);

								double dTips = 0;

								while (calPreviousPayDay.getTime().before(dateCurrent))
								{
									int nTempYear = calPreviousPayDay.get(Calendar.YEAR);
									int nTempMOY = calPreviousPayDay.get(Calendar.MONTH);
									int nTempDOM = calPreviousPayDay.get(Calendar.DAY_OF_MONTH);

									DailyShiftDetails shiftDetails = getDailyShiftDetails(nTempYear, (nTempMOY + 1), nTempDOM);

									for (int i = 0; i < shiftDetails.getJobIDs().size(); i++)
									{
										if (nJobID == shiftDetails.getJobIDs().get(i))
										{
											nRegularMinutes += shiftDetails.getRegularMinutes().get(i);
											nOvertimeMinutes += shiftDetails.getOvertimeMinutes().get(i);
											nSpecialMinutes += shiftDetails.getSpecialMinutes().get(i);

											dRegularAmount += shiftDetails.getRegularAmounts().get(i);
											dOvertimeAmount += shiftDetails.getOvertimeAmounts().get(i);
											dSpecialAmount += shiftDetails.getSpecialAmounts().get(i);

											dTips += shiftDetails.getTips().get(i);
										}
									}

									calPreviousPayDay.add(Calendar.DAY_OF_YEAR, 1);
								}

								alnRegularMinutes.add(nRegularMinutes);
								alnOvertimeMinutes.add(nOvertimeMinutes);
								alnSpecialMinutes.add(nSpecialMinutes);

								aldRegularAmounts.add(dRegularAmount);
								aldOvertimeAmounts.add(dOvertimeAmount);
								aldSpecialAmounts.add(dSpecialAmount);

								aldTips.add(dTips);

								double dDeductionRate = getDeductionFromTabJobs(nJobID);
								aldDeductionRates.add(dDeductionRate);

								double dDeduction = (dRegularAmount + dOvertimeAmount + dSpecialAmount) * (dDeductionRate / 100);
								aldDeductions.add(dDeduction);

							}
						}

					}
				} while (cursor.moveToNext());
			}

			dailyPiggyDetails.setJobIDs(alnJobIDs);
			dailyPiggyDetails.setJobNames(alsJobNames);

			dailyPiggyDetails.setRegularMinutes(alnRegularMinutes);
			dailyPiggyDetails.setOvertimeMinutes(alnOvertimeMinutes);
			dailyPiggyDetails.setSpecialMinutes(alnSpecialMinutes);

			dailyPiggyDetails.setRegularAmounts(aldRegularAmounts);
			dailyPiggyDetails.setOvertimeAmounts(aldOvertimeAmounts);
			dailyPiggyDetails.setSpecialAmounts(aldSpecialAmounts);

			dailyPiggyDetails.setTips(aldTips);

			dailyPiggyDetails.setDeductionRates(aldDeductionRates);
			dailyPiggyDetails.setDeductions(aldDeductions);
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return dailyPiggyDetails;
	}

	public int getMaxJobID()
	{

		SQLiteDatabase db = this.getReadableDatabase();
		final SQLiteStatement stmt = db.compileStatement("SELECT MAX(" + JOBS_ID + ") FROM " + TABLE_PAYCAL_JOBS + ";");

		int nReturn = (int) stmt.simpleQueryForLong();

		if (db.isOpen())
			db.close();

		return nReturn;
	}

	public int getMaxShiftTemplateID()
	{

		SQLiteDatabase db = this.getReadableDatabase();
		final SQLiteStatement stmt = db.compileStatement("SELECT MAX(" + ST_ID + ") FROM " + TABLE_PAYCAL_ST + ";");

		int nReturn = (int) stmt.simpleQueryForLong();

		if (db.isOpen())
			db.close();

		return nReturn;
	}

	public int getMaxShiftInstanceID()
	{

		SQLiteDatabase db = this.getReadableDatabase();
		final SQLiteStatement stmt = db.compileStatement("SELECT MAX(" + SI_ID + ") FROM " + TABLE_PAYCAL_SI + ";");

		int nReturn = (int) stmt.simpleQueryForLong();

		if (db.isOpen())
			db.close();

		return nReturn;
	}

	public int getMaxTallyID()
	{

		SQLiteDatabase db = this.getReadableDatabase();
		final SQLiteStatement stmt = db.compileStatement("SELECT MAX(" + TALLY_ID + ") FROM " + TABLE_PAYCAL_TALLIES + ";");

		int nReturn = (int) stmt.simpleQueryForLong();

		if (db.isOpen())
			db.close();

		return nReturn;
	}

	public boolean isJobsEmpty()
	{
		boolean bReturn = false;

		SQLiteDatabase db = this.getReadableDatabase();
		long lRows = DatabaseUtils.queryNumEntries(db, TABLE_PAYCAL_JOBS);

		if (lRows <= 0)
			bReturn = true;

		if (db.isOpen())
			db.close();

		return bReturn;
	}

	public boolean isJobNameAvailable(int nJobID, String strJobName)
	{
		boolean bReturn = true;

		String selectQuery = "SELECT " + JOBS_ID + ", " + JOBS_NAME + " FROM " + TABLE_PAYCAL_JOBS;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst())
		{
			do
			{
				int nRowJobID = cursor.getInt(0);
				int nRowJobName = cursor.getInt(1);

				if (nJobID != nRowJobID && strJobName.equals(nRowJobName))
				{
					bReturn = false;
					break;
				}
			} while (cursor.moveToNext());
		}

		if (!cursor.isClosed())
			cursor.close();

		if (db.isOpen())
			db.close();

		return bReturn;
	}

	public boolean isThereAnyIdenticalShiftTemplate(ShiftTemplate shiftTemplate)
	{
		boolean bReturn = false;
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PAYCAL_ST + ";", null);

		if (cursor.moveToFirst())
		{
			do
			{
				if (shiftTemplate.getJobID() != cursor.getInt(1))
					continue;

				if (shiftTemplate.getColor() != cursor.getInt(2))
					continue;

				if (shiftTemplate.getIcon() != cursor.getInt(3))
					continue;

				if (shiftTemplate.getFromHour() != cursor.getInt(4))
					continue;

				if (shiftTemplate.getFromMinute() != cursor.getInt(5))
					continue;

				if (shiftTemplate.getToHour() != cursor.getInt(6))
					continue;

				if (shiftTemplate.getToMinute() != cursor.getInt(7))
					continue;

				if (shiftTemplate.getUnPaidBreak() != cursor.getInt(8))
					continue;

				if (shiftTemplate.getSpecialWage() != cursor.getDouble(9))
					continue;

				if (shiftTemplate.getTip() != cursor.getDouble(10))
					continue;

				if (!shiftTemplate.getNotes().equalsIgnoreCase(cursor.getString(11)))
					continue;

				if (shiftTemplate.getPaidMinutes() != cursor.getInt(12))
					continue;

				bReturn = true;
				break;
			} while (cursor.moveToNext());
		}

		return bReturn;
	}
}