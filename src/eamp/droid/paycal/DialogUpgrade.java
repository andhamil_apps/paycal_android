/*
 * DialogUpgrade
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import com.android.vending.billing.utils.IabHelper;
import com.android.vending.billing.utils.IabResult;
import com.android.vending.billing.utils.Purchase;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class DialogUpgrade extends Dialog
{
	private Activity mActivity;
	private Context mContext;

	private Button mbtnUpgrade;

	private ImageButton mibClose;

	// Shared Preferences
	private SharedPreferences mPreferences;
	private SharedPreferences.Editor mPrefEditor;

	// In-App-Billing
	private IabHelper mIabHelper;
	private IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener;

	public DialogUpgrade(Activity activity, Context context)
	{
		super(context);

		mActivity = activity;
		mContext = context;

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		this.setContentView(R.layout.dialog_upgrade);
	}

	public void displayDialog()
	{
		mPreferences = mContext.getSharedPreferences(Konstant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		mPrefEditor = mPreferences.edit();

		mIabHelper = new IabHelper(mContext, Konstant.APPLICATION_PUBLIC_KEY);
		mIabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener()
		{
			public void onIabSetupFinished(IabResult result)
			{
			}
		});
		mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener()
		{
			public void onIabPurchaseFinished(IabResult result, Purchase purchase)
			{
				if (result.isSuccess())
				{
					if (purchase.getSku().equals(Konstant.APPLICATION_SKU_FOR_PREMIUM))
					{
						mPrefEditor.putBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, true);
						mPrefEditor.commit();
						Toast.makeText(mContext, mContext.getString(R.string.msg_upgraded), Toast.LENGTH_SHORT).show();
						dismiss();
						return;
					}
				}
			}
		};

		mibClose = (ImageButton) findViewById(R.id.ib_upgrade_close);
		mibClose.setOnClickListener(new android.view.View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dismiss();
			}
		});

		mbtnUpgrade = (Button) findViewById(R.id.btn_upgrade_footer);
		mbtnUpgrade.setOnClickListener(new android.view.View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				AccountManager accountManager = (AccountManager) mContext.getSystemService(Activity.ACCOUNT_SERVICE);
				Account[] accounts = accountManager.getAccounts();
				String strIdentifier = "[" + accounts[0].name + "][" + android.os.Build.MODEL + "][" + android.os.Build.VERSION.RELEASE + "]";
				try
				{
					mIabHelper.launchPurchaseFlow(mActivity, Konstant.APPLICATION_SKU_FOR_PREMIUM, 10001, mPurchaseFinishedListener, strIdentifier);
				}
				catch (Exception e)
				{
				}
			}
		});

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		getWindow().setAttributes(lp);
		show();
	}
}