/*
 * ActivitySplash
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;

public class ActivitySplash extends Activity
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);
		
		EnAppLauncher enLauncher = new EnAppLauncher();
		enLauncher.execute();
	}

	private void launchApplication()
	{
		SharedPreferences preferences = getSharedPreferences(Konstant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);

		boolean bIsTutorialDone = preferences.getBoolean(Konstant.PREF_KEY_IS_TUTORIAL_DONE, Konstant.PREF_DEF_IS_TUTORIAL_DONE);
		if (bIsTutorialDone)
		{
			Intent intentHome = new Intent(ActivitySplash.this, ActivityHome.class);
			startActivity(intentHome);
			finish();
		}
		else
		{
			Intent intentTutorial = new Intent(ActivitySplash.this, ActivityTutorial.class);
			intentTutorial.putExtra(Konstant.KEY_TUTORIAL_LAUNCH_FROM, Konstant.ACTIVITY_SPLASH);
			startActivity(intentTutorial);
			finish();
		}
	}

	private class EnAppLauncher extends AsyncTask<Void, Void, Void>
	{
		EnAppLauncher()
		{
		}

		@Override
		protected void onPreExecute()
		{
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			return null;
		}

		@Override
		protected void onProgressUpdate(Void... params)
		{
		}

		@Override
		protected void onPostExecute(Void param)
		{

			TimerTask pTimerTask = new TimerTask()
			{
				@Override
				public void run()
				{
					launchApplication();
				}
			};
			Timer pTimer = new Timer();
			pTimer.schedule(pTimerTask, 0);
		}
	}
}
