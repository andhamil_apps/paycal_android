/*
 * AdapterTallies
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnDismissListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterTallies extends ArrayAdapter<Integer>
{
	private Context mContext;

	private ArrayList<Integer> malnTallyIDs;
	private ArrayList<String> malsTallyFromDates;
	private ArrayList<Integer> malnTallyCounts;
	private ArrayList<Integer> malnTallyEvery;
	private ArrayList<Integer> malnTallyOn;

	private ExistingTally mListItemHolder;

	private int mLayoutResourceId;

	private SharedPreferences mPreferences;

	public AdapterTallies(SharedPreferences preferences, Context context, int layoutResourceId, ArrayList<Integer> alnTallyIDs, ArrayList<String> alsTallyFromDates, ArrayList<Integer> alnTallyCounts,
			ArrayList<Integer> alnTallyEvery, ArrayList<Integer> alnTallyOn)
	{
		super(context, layoutResourceId, alnTallyIDs);

		mPreferences = preferences;
		mContext = context;
		mLayoutResourceId = layoutResourceId;

		malnTallyIDs = alnTallyIDs;
		malsTallyFromDates = alsTallyFromDates;
		malnTallyCounts = alnTallyCounts;
		malnTallyEvery = alnTallyEvery;
		malnTallyOn = alnTallyOn;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		View row = convertView;
		if (row == null)
		{
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			row = inflater.inflate(mLayoutResourceId, parent, false);
			mListItemHolder = new ExistingTally();
			mListItemHolder.tvEvery = (TextView) row.findViewById(R.id.tv_lit_every);
			mListItemHolder.tvOn = (TextView) row.findViewById(R.id.tv_lit_on);
			mListItemHolder.ivDelete = (ImageView) row.findViewById(R.id.iv_lit_delete);
			row.setTag(mListItemHolder);
		}
		else
		{
			mListItemHolder = (ExistingTally) row.getTag();
		}

		mListItemHolder.ivDelete.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DBHelper.getInstance(mContext).deleteTally(malnTallyIDs.get(position));

				SharedPreferences.Editor prefEditor = mPreferences.edit();
				prefEditor.remove(Konstant.PREF_KEY_TALLY_DATES_FOR + malnTallyIDs.get(position));
				prefEditor.commit();

				Utils.msaTallyDates = Utils.getTallyDatesMappedByTallyID(mContext, mPreferences);

				malnTallyIDs.remove(position);
				malsTallyFromDates.remove(position);
				malnTallyCounts.remove(position);
				malnTallyEvery.remove(position);
				malnTallyOn.remove(position);

				notifyDataSetChanged();

				ActivitySettings.notifyTalliesChanged();
			}
		});

		row.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				int nID = malnTallyIDs.get(position);

				String strFromDate = malsTallyFromDates.get(position);
				int nCount = malnTallyCounts.get(position);
				int nEvery = malnTallyEvery.get(position);
				int nOn = malnTallyOn.get(position);

				DialogTally tallyDialog = new DialogTally(mContext);
				tallyDialog.setTallyDetails(nID, strFromDate, nCount, nEvery, nOn);

				tallyDialog.setOnDismissListener(new OnDismissListener()
				{
					@Override
					public void onDismiss(DialogInterface dialog)
					{
						ActivitySettings.refreshTallies();
					}
				});

				tallyDialog.displayDialog(true);
			}
		});

		String strEvery = "Every " + malnTallyCounts.get(position) + " " + Konstant.TALLY_EVERY[malnTallyEvery.get(position)];

		String strOn = "";
		switch (malnTallyEvery.get(position))
		{
		case Konstant.TALLY_EVERY_WEEKS:
			strOn = "On " + Konstant.TALLY_WEEKS_ON[malnTallyOn.get(position)];
			break;
		case Konstant.TALLY_EVERY_MONTHS:
			strOn = "On " + Konstant.TALLY_MONTHS_ON[malnTallyOn.get(position)];
			break;
		}

		mListItemHolder.tvEvery.setText(strEvery);
		mListItemHolder.tvOn.setText(strOn);
		mListItemHolder.ivDelete.setImageResource(R.drawable.btn_delete);

		return row;
	}

	public void changeDataset(ArrayList<Integer> alnTallyIDs, ArrayList<String> alsTallyFromDates, ArrayList<Integer> alnTallyCounts, ArrayList<Integer> alnTallyEvery, ArrayList<Integer> alnTallyOn)
	{
		malnTallyIDs.clear();
		malnTallyIDs.addAll(alnTallyIDs);

		malsTallyFromDates.clear();
		malsTallyFromDates.addAll(alsTallyFromDates);

		malnTallyCounts.clear();
		malnTallyCounts.addAll(alnTallyCounts);

		malnTallyEvery.clear();
		malnTallyEvery.addAll(alnTallyEvery);

		malnTallyOn.clear();
		malnTallyOn.addAll(alnTallyOn);

		notifyDataSetChanged();
	}

	static class ExistingTally
	{
		TextView tvEvery;
		TextView tvOn;
		ImageView ivDelete;
		boolean bAdded = true;
	}
}
