/*
 * ActivityShiftInstancesAdd
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import eamp.droid.paycal.model.Job;
import eamp.droid.paycal.model.ShiftInstance;
import eamp.droid.paycal.model.ShiftInstanceContinuing;
import eamp.droid.paycal.model.ShiftInstanceReminder;
import eamp.droid.paycal.model.ShiftTemplate;
import eamp.droid.paycal.model.ShiftTemplateRemainder;

public class ActivityShiftInstancesAdd extends Activity
{
	private boolean mbMilitaryTime;
	private boolean mbEditMode;

	private int mnOriginalShiftColor;
	private int mnSelectedColor;
	private int mnSelectedIcon;

	private int mnShiftInstanceID;

	private int mnDateYear;
	private int mnDateMOY;
	private int mnDateDOM;
	private int mnFromHour;
	private int mnFromMinute;
	private int mnToHour;
	private int mnToMinute;

	private long mlCalendarEventID;

	private ArrayList<Integer> malnJobIDs;

	private ImageButton mibUpgrade;
	private ImageButton mibUpgradeDummy;

	private Spinner mspnJobs;

	private ImageView mivPurple;
	private ImageView mivCyan;
	private ImageView mivGreen;
	private ImageView mivMustard;
	private ImageView mivOrange;
	private ImageView mivRed;

	private ImageView mivIcon;

	private CheckBox mcbSaveAsShiftTemplate;

	private EditText metDate;
	private EditText metFromTime;
	private EditText metToTime;

	private EditText metUnPaidBreak;
	private TextView mtvSpecialWage;
	private EditText metSpecialWage;
	private TextView mtvUpgradeOnSpecialWage;
	private TextView mtvTip;
	private EditText metTip;
	private TextView mtvNotes;
	private EditText metNotes;
	private TextView mtvUpgradeOnNotes;

	private TextView mtvReminders;
	private TextView mtvUpgradeOnReminders;
	private LinearLayout mllRemainders;
	private Button mbtnAddRemainder;

	private ArrayList<Spinner> malRemainderSpinners;

	private DatePickerDialog.OnDateSetListener mDateSetListener;
	private TimePickerDialog.OnTimeSetListener mFromTimeSetListener;
	private TimePickerDialog.OnTimeSetListener mToTimeSetListener;

	private Keyboard mKeyboard;
	private KeyboardView mKeyboardView;
	private OnKeyboardActionListener mOnKeyboardActionListener;

	private SharedPreferences mPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shift_instances_add);

		ActionBar currentActionBar = getActionBar();
		currentActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		currentActionBar.setDisplayShowTitleEnabled(false);
		currentActionBar.setCustomView(R.layout.action_bar_items_2);

		TextView tvActivityTitle = (TextView) findViewById(R.id.tv_ab2_title);
		tvActivityTitle.setText(getString(R.string.add_shift_instance));

		mPreferences = getSharedPreferences(Konstant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		mbMilitaryTime = mPreferences.getBoolean(Konstant.PREF_KEY_MILITARY_TIME_ON, Konstant.PREF_DEF_MILITARY_TIME_ON);

		mtvUpgradeOnSpecialWage = (TextView) findViewById(R.id.tv_sia_details_special_wage_upgrade);
		mtvUpgradeOnSpecialWage.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftInstancesAdd.this, ActivityShiftInstancesAdd.this);
				dialogUpgrade.displayDialog();
			}
		});

		mtvUpgradeOnNotes = (TextView) findViewById(R.id.tv_sia_notes_upgrade);
		mtvUpgradeOnNotes.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftInstancesAdd.this, ActivityShiftInstancesAdd.this);
				dialogUpgrade.displayDialog();
			}
		});

		mtvUpgradeOnReminders = (TextView) findViewById(R.id.tv_sia_remainders_upgrade);
		mtvUpgradeOnReminders.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftInstancesAdd.this, ActivityShiftInstancesAdd.this);
				dialogUpgrade.displayDialog();
			}
		});

		mibUpgrade = (ImageButton) findViewById(R.id.ib_asi_upgrade);
		mibUpgrade.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftInstancesAdd.this, ActivityShiftInstancesAdd.this);
				dialogUpgrade.displayDialog();
			}
		});
		mibUpgradeDummy = (ImageButton) findViewById(R.id.ib_asi_upgrade_dummy);

		ImageButton ibSave = (ImageButton) findViewById(R.id.ib_ab2_save);
		ibSave.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				saveShift();
			}
		});

		malRemainderSpinners = new ArrayList<Spinner>();

		mbEditMode = false;
		mnSelectedColor = Konstant.COLOR_GREEN;
		mnSelectedIcon = 1;
		mnShiftInstanceID = -1;
		mlCalendarEventID = -1;

		mOnKeyboardActionListener = new OnKeyboardActionListener()
		{
			@Override
			public void onKey(int primaryCode, int[] keyCodes)
			{
				mivIcon.setImageResource(Utils.getShiftTemplateIcon(primaryCode));
				hideCustomKeyboard();
			}

			@Override
			public void onPress(int arg0)
			{
			}

			@Override
			public void onRelease(int primaryCode)
			{
			}

			@Override
			public void onText(CharSequence text)
			{
			}

			@Override
			public void swipeDown()
			{
			}

			@Override
			public void swipeLeft()
			{
			}

			@Override
			public void swipeRight()
			{
			}

			@Override
			public void swipeUp()
			{
			}
		};

		mKeyboard = new Keyboard(ActivityShiftInstancesAdd.this, R.xml.st_icon_keyboard);
		mKeyboardView = (KeyboardView) findViewById(R.id.kv_ast);
		mKeyboardView.setKeyboard(mKeyboard);
		mKeyboardView.setPreviewEnabled(false);
		mKeyboardView.setOnKeyboardActionListener(mOnKeyboardActionListener);

		Calendar calendar = Calendar.getInstance();
		mnDateYear = calendar.get(Calendar.YEAR);
		mnDateMOY = calendar.get(Calendar.MONTH);
		mnDateDOM = calendar.get(Calendar.DAY_OF_MONTH);

		mnFromHour = Konstant.DEFAULT_SHIFT_FROM_HOUR;
		mnFromMinute = Konstant.DEFAULT_SHIFT_FROM_MINUTE;
		mnToHour = Konstant.DEFAULT_SHIFT_TO_HOUR;
		mnToMinute = Konstant.DEFAULT_SHIFT_TO_MINUTE;

		malnJobIDs = DBHelper.getInstance(ActivityShiftInstancesAdd.this).getColIDsFromTabJobs();
		ArrayList<String> alsJobNames = DBHelper.getInstance(ActivityShiftInstancesAdd.this).getColNamesFromTabJobs();
		final ArrayList<Integer> alnJobColors = DBHelper.getInstance(ActivityShiftInstancesAdd.this).getColColorsFromTabJobs();
		ArrayAdapter<String> adapterJobs = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, alsJobNames);
		adapterJobs.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mspnJobs = (Spinner) findViewById(R.id.spn_sia_job);
		mspnJobs.setAdapter(adapterJobs);
		mspnJobs.setOnItemSelectedListener(new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int nPosition, long arg3)
			{
				setShiftTemplateColor(alnJobColors.get(nPosition));
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0)
			{

			}
		});

		final RelativeLayout rlColor = (RelativeLayout) findViewById(R.id.rl_sia_color);
		rlColor.setVisibility(View.GONE);

		final RelativeLayout rlIcon = (RelativeLayout) findViewById(R.id.rl_sia_icon);
		rlIcon.setVisibility(View.GONE);

		mcbSaveAsShiftTemplate = (CheckBox) findViewById(R.id.cb_sia_save_as_template);
		mcbSaveAsShiftTemplate.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				if (isChecked)
				{
					rlColor.setVisibility(View.VISIBLE);
					rlIcon.setVisibility(View.VISIBLE);
				}
				else
				{
					rlColor.setVisibility(View.GONE);
					rlIcon.setVisibility(View.GONE);
				}
			}
		});

		mivPurple = (ImageView) findViewById(R.id.iv_sia_color_purple);
		mivCyan = (ImageView) findViewById(R.id.iv_sia_color_cyan);
		mivGreen = (ImageView) findViewById(R.id.iv_sia_color_green);
		mivGreen.setImageResource(R.drawable.color_selected);
		mivMustard = (ImageView) findViewById(R.id.iv_sia_color_mustard);
		mivOrange = (ImageView) findViewById(R.id.iv_sia_color_orange);
		mivRed = (ImageView) findViewById(R.id.iv_sia_color_red);

		mivPurple.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setShiftTemplateColor(Konstant.COLOR_PURPLE);
			}
		});

		mivCyan.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setShiftTemplateColor(Konstant.COLOR_CYAN);
			}
		});

		mivGreen.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setShiftTemplateColor(Konstant.COLOR_GREEN);
			}
		});

		mivMustard.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setShiftTemplateColor(Konstant.COLOR_MUSTARD);
			}
		});

		mivOrange.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setShiftTemplateColor(Konstant.COLOR_ORANGE);
			}
		});

		mivRed.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setShiftTemplateColor(Konstant.COLOR_RED);
			}
		});

		setShiftTemplateColor(alnJobColors.get(0));

		mivIcon = (ImageView) findViewById(R.id.iv_sia_icon);
		mivIcon.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				showCustomKeyboard(v);
			}
		});

		mDateSetListener = new DatePickerDialog.OnDateSetListener()
		{
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
			{
				mnDateYear = year;
				mnDateMOY = monthOfYear;
				mnDateDOM = dayOfMonth;
				metDate.setText(Utils.constructDate(year, monthOfYear, dayOfMonth));
			}
		};
		metDate = (EditText) findViewById(R.id.et_sia_date);
		metDate.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DatePickerDialog datePicker = new DatePickerDialog(ActivityShiftInstancesAdd.this, mDateSetListener, mnDateYear, mnDateMOY, mnDateDOM);
				datePicker.setTitle(getString(R.string.date));
				datePicker.show();
			}
		});
		metDate.setText(Utils.constructDate(mnDateYear, (mnDateMOY - 1), mnDateDOM));

		mFromTimeSetListener = new TimePickerDialog.OnTimeSetListener()
		{
			@Override
			public void onTimeSet(TimePicker arg0, int nHourOfDay, int nMinute)
			{
				mnFromHour = nHourOfDay;
				mnFromMinute = nMinute;
				metFromTime.setText(Utils.constructTime(mnFromHour, mnFromMinute, mPreferences));
			}
		};

		metFromTime = (EditText) findViewById(R.id.et_sia_from);
		metFromTime.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				TimePickerDialog timePicker = new TimePickerDialog(ActivityShiftInstancesAdd.this, mFromTimeSetListener, mnFromHour, mnFromMinute, mbMilitaryTime);
				timePicker.setTitle(getString(R.string.from));
				timePicker.show();
			}
		});
		metFromTime.setText(Utils.constructTime(mnFromHour, mnFromMinute, mPreferences));

		mToTimeSetListener = new TimePickerDialog.OnTimeSetListener()
		{
			@Override
			public void onTimeSet(TimePicker arg0, int nHourOfDay, int nMinute)
			{
				mnToHour = nHourOfDay;
				mnToMinute = nMinute;
				metToTime.setText(Utils.constructTime(mnToHour, mnToMinute, mPreferences));
			}
		};

		metToTime = (EditText) findViewById(R.id.et_sia_to);
		metToTime.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				TimePickerDialog timePicker = new TimePickerDialog(ActivityShiftInstancesAdd.this, mToTimeSetListener, mnToHour, mnToMinute, mbMilitaryTime);
				timePicker.setTitle(getString(R.string.to));
				timePicker.show();
			}
		});
		metToTime.setText(Utils.constructTime(mnToHour, mnToMinute, mPreferences));

		metUnPaidBreak = (EditText) findViewById(R.id.et_sia_details_unpaid_break);
		metUnPaidBreak.setText(Utils.convertMinsIntoString(Konstant.DEFAULT_SHIFT_UNPAID_BREAK));
		metUnPaidBreak.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if (hasFocus)
				{
					metUnPaidBreak.setText(Integer.toString(Utils.convertMinsIntoInt(metUnPaidBreak.getText().toString())));
				}
				else
				{
					metUnPaidBreak.setText(Utils.convertMinsIntoString(metUnPaidBreak.getText().toString()));
				}
			}
		});

		mtvSpecialWage = (TextView) findViewById(R.id.tv_sia_details_special_wage);

		metSpecialWage = (EditText) findViewById(R.id.et_sia_details_special_wage);
		Job job = DBHelper.getInstance(ActivityShiftInstancesAdd.this).getJob(malnJobIDs.get(mspnJobs.getSelectedItemPosition()));
		metSpecialWage.setText(Utils.convertRateIntoString(job.getPay(), Konstant.DEFAULT_SHIFT_SPECIAL_WAGE));
		metSpecialWage.setTag(metSpecialWage.getKeyListener());
		metSpecialWage.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if (hasFocus)
				{
					metSpecialWage.setText(Double.toString(Utils.convertRateIntoDouble(metSpecialWage.getText().toString())));
				}
				else
				{
					Job job = DBHelper.getInstance(ActivityShiftInstancesAdd.this).getJob(malnJobIDs.get(mspnJobs.getSelectedItemPosition()));
					metSpecialWage.setText(Utils.convertRateIntoString(Double.toString(job.getPay()), metSpecialWage.getText().toString()));
				}
			}
		});

		mtvTip = (TextView) findViewById(R.id.tv_sia_details_tip);

		metTip = (EditText) findViewById(R.id.et_sia_details_tip);
		metTip.setText(Utils.convertAmountIntoString(Konstant.DEFAULT_SHIFT_TIP));
		metTip.setTag(metTip.getKeyListener());
		metTip.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if (hasFocus)
				{
					metTip.setText(Double.toString(Utils.convertAmountIntoDouble(metTip.getText().toString())));
				}
				else
				{
					metTip.setText(Utils.convertAmountIntoString(metTip.getText().toString()));
				}
			}
		});

		mtvNotes = (TextView) findViewById(R.id.tv_sia_notes);

		metNotes = (EditText) findViewById(R.id.et_sia_notes);
		metNotes.setText(Konstant.DEFAULT_SHIFT_NOTES);
		metNotes.setTag(metNotes.getKeyListener());

		mtvReminders = (TextView) findViewById(R.id.tv_sia_remainders);

		mllRemainders = (LinearLayout) findViewById(R.id.ll_sia_remainders);

		final TextView tvNoRemainder = (TextView) findViewById(R.id.tv_sia_remainders_not_found);

		final ArrayAdapter<String> adapterRemainders = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Konstant.REMAINDERS);
		adapterRemainders.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mbtnAddRemainder = (Button) findViewById(R.id.btn_sia_add_remainder);
		mbtnAddRemainder.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);
				if (bIsPremium)
				{
					UtilsCalendar.mlCalendarID = mPreferences.getLong(Konstant.PREF_KEY_CALENDAR_ID, Konstant.PREF_DEF_CALENDAR_ID);
					if (UtilsCalendar.mlCalendarID == Konstant.PREF_DEF_CALENDAR_ID)
					{
						Utils.setupCalendarID(ActivityShiftInstancesAdd.this, mPreferences);
					}
					else
					{
						final LinearLayout llSingleRemainder = new LinearLayout(ActivityShiftInstancesAdd.this);

						int nPadding = getResources().getInteger(R.integer.padding_s);

						LinearLayout.LayoutParams lpSpinner = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, (float) 1.0);
						lpSpinner.gravity = Gravity.CENTER_VERTICAL;
						lpSpinner.setMargins(nPadding, nPadding, nPadding, nPadding);

						final Spinner spnRemainders = new Spinner(ActivityShiftInstancesAdd.this);
						spnRemainders.setAdapter(adapterRemainders);
						spnRemainders.setLayoutParams(lpSpinner);
						llSingleRemainder.addView(spnRemainders);

						LinearLayout.LayoutParams lpDelete = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
						lpDelete.gravity = Gravity.CENTER_VERTICAL;
						lpDelete.setMargins(nPadding, nPadding, nPadding, nPadding);

						ImageView ivDelete = new ImageView(ActivityShiftInstancesAdd.this);
						ivDelete.setImageResource(R.drawable.btn_delete);
						ivDelete.setLayoutParams(lpDelete);
						llSingleRemainder.addView(ivDelete);

						mllRemainders.addView(llSingleRemainder);

						malRemainderSpinners.add(spnRemainders);
						tvNoRemainder.setVisibility(View.GONE);

						ivDelete.setOnClickListener(new OnClickListener()
						{
							@Override
							public void onClick(View v)
							{
								malRemainderSpinners.remove(spnRemainders);
								if (malRemainderSpinners.isEmpty())
								{
									tvNoRemainder.setVisibility(View.VISIBLE);
								}
								mllRemainders.removeView(llSingleRemainder);
							}
						});
					}
				}
				else
				{
					DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftInstancesAdd.this, ActivityShiftInstancesAdd.this);
					dialogUpgrade.displayDialog();
				}
			}
		});

		Button btnDeleteShiftInstance = (Button) findViewById(R.id.btn_sia_delete_si);
		btnDeleteShiftInstance.setVisibility(View.GONE);
		btnDeleteShiftInstance.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(ActivityShiftInstancesAdd.this);
				builder.setTitle(R.string.delete_shift_instance);
				builder.setMessage(R.string.msg_prompt_shift_instance_delete);
				builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int id)
					{
						DBHelper.getInstance(ActivityShiftInstancesAdd.this).deleteShiftInstance(mnShiftInstanceID);
						DBHelper.getInstance(ActivityShiftInstancesAdd.this).deleteShiftInstanceReminders(mnShiftInstanceID);
						DBHelper.getInstance(ActivityShiftInstancesAdd.this).deleteShiftInstanceContinuing(mnShiftInstanceID);

						UtilsCalendar.deleteShiftEvent(getContentResolver(), mlCalendarEventID);

						finish();
					}
				});
				builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int id)
					{
					}
				});
				builder.create();
				builder.show();
			}
		});

		Intent intent = getIntent();
		if (intent != null)
		{
			if (intent.hasExtra(Konstant.KEY_SELECTED_DATE_YEAR) && intent.hasExtra(Konstant.KEY_SELECTED_DATE_MOY) && intent.hasExtra(Konstant.KEY_SELECTED_DATE_DOM))
			{
				mnDateYear = intent.getIntExtra(Konstant.KEY_SELECTED_DATE_YEAR, -1);
				mnDateMOY = intent.getIntExtra(Konstant.KEY_SELECTED_DATE_MOY, -1);
				mnDateDOM = intent.getIntExtra(Konstant.KEY_SELECTED_DATE_DOM, -1);
				metDate.setText(Utils.constructDate(mnDateYear, mnDateMOY, mnDateDOM));
			}

			mnShiftInstanceID = intent.getIntExtra(Konstant.KEY_SELECTED_SII_ID, -1);
			if (mnShiftInstanceID != -1)
			{
				tvActivityTitle.setText(getString(R.string.edit_shift_instance));

				ShiftInstance shiftInstance = DBHelper.getInstance(ActivityShiftInstancesAdd.this).getShiftInstance(mnShiftInstanceID);
				getActionBar().setTitle(getString(R.string.edit_shift_instance));
				mbEditMode = true;
				mcbSaveAsShiftTemplate.setVisibility(View.GONE);
				btnDeleteShiftInstance.setVisibility(View.VISIBLE);

				setShiftInstanceDetails(shiftInstance);

				mnOriginalShiftColor = shiftInstance.getColor();
				mlCalendarEventID = shiftInstance.getCalendarEventID();

				ArrayList<ShiftInstanceReminder> alSIIR = DBHelper.getInstance(ActivityShiftInstancesAdd.this).getShiftInstanceRemainders(mnShiftInstanceID);

				if (!alSIIR.isEmpty())
					tvNoRemainder.setVisibility(View.GONE);

				for (int i = 0; i < alSIIR.size(); i++)
				{
					final LinearLayout llSingleRemainder = new LinearLayout(ActivityShiftInstancesAdd.this);

					int nPadding = getResources().getInteger(R.integer.padding_s);

					LinearLayout.LayoutParams lpSpinner = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, (float) 1.0);
					lpSpinner.gravity = Gravity.CENTER_VERTICAL;
					lpSpinner.setMargins(nPadding, nPadding, nPadding, nPadding);

					final Spinner spnRemainders = new Spinner(ActivityShiftInstancesAdd.this);
					spnRemainders.setAdapter(adapterRemainders);
					spnRemainders.setLayoutParams(lpSpinner);
					spnRemainders.setSelection(alSIIR.get(i).getRemainderTimeIndex());
					malRemainderSpinners.add(spnRemainders);
					llSingleRemainder.addView(spnRemainders);

					LinearLayout.LayoutParams lpDelete = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					lpDelete.gravity = Gravity.CENTER_VERTICAL;
					lpDelete.setMargins(nPadding, nPadding, nPadding, nPadding);

					ImageView ivDelete = new ImageView(ActivityShiftInstancesAdd.this);
					ivDelete.setImageResource(R.drawable.btn_delete);
					ivDelete.setLayoutParams(lpDelete);
					llSingleRemainder.addView(ivDelete);

					mllRemainders.addView(llSingleRemainder);

					ivDelete.setOnClickListener(new OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							malRemainderSpinners.remove(spnRemainders);
							if (malRemainderSpinners.isEmpty())
							{
								tvNoRemainder.setVisibility(View.VISIBLE);
							}
							mllRemainders.removeView(llSingleRemainder);
						}
					});
				}
			}
		}

		checkIsAppPremium();
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		checkIsAppPremium();
	}

	private void checkIsAppPremium()
	{
		boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);
		if (bIsPremium)
		{
			mtvSpecialWage.setTextColor(getResources().getColor(android.R.color.white));
			mtvSpecialWage.setOnClickListener(null);
			mtvTip.setTextColor(getResources().getColor(android.R.color.white));
			mtvTip.setOnClickListener(null);
			mtvNotes.setTextColor(getResources().getColor(android.R.color.white));
			mtvNotes.setOnClickListener(null);
			mtvReminders.setTextColor(getResources().getColor(android.R.color.white));
			mtvReminders.setOnClickListener(null);

			metSpecialWage.setVisibility(View.VISIBLE);
			metTip.setVisibility(View.VISIBLE);
			metNotes.setVisibility(View.VISIBLE);
			mllRemainders.setVisibility(View.VISIBLE);
			mbtnAddRemainder.setVisibility(View.VISIBLE);

			mibUpgrade.setVisibility(View.GONE);
			mibUpgradeDummy.setVisibility(View.GONE);
			mtvUpgradeOnSpecialWage.setVisibility(View.GONE);
			mtvUpgradeOnNotes.setVisibility(View.GONE);
			mtvUpgradeOnReminders.setVisibility(View.GONE);
		}
		else
		{
			mtvSpecialWage.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftInstancesAdd.this, ActivityShiftInstancesAdd.this);
					dialogUpgrade.displayDialog();
				}
			});
			mtvTip.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftInstancesAdd.this, ActivityShiftInstancesAdd.this);
					dialogUpgrade.displayDialog();
				}
			});
			mtvNotes.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftInstancesAdd.this, ActivityShiftInstancesAdd.this);
					dialogUpgrade.displayDialog();
				}
			});
			mtvReminders.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftInstancesAdd.this, ActivityShiftInstancesAdd.this);
					dialogUpgrade.displayDialog();
				}
			});

			metSpecialWage.setVisibility(View.GONE);
			metTip.setVisibility(View.GONE);
			metNotes.setVisibility(View.GONE);
			mllRemainders.setVisibility(View.GONE);
			mbtnAddRemainder.setVisibility(View.GONE);

			mibUpgrade.setVisibility(View.VISIBLE);
			mibUpgradeDummy.setVisibility(View.VISIBLE);
			mtvUpgradeOnSpecialWage.setVisibility(View.VISIBLE);
			mtvUpgradeOnNotes.setVisibility(View.VISIBLE);
			mtvUpgradeOnReminders.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onBackPressed()
	{
		if (isCustomKeyboardVisible())
			hideCustomKeyboard();
		else
			this.finish();
	}

	private boolean saveShift()
	{
		try
		{
			int nJobID = malnJobIDs.get(mspnJobs.getSelectedItemPosition());

			boolean bShiftAlreadyAdded = false;
			if (mbEditMode)
				bShiftAlreadyAdded = DBHelper.getInstance(ActivityShiftInstancesAdd.this).isShiftAlreadyAdded(mnShiftInstanceID, nJobID, mnDateYear, mnDateMOY, mnDateDOM, mnFromHour, mnFromMinute, mnToHour, mnToMinute);
			else
				bShiftAlreadyAdded = DBHelper.getInstance(ActivityShiftInstancesAdd.this).isShiftAlreadyAdded(nJobID, mnDateYear, mnDateMOY, mnDateDOM, mnFromHour, mnFromMinute, mnToHour, mnToMinute);
			if (bShiftAlreadyAdded)
			{
				Toast.makeText(ActivityShiftInstancesAdd.this, getString(R.string.msg_si_already_added), Toast.LENGTH_SHORT).show();
				return false;
			}

			Job job = DBHelper.getInstance(ActivityShiftInstancesAdd.this).getJob(nJobID);

			Calendar currentCalendar = Calendar.getInstance();
			currentCalendar.set(mnDateYear, mnDateMOY, mnDateDOM);

			Calendar jobCalendar = Calendar.getInstance();
			jobCalendar.set(job.getPayBeganYear(), job.getPayBeganMOY(), job.getPayBeganDOM());

			if (currentCalendar.before(jobCalendar))
			{
				Toast.makeText(ActivityShiftInstancesAdd.this, getString(R.string.msg_no_shift_before_job_began), Toast.LENGTH_SHORT).show();
			}
			else
			{
				int nUnPaidBreak = Utils.convertMinsIntoInt(metUnPaidBreak.getText().toString());

				int nPaidMinutes = 0;
				int nStart = mnFromHour * 60 + mnFromMinute;
				int nEnd = mnToHour * 60 + mnToMinute;
				if (nStart <= nEnd)
				{
					nPaidMinutes = nEnd - nStart - nUnPaidBreak;
				}
				else
				{
					nPaidMinutes = (24 * 60 - nStart) + nEnd;
				}

				if (nPaidMinutes <= 0)
				{
					Toast.makeText(ActivityShiftInstancesAdd.this, getString(R.string.msg_shift_invalid_time), Toast.LENGTH_SHORT).show();
				}
				else
				{
					double dSpecialWage = Utils.convertRateIntoDouble(metSpecialWage.getText().toString());
					double dTip = Utils.convertAmountIntoDouble(metTip.getText().toString());

					String strNotes = metNotes.getText().toString();

					if (mcbSaveAsShiftTemplate.isChecked())
					{
						addShiftTemplate(nJobID, nUnPaidBreak, dSpecialWage, dTip, strNotes);
					}

					ShiftInstance shiftInstance = new ShiftInstance();
					shiftInstance.setJobID(nJobID);
					shiftInstance.setDateYear(mnDateYear);
					shiftInstance.setDateMOY((mnDateMOY + 1));
					shiftInstance.setDateDOM(mnDateDOM);
					shiftInstance.setFromHour(mnFromHour);
					shiftInstance.setFromMinute(mnFromMinute);
					shiftInstance.setToHour(mnToHour);
					shiftInstance.setToMinute(mnToMinute);
					shiftInstance.setUnPaidBreak(nUnPaidBreak);
					shiftInstance.setSpecialWage(dSpecialWage);
					shiftInstance.setTip(dTip);
					shiftInstance.setNotes(strNotes);
					shiftInstance.setPaidMinutes(nPaidMinutes);

					if (mbEditMode)
					{
						shiftInstance.setColor(mnOriginalShiftColor);
						updateShiftInstance(shiftInstance, job);
					}
					else
					{
						if (mcbSaveAsShiftTemplate.isChecked())
							shiftInstance.setColor(mnSelectedColor);
						else
							shiftInstance.setColor(job.getColor());
						addShiftInstance(shiftInstance, job);
					}
				}
			}
		}
		catch (NumberFormatException nfe)
		{
			Toast.makeText(ActivityShiftInstancesAdd.this, getString(R.string.msg_invalid_entry), Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	private void addShiftTemplate(int nJobID, int nUnPaidBreak, double dSpecialWage, double dTip, String strNotes)
	{
		ShiftTemplate shiftTemplate = new ShiftTemplate();
		shiftTemplate.setJobID(nJobID);
		shiftTemplate.setColor(mnSelectedColor);
		shiftTemplate.setIcon(mnSelectedIcon);
		shiftTemplate.setFromHour(mnFromHour);
		shiftTemplate.setFromMinute(mnFromMinute);
		shiftTemplate.setToHour(mnToHour);
		shiftTemplate.setToMinute(mnToMinute);
		shiftTemplate.setUnPaidBreak(nUnPaidBreak);
		shiftTemplate.setSpecialWage(dSpecialWage);
		shiftTemplate.setTip(dTip);
		shiftTemplate.setNotes(strNotes);

		int nPaidMinutes = 0;
		int nStart = mnFromHour * 60 + mnFromMinute;
		int nEnd = mnToHour * 60 + mnToMinute;
		if (nStart <= nEnd)
		{
			nPaidMinutes = nEnd - nStart - nUnPaidBreak;
		}
		else
		{
			nPaidMinutes = (24 * 60 - nStart) + nEnd;
		}
		shiftTemplate.setPaidMinutes(nPaidMinutes);

		if (DBHelper.getInstance(ActivityShiftInstancesAdd.this).addShiftTemplate(shiftTemplate))
		{
			int nCurrentShiftTemplateID = DBHelper.getInstance(ActivityShiftInstancesAdd.this).getMaxShiftTemplateID();
			if (nCurrentShiftTemplateID > 0)
			{
				for (int i = 0; i < malRemainderSpinners.size(); i++)
				{
					int nIndex = malRemainderSpinners.get(i).getSelectedItemPosition();
					ShiftTemplateRemainder srt = new ShiftTemplateRemainder();
					srt.setShiftTemplateID(nCurrentShiftTemplateID);
					srt.setRemainderTimeIndex(nIndex);
					DBHelper.getInstance(ActivityShiftInstancesAdd.this).addShiftTemplateRemainder(srt);
				}
			}
			ActivityShiftTemplates.notifyShiftTemplatesChanged();
			ActivityHome.setShiftTrayChanged(true);

			finish();
		}
		else
		{
			Toast.makeText(ActivityShiftInstancesAdd.this, getString(R.string.msg_st_add_fail), Toast.LENGTH_LONG).show();
		}
	}

	private void addShiftInstance(ShiftInstance shiftInstance, Job job)
	{
		int nStartYear = mnDateYear;
		int nStartMonth = mnDateMOY - 1;
		int nStartDay = mnDateDOM;

		Calendar calendar = Calendar.getInstance();
		calendar.set(mnDateYear, mnDateMOY, mnDateDOM);
		if (mnFromHour > mnToHour)
			calendar.add(Calendar.DAY_OF_YEAR, 1);

		int nEndYear = calendar.get(Calendar.YEAR);
		int nEndMOY = calendar.get(Calendar.MONTH) - 1;
		int nEndDOM = calendar.get(Calendar.DAY_OF_MONTH);

		boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);

		long lCalendarEventID = -1;
		if (bIsPremium)
			lCalendarEventID = UtilsCalendar.addShiftEvent(getContentResolver(), job.getName(), nStartYear, nStartMonth, nStartDay, mnFromHour, mnFromMinute, nEndYear, nEndMOY, nEndDOM, mnToHour, mnToMinute);

		if ((bIsPremium && (lCalendarEventID > 0)) || (!bIsPremium && (lCalendarEventID == -1)))
		{
			shiftInstance.setCalendarEventID(lCalendarEventID);

			if (DBHelper.getInstance(ActivityShiftInstancesAdd.this).addShiftInstance(shiftInstance))
			{
				int nGeneratedShiftInstanceID = DBHelper.getInstance(ActivityShiftInstancesAdd.this).getMaxShiftInstanceID();
				if (nGeneratedShiftInstanceID > 0)
				{
					for (int i = 0; i < malRemainderSpinners.size(); i++)
					{
						int nIndex = malRemainderSpinners.get(i).getSelectedItemPosition();
						ShiftInstanceReminder shiftInstanceReminder = new ShiftInstanceReminder();
						shiftInstanceReminder.setShiftInstanceID(nGeneratedShiftInstanceID);
						shiftInstanceReminder.setRemainderTimeIndex(nIndex);

						long lCalendarReminderID = UtilsCalendar.addShiftRemainder(getContentResolver(), lCalendarEventID, nIndex);
						if (lCalendarReminderID > 0)
						{
							DBHelper.getInstance(ActivityShiftInstancesAdd.this).addShiftInstanceReminder(shiftInstanceReminder);
						}
						else
						{
							finish();
							Toast.makeText(ActivityShiftInstancesAdd.this, getString(R.string.msg_si_add_fail), Toast.LENGTH_SHORT).show();
						}
					}

					if (mnFromHour > mnToHour)
					{
						ShiftInstanceContinuing shiftInstanceContinuing = new ShiftInstanceContinuing();
						shiftInstanceContinuing.setShiftInstanceID(nGeneratedShiftInstanceID);
						shiftInstanceContinuing.setJobID(job.getID());
						if (mcbSaveAsShiftTemplate.isChecked())
							shiftInstanceContinuing.setColor(mnSelectedColor);
						else
							shiftInstanceContinuing.setColor(job.getColor());
						shiftInstanceContinuing.setDateYear(nEndYear);
						shiftInstanceContinuing.setDateMOY((nEndMOY + 1));
						shiftInstanceContinuing.setDateDOM(nEndDOM);
						shiftInstanceContinuing.setToHour(mnToHour);
						shiftInstanceContinuing.setToMinute(mnToMinute);

						DBHelper.getInstance(ActivityShiftInstancesAdd.this).addShiftInstanceContinuing(shiftInstanceContinuing);
					}
				}
				finish();
			}
			else
			{
				finish();
				Toast.makeText(ActivityShiftInstancesAdd.this, getString(R.string.msg_si_add_fail), Toast.LENGTH_SHORT).show();
			}
		}
		else
		{
			finish();
			Toast.makeText(ActivityShiftInstancesAdd.this, getString(R.string.msg_si_add_fail), Toast.LENGTH_SHORT).show();
		}
	}

	private void updateShiftInstance(ShiftInstance shiftInstance, Job job)
	{
		UtilsCalendar.deleteShiftEvent(getContentResolver(), mlCalendarEventID);

		int nStartYear = mnDateYear;
		int nStartMonth = mnDateMOY - 1;
		int nStartDay = mnDateDOM;

		Calendar calendar = Calendar.getInstance();
		calendar.set(mnDateYear, mnDateMOY, mnDateDOM);
		if (mnFromHour > mnToHour)
			calendar.add(Calendar.DAY_OF_YEAR, 1);

		int nEndYear = calendar.get(Calendar.YEAR);
		int nEndMOY = calendar.get(Calendar.MONTH) - 1;
		int nEndDOM = calendar.get(Calendar.DAY_OF_MONTH);

		boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);

		long lCalendarEventID = -1;
		if (bIsPremium)
			lCalendarEventID = UtilsCalendar.addShiftEvent(getContentResolver(), job.getName(), nStartYear, nStartMonth, nStartDay, mnFromHour, mnFromMinute, nEndYear, nEndMOY, nEndDOM, mnToHour, mnToMinute);

		if ((bIsPremium && (lCalendarEventID > 0)) || (!bIsPremium && (lCalendarEventID == -1)))
		{
			shiftInstance.setCalendarEventID(lCalendarEventID);
			shiftInstance.setID(mnShiftInstanceID);
			DBHelper.getInstance(ActivityShiftInstancesAdd.this).deleteShiftInstanceReminders(mnShiftInstanceID);

			if (DBHelper.getInstance(ActivityShiftInstancesAdd.this).updateShiftInstance(shiftInstance))
			{
				for (int i = 0; i < malRemainderSpinners.size(); i++)
				{
					int nIndex = malRemainderSpinners.get(i).getSelectedItemPosition();
					ShiftInstanceReminder siir = new ShiftInstanceReminder();
					siir.setShiftInstanceID(mnShiftInstanceID);
					siir.setRemainderTimeIndex(nIndex);

					long lCalendarReminderID = UtilsCalendar.addShiftRemainder(getContentResolver(), lCalendarEventID, nIndex);
					if (lCalendarReminderID > 0)
					{
						DBHelper.getInstance(ActivityShiftInstancesAdd.this).addShiftInstanceReminder(siir);
					}
				}

				DBHelper.getInstance(ActivityShiftInstancesAdd.this).deleteShiftInstanceContinuing(mnShiftInstanceID);
				if (mnFromHour > mnToHour)
				{
					ShiftInstanceContinuing shiftInstanceContinuing = new ShiftInstanceContinuing();
					shiftInstanceContinuing.setShiftInstanceID(mnShiftInstanceID);
					shiftInstanceContinuing.setJobID(job.getID());
					shiftInstanceContinuing.setColor(mnOriginalShiftColor);
					shiftInstanceContinuing.setDateYear(nEndYear);
					shiftInstanceContinuing.setDateMOY((nEndMOY + 1));
					shiftInstanceContinuing.setDateDOM(nEndDOM);
					shiftInstanceContinuing.setToHour(mnToHour);
					shiftInstanceContinuing.setToMinute(mnToMinute);

					DBHelper.getInstance(ActivityShiftInstancesAdd.this).addShiftInstanceContinuing(shiftInstanceContinuing);
				}

				finish();
			}
			else
			{
				Toast.makeText(ActivityShiftInstancesAdd.this, getString(R.string.msg_si_edit_fail), Toast.LENGTH_SHORT).show();
			}
		}
		else
		{
			Toast.makeText(ActivityShiftInstancesAdd.this, getString(R.string.msg_si_edit_fail), Toast.LENGTH_SHORT).show();
		}
	}

	private void setShiftInstanceDetails(ShiftInstance shiftInstance)
	{
		mspnJobs.setSelection(malnJobIDs.indexOf(shiftInstance.getJobID()));

		mnDateYear = shiftInstance.getDateYear();
		mnDateMOY = (shiftInstance.getDateMOY() - 1);
		mnDateDOM = shiftInstance.getDateDOM();
		metDate.setText(Utils.constructDate(mnDateYear, mnDateMOY, mnDateDOM));

		mnFromHour = shiftInstance.getFromHour();
		mnFromMinute = shiftInstance.getFromMinute();
		metFromTime.setText(Utils.constructTime(mnFromHour, mnFromMinute, mPreferences));

		mnToHour = shiftInstance.getToHour();
		mnToMinute = shiftInstance.getToMinute();
		metToTime.setText(Utils.constructTime(mnToHour, mnToMinute, mPreferences));

		metUnPaidBreak.setText(Utils.convertMinsIntoString(shiftInstance.getUnPaidBreak()));

		Job job = DBHelper.getInstance(ActivityShiftInstancesAdd.this).getJob(shiftInstance.getJobID());
		metSpecialWage.setText(Utils.convertRateIntoString(job.getPay(), shiftInstance.getSpecialWage()));

		metTip.setText(Utils.convertAmountIntoString(shiftInstance.getTip()));

		metNotes.setText(shiftInstance.getNotes());
	}

	private void setShiftTemplateColor(int nColor)
	{
		switch (nColor)
		{
		case Konstant.COLOR_PURPLE:
			mnSelectedColor = Konstant.COLOR_PURPLE;
			mivPurple.setImageResource(R.drawable.color_selected);
			mivCyan.setImageResource(0);
			mivGreen.setImageResource(0);
			mivMustard.setImageResource(0);
			mivOrange.setImageResource(0);
			mivRed.setImageResource(0);
			break;
		case Konstant.COLOR_CYAN:
			mnSelectedColor = Konstant.COLOR_CYAN;
			mivPurple.setImageResource(0);
			mivCyan.setImageResource(R.drawable.color_selected);
			mivGreen.setImageResource(0);
			mivMustard.setImageResource(0);
			mivOrange.setImageResource(0);
			mivRed.setImageResource(0);
			break;
		case Konstant.COLOR_GREEN:
			mnSelectedColor = Konstant.COLOR_GREEN;
			mivPurple.setImageResource(0);
			mivCyan.setImageResource(0);
			mivGreen.setImageResource(R.drawable.color_selected);
			mivMustard.setImageResource(0);
			mivOrange.setImageResource(0);
			mivRed.setImageResource(0);
			break;
		case Konstant.COLOR_MUSTARD:
			mnSelectedColor = Konstant.COLOR_MUSTARD;
			mivPurple.setImageResource(0);
			mivCyan.setImageResource(0);
			mivGreen.setImageResource(0);
			mivMustard.setImageResource(R.drawable.color_selected);
			mivOrange.setImageResource(0);
			mivRed.setImageResource(0);
			break;
		case Konstant.COLOR_ORANGE:
			mnSelectedColor = Konstant.COLOR_ORANGE;
			mivPurple.setImageResource(0);
			mivCyan.setImageResource(0);
			mivGreen.setImageResource(0);
			mivMustard.setImageResource(0);
			mivOrange.setImageResource(R.drawable.color_selected);
			mivRed.setImageResource(0);
			break;
		case Konstant.COLOR_RED:
			mnSelectedColor = Konstant.COLOR_RED;
			mivPurple.setImageResource(0);
			mivCyan.setImageResource(0);
			mivGreen.setImageResource(0);
			mivMustard.setImageResource(0);
			mivOrange.setImageResource(0);
			mivRed.setImageResource(R.drawable.color_selected);
			break;
		}
	}

	private void hideCustomKeyboard()
	{
		mKeyboardView.setVisibility(View.GONE);
		mKeyboardView.setEnabled(false);
	}

	private void showCustomKeyboard(View v)
	{
		mKeyboardView.setVisibility(View.VISIBLE);
		mKeyboardView.setEnabled(true);
		if (v != null)
			((InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	private boolean isCustomKeyboardVisible()
	{
		return mKeyboardView.getVisibility() == View.VISIBLE;
	}
}