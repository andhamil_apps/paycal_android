/*
 * ActivityShiftTemplatesAdd
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import eamp.droid.paycal.model.Job;
import eamp.droid.paycal.model.ShiftTemplate;
import eamp.droid.paycal.model.ShiftTemplateRemainder;

public class ActivityShiftTemplatesAdd extends Activity
{
	private boolean mbMilitaryTime;
	private boolean mbEditMode;

	private int mnShiftTemplateID;

	private int mnSelectedColor;
	private int mnSelectedIcon;

	private int mnFromHour;
	private int mnFromMinute;
	private int mnToHour;
	private int mnToMinute;

	private ArrayList<Integer> malnJobIDs;

	private ImageButton mibUpgrade;
	private ImageButton mibUpgradeDummy;

	private Spinner mspnJobs;

	private ImageView mivPurple;
	private ImageView mivCyan;
	private ImageView mivGreen;
	private ImageView mivMustard;
	private ImageView mivOrange;
	private ImageView mivRed;

	private ImageView mivIcon;

	private EditText metFromTime;
	private EditText metToTime;

	private EditText metUnPaidBreak;
	private TextView mtvSpecialWage;
	private EditText metSpecialWage;
	private TextView mtvUpgradeOnSpecialWage;
	private TextView mtvTip;
	private EditText metTip;
	private TextView mtvNotes;
	private EditText metNotes;
	private TextView mtvUpgradeOnNotes;

	private TextView mtvReminders;
	private TextView mtvUpgradeOnReminders;
	private LinearLayout mllRemainders;
	private Button mbtnAddRemainder;

	private ArrayList<Spinner> malRemainderSpinners;

	private TimePickerDialog.OnTimeSetListener mFromTimeSetListener;
	private TimePickerDialog.OnTimeSetListener mToTimeSetListener;

	private Keyboard mKeyboard;
	private KeyboardView mKeyboardView;
	private OnKeyboardActionListener mOnKeyboardActionListener;

	private SharedPreferences mPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shift_templates_add);

		ActionBar currentActionBar = getActionBar();
		currentActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		currentActionBar.setDisplayShowTitleEnabled(false);
		currentActionBar.setCustomView(R.layout.action_bar_items_2);

		TextView tvActivityTitle = (TextView) findViewById(R.id.tv_ab2_title);
		tvActivityTitle.setText(getString(R.string.add_shift_template));

		mtvUpgradeOnSpecialWage = (TextView) findViewById(R.id.tv_sta_details_special_wage_upgrade);
		mtvUpgradeOnSpecialWage.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftTemplatesAdd.this, ActivityShiftTemplatesAdd.this);
				dialogUpgrade.displayDialog();
			}
		});

		mtvUpgradeOnNotes = (TextView) findViewById(R.id.tv_sta_notes_upgrade);
		mtvUpgradeOnNotes.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftTemplatesAdd.this, ActivityShiftTemplatesAdd.this);
				dialogUpgrade.displayDialog();
			}
		});

		mtvUpgradeOnReminders = (TextView) findViewById(R.id.tv_sta_remainders_upgrade);
		mtvUpgradeOnReminders.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftTemplatesAdd.this, ActivityShiftTemplatesAdd.this);
				dialogUpgrade.displayDialog();
			}
		});

		mibUpgrade = (ImageButton) findViewById(R.id.ib_ast_upgrade);
		mibUpgrade.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftTemplatesAdd.this, ActivityShiftTemplatesAdd.this);
				dialogUpgrade.displayDialog();
			}
		});
		mibUpgradeDummy = (ImageButton) findViewById(R.id.ib_ast_upgrade_dummy);

		ImageButton ibSave = (ImageButton) findViewById(R.id.ib_ab2_save);
		ibSave.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				saveShiftTemplate();
			}
		});

		mPreferences = getSharedPreferences(Konstant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		mbMilitaryTime = mPreferences.getBoolean(Konstant.PREF_KEY_MILITARY_TIME_ON, Konstant.PREF_DEF_MILITARY_TIME_ON);

		malRemainderSpinners = new ArrayList<Spinner>();

		mnSelectedColor = Konstant.COLOR_GREEN;
		mnSelectedIcon = 1;
		mbEditMode = false;

		mOnKeyboardActionListener = new OnKeyboardActionListener()
		{
			@Override
			public void onKey(int primaryCode, int[] keyCodes)
			{
				mnSelectedIcon = primaryCode;
				mivIcon.setImageResource(Utils.getShiftTemplateIcon(mnSelectedIcon));
				hideCustomKeyboard();
			}

			@Override
			public void onPress(int arg0)
			{
			}

			@Override
			public void onRelease(int primaryCode)
			{
			}

			@Override
			public void onText(CharSequence text)
			{
			}

			@Override
			public void swipeDown()
			{
			}

			@Override
			public void swipeLeft()
			{
			}

			@Override
			public void swipeRight()
			{
			}

			@Override
			public void swipeUp()
			{
			}
		};

		mKeyboard = new Keyboard(ActivityShiftTemplatesAdd.this, R.xml.st_icon_keyboard);
		mKeyboardView = (KeyboardView) findViewById(R.id.kv_ast);
		mKeyboardView.setKeyboard(mKeyboard);
		mKeyboardView.setPreviewEnabled(false);
		mKeyboardView.setOnKeyboardActionListener(mOnKeyboardActionListener);

		mnFromHour = Konstant.DEFAULT_SHIFT_FROM_HOUR;
		mnFromMinute = Konstant.DEFAULT_SHIFT_FROM_MINUTE;
		mnToHour = Konstant.DEFAULT_SHIFT_TO_HOUR;
		mnToMinute = Konstant.DEFAULT_SHIFT_TO_MINUTE;

		malnJobIDs = DBHelper.getInstance(ActivityShiftTemplatesAdd.this).getColIDsFromTabJobs();
		ArrayList<String> alsJobNames = DBHelper.getInstance(ActivityShiftTemplatesAdd.this).getColNamesFromTabJobs();
		final ArrayList<Integer> alnJobColors = DBHelper.getInstance(ActivityShiftTemplatesAdd.this).getColColorsFromTabJobs();
		ArrayAdapter<String> adapterJobs = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, alsJobNames);
		adapterJobs.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mspnJobs = (Spinner) findViewById(R.id.spn_sta_job);
		mspnJobs.setAdapter(adapterJobs);

		mivPurple = (ImageView) findViewById(R.id.iv_sta_color_purple);
		mivCyan = (ImageView) findViewById(R.id.iv_sta_color_cyan);
		mivGreen = (ImageView) findViewById(R.id.iv_sta_color_green);
		mivGreen.setImageResource(R.drawable.color_selected);
		mivMustard = (ImageView) findViewById(R.id.iv_sta_color_mustard);
		mivOrange = (ImageView) findViewById(R.id.iv_sta_color_orange);
		mivRed = (ImageView) findViewById(R.id.iv_sta_color_red);

		mivPurple.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setShiftTemplateColor(Konstant.COLOR_PURPLE);
			}
		});

		mivCyan.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setShiftTemplateColor(Konstant.COLOR_CYAN);
			}
		});

		mivGreen.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setShiftTemplateColor(Konstant.COLOR_GREEN);
			}
		});

		mivMustard.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setShiftTemplateColor(Konstant.COLOR_MUSTARD);
			}
		});

		mivOrange.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setShiftTemplateColor(Konstant.COLOR_ORANGE);
			}
		});

		mivRed.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setShiftTemplateColor(Konstant.COLOR_RED);
			}
		});

		setShiftTemplateColor(alnJobColors.get(0));

		mivIcon = (ImageView) findViewById(R.id.iv_sta_icon);
		mivIcon.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				showCustomKeyboard(v);
			}
		});

		mFromTimeSetListener = new TimePickerDialog.OnTimeSetListener()
		{
			@Override
			public void onTimeSet(TimePicker arg0, int nHourOfDay, int nMinute)
			{
				mnFromHour = nHourOfDay;
				mnFromMinute = nMinute;
				metFromTime.setText(Utils.constructTime(mnFromHour, mnFromMinute, mPreferences));
			}
		};

		metFromTime = (EditText) findViewById(R.id.et_sta_from);
		metFromTime.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				TimePickerDialog timePicker = new TimePickerDialog(ActivityShiftTemplatesAdd.this, mFromTimeSetListener, mnFromHour, mnFromMinute, mbMilitaryTime);
				timePicker.setTitle(getString(R.string.from));
				timePicker.show();
			}
		});
		metFromTime.setText(Utils.constructTime(mnFromHour, mnFromMinute, mPreferences));

		mToTimeSetListener = new TimePickerDialog.OnTimeSetListener()
		{
			@Override
			public void onTimeSet(TimePicker arg0, int nHourOfDay, int nMinute)
			{
				mnToHour = nHourOfDay;
				mnToMinute = nMinute;
				metToTime.setText(Utils.constructTime(mnToHour, mnToMinute, mPreferences));
			}
		};

		metToTime = (EditText) findViewById(R.id.et_sta_to);
		metToTime.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				TimePickerDialog timePicker = new TimePickerDialog(ActivityShiftTemplatesAdd.this, mToTimeSetListener, mnToHour, mnToMinute, mbMilitaryTime);
				timePicker.setTitle(getString(R.string.to));
				timePicker.show();
			}
		});
		metToTime.setText(Utils.constructTime(mnToHour, mnToMinute, mPreferences));

		metUnPaidBreak = (EditText) findViewById(R.id.et_sta_details_unpaid_break);
		metUnPaidBreak.setText(Utils.convertMinsIntoString(Konstant.DEFAULT_SHIFT_UNPAID_BREAK));
		metUnPaidBreak.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if (hasFocus)
				{
					metUnPaidBreak.setText(Integer.toString(Utils.convertMinsIntoInt(metUnPaidBreak.getText().toString())));
				}
				else
				{
					metUnPaidBreak.setText(Utils.convertMinsIntoString(metUnPaidBreak.getText().toString()));
				}
			}
		});

		mtvSpecialWage = (TextView) findViewById(R.id.tv_sta_details_special_wage);

		metSpecialWage = (EditText) findViewById(R.id.et_sta_details_special_wage);
		Job job = DBHelper.getInstance(ActivityShiftTemplatesAdd.this).getJob(malnJobIDs.get(mspnJobs.getSelectedItemPosition()));
		metSpecialWage.setText(Utils.convertRateIntoString(job.getPay(), Konstant.DEFAULT_SHIFT_SPECIAL_WAGE));
		metSpecialWage.setTag(metSpecialWage.getKeyListener());
		metSpecialWage.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if (hasFocus)
				{
					metSpecialWage.setText(Double.toString(Utils.convertRateIntoDouble(metSpecialWage.getText().toString())));
				}
				else
				{
					Job job = DBHelper.getInstance(ActivityShiftTemplatesAdd.this).getJob(malnJobIDs.get(mspnJobs.getSelectedItemPosition()));
					metSpecialWage.setText(Utils.convertRateIntoString(Double.toString(job.getPay()), metSpecialWage.getText().toString()));
				}
			}
		});

		mtvTip = (TextView) findViewById(R.id.tv_sta_details_tip);

		metTip = (EditText) findViewById(R.id.et_sta_details_tip);
		metTip.setText(Utils.convertAmountIntoString(Konstant.DEFAULT_SHIFT_TIP));
		metTip.setTag(metTip.getKeyListener());
		metTip.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if (hasFocus)
				{
					metTip.setText(Double.toString(Utils.convertAmountIntoDouble(metTip.getText().toString())));
				}
				else
				{
					metTip.setText(Utils.convertAmountIntoString(metTip.getText().toString()));
				}
			}
		});

		mtvNotes = (TextView) findViewById(R.id.tv_sta_notes);

		metNotes = (EditText) findViewById(R.id.et_sta_notes);
		metNotes.setText(Konstant.DEFAULT_SHIFT_NOTES);
		metNotes.setTag(metNotes.getKeyListener());

		mtvReminders = (TextView) findViewById(R.id.tv_sta_remainders);

		mllRemainders = (LinearLayout) findViewById(R.id.ll_sta_remainders);

		final TextView tvNoRemainder = (TextView) findViewById(R.id.tv_sta_remainders_not_found);

		final ArrayAdapter<String> adapterRemainders = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Konstant.REMAINDERS);
		adapterRemainders.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		mbtnAddRemainder = (Button) findViewById(R.id.btn_sta_add_remainder);
		mbtnAddRemainder.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);
				if (bIsPremium)
				{
					UtilsCalendar.mlCalendarID = mPreferences.getLong(Konstant.PREF_KEY_CALENDAR_ID, Konstant.PREF_DEF_CALENDAR_ID);
					if (UtilsCalendar.mlCalendarID == Konstant.PREF_DEF_CALENDAR_ID)
					{
						Utils.setupCalendarID(ActivityShiftTemplatesAdd.this, mPreferences);
					}
					else
					{
						final LinearLayout llSingleRemainder = new LinearLayout(ActivityShiftTemplatesAdd.this);

						int nPadding = getResources().getInteger(R.integer.padding_s);

						LinearLayout.LayoutParams lpSpinner = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, (float) 1.0);
						lpSpinner.gravity = Gravity.CENTER_VERTICAL;
						lpSpinner.setMargins(nPadding, nPadding, nPadding, nPadding);

						final Spinner spnRemainders = new Spinner(ActivityShiftTemplatesAdd.this);
						spnRemainders.setAdapter(adapterRemainders);
						spnRemainders.setLayoutParams(lpSpinner);
						llSingleRemainder.addView(spnRemainders);

						LinearLayout.LayoutParams lpDelete = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
						lpDelete.gravity = Gravity.CENTER_VERTICAL;
						lpDelete.setMargins(nPadding, nPadding, nPadding, nPadding);

						ImageView ivDelete = new ImageView(ActivityShiftTemplatesAdd.this);
						ivDelete.setImageResource(R.drawable.btn_delete);
						ivDelete.setLayoutParams(lpDelete);
						llSingleRemainder.addView(ivDelete);

						mllRemainders.addView(llSingleRemainder);

						malRemainderSpinners.add(spnRemainders);
						tvNoRemainder.setVisibility(View.GONE);

						ivDelete.setOnClickListener(new OnClickListener()
						{
							@Override
							public void onClick(View v)
							{
								malRemainderSpinners.remove(spnRemainders);
								if (malRemainderSpinners.isEmpty())
								{
									tvNoRemainder.setVisibility(View.VISIBLE);
								}
								mllRemainders.removeView(llSingleRemainder);
							}
						});
					}
				}
			}
		});

		Button btnDeleteShiftTemplate = (Button) findViewById(R.id.btn_sta_delete_shift_template);
		btnDeleteShiftTemplate.setVisibility(View.GONE);
		btnDeleteShiftTemplate.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(ActivityShiftTemplatesAdd.this);
				builder.setTitle(R.string.delete_shift_template);
				builder.setMessage(R.string.msg_prompt_shift_template_delete);
				builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int id)
					{
						DBHelper.getInstance(ActivityShiftTemplatesAdd.this).deleteShiftTemplate(mnShiftTemplateID);
						DBHelper.getInstance(ActivityShiftTemplatesAdd.this).deleteShiftTemplateRemainders(mnShiftTemplateID);

						ActivityHome.setShiftTrayChanged(true);

						finish();
					}
				});
				builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int id)
					{
					}
				});
				builder.create();
				builder.show();
			}
		});

		Intent intent = getIntent();
		if (intent != null)
		{
			if (intent.hasExtra(Konstant.KEY_SELECTED_SHIFT_TEMPLATE_ID))
			{
				tvActivityTitle.setText(getString(R.string.edit_shift_template));

				int nShiftTemplateID = intent.getIntExtra(Konstant.KEY_SELECTED_SHIFT_TEMPLATE_ID, -1);
				ShiftTemplate shiftTemplate = DBHelper.getInstance(ActivityShiftTemplatesAdd.this).getShiftTemplate(nShiftTemplateID);

				getActionBar().setTitle(getString(R.string.edit_shift_template));
				mbEditMode = true;
				btnDeleteShiftTemplate.setVisibility(View.VISIBLE);

				ArrayList<ShiftTemplateRemainder> alSTR = DBHelper.getInstance(ActivityShiftTemplatesAdd.this).getShiftTemplateRemainders(nShiftTemplateID);

				if (!alSTR.isEmpty())
					tvNoRemainder.setVisibility(View.GONE);

				for (int i = 0; i < alSTR.size(); i++)
				{
					final LinearLayout llSingleRemainder = new LinearLayout(ActivityShiftTemplatesAdd.this);

					int nPadding = getResources().getInteger(R.integer.padding_s);

					LinearLayout.LayoutParams lpSpinner = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, (float) 1.0);
					lpSpinner.gravity = Gravity.CENTER_VERTICAL;
					lpSpinner.setMargins(nPadding, nPadding, nPadding, nPadding);

					final Spinner spnRemainders = new Spinner(ActivityShiftTemplatesAdd.this);
					spnRemainders.setAdapter(adapterRemainders);
					spnRemainders.setLayoutParams(lpSpinner);
					spnRemainders.setSelection(alSTR.get(i).getRemainderTimeIndex());
					malRemainderSpinners.add(spnRemainders);
					llSingleRemainder.addView(spnRemainders);

					LinearLayout.LayoutParams lpDelete = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					lpDelete.gravity = Gravity.CENTER_VERTICAL;
					lpDelete.setMargins(nPadding, nPadding, nPadding, nPadding);

					ImageView ivDelete = new ImageView(ActivityShiftTemplatesAdd.this);
					ivDelete.setImageResource(R.drawable.btn_delete);
					ivDelete.setLayoutParams(lpDelete);
					llSingleRemainder.addView(ivDelete);

					mllRemainders.addView(llSingleRemainder);

					ivDelete.setOnClickListener(new OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							malRemainderSpinners.remove(spnRemainders);
							if (malRemainderSpinners.isEmpty())
							{
								tvNoRemainder.setVisibility(View.VISIBLE);
							}
							mllRemainders.removeView(llSingleRemainder);
						}
					});
				}

				setShiftTemplateDetails(shiftTemplate);
			}
		}

		checkIsAppPremium();
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		checkIsAppPremium();
	}

	@Override
	public void onBackPressed()
	{
		if (isCustomKeyboardVisible())
			hideCustomKeyboard();
		else
			this.finish();
	}

	private void checkIsAppPremium()
	{
		boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);
		if (bIsPremium)
		{
			mtvSpecialWage.setTextColor(getResources().getColor(android.R.color.white));
			mtvSpecialWage.setOnClickListener(null);
			mtvTip.setTextColor(getResources().getColor(android.R.color.white));
			mtvTip.setOnClickListener(null);
			mtvNotes.setTextColor(getResources().getColor(android.R.color.white));
			mtvNotes.setOnClickListener(null);
			mtvReminders.setTextColor(getResources().getColor(android.R.color.white));
			mtvReminders.setOnClickListener(null);

			metSpecialWage.setVisibility(View.VISIBLE);
			metTip.setVisibility(View.VISIBLE);
			metNotes.setVisibility(View.VISIBLE);
			mllRemainders.setVisibility(View.VISIBLE);
			mbtnAddRemainder.setVisibility(View.VISIBLE);

			mibUpgrade.setVisibility(View.GONE);
			mibUpgradeDummy.setVisibility(View.GONE);
			mtvUpgradeOnSpecialWage.setVisibility(View.GONE);
			mtvUpgradeOnNotes.setVisibility(View.GONE);
			mtvUpgradeOnReminders.setVisibility(View.GONE);
		}
		else
		{
			mtvSpecialWage.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftTemplatesAdd.this, ActivityShiftTemplatesAdd.this);
					dialogUpgrade.displayDialog();
				}
			});
			mtvTip.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftTemplatesAdd.this, ActivityShiftTemplatesAdd.this);
					dialogUpgrade.displayDialog();
				}
			});
			mtvNotes.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftTemplatesAdd.this, ActivityShiftTemplatesAdd.this);
					dialogUpgrade.displayDialog();
				}
			});
			mtvReminders.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftTemplatesAdd.this, ActivityShiftTemplatesAdd.this);
					dialogUpgrade.displayDialog();
				}
			});

			metSpecialWage.setVisibility(View.GONE);
			metTip.setVisibility(View.GONE);
			metNotes.setVisibility(View.GONE);
			mllRemainders.setVisibility(View.GONE);
			mbtnAddRemainder.setVisibility(View.GONE);

			mibUpgrade.setVisibility(View.VISIBLE);
			mibUpgradeDummy.setVisibility(View.VISIBLE);
			mtvUpgradeOnSpecialWage.setVisibility(View.VISIBLE);
			mtvUpgradeOnNotes.setVisibility(View.VISIBLE);
			mtvUpgradeOnReminders.setVisibility(View.VISIBLE);
		}
	}

	private boolean saveShiftTemplate()
	{
		try
		{
			int nJobID = malnJobIDs.get(mspnJobs.getSelectedItemPosition());

			int nUnPaidBreak = Utils.convertMinsIntoInt(metUnPaidBreak.getText().toString());

			int nPaidMinutes = 0;
			int nStart = mnFromHour * 60 + mnFromMinute;
			int nEnd = mnToHour * 60 + mnToMinute;
			if (nStart <= nEnd)
			{
				nPaidMinutes = nEnd - nStart - nUnPaidBreak;
			}
			else
			{
				nPaidMinutes = (24 * 60 - nStart) + nEnd;
			}

			if (nPaidMinutes <= 0)
			{
				Toast.makeText(ActivityShiftTemplatesAdd.this, getString(R.string.msg_shift_invalid_time), Toast.LENGTH_LONG).show();
			}
			else
			{
				double dSpecialWage = Utils.convertRateIntoDouble(metSpecialWage.getText().toString());
				double dTip = Utils.convertAmountIntoDouble(metTip.getText().toString());

				String strNotes = metNotes.getText().toString();

				final ShiftTemplate shiftTemplate = new ShiftTemplate();
				shiftTemplate.setJobID(nJobID);
				shiftTemplate.setColor(mnSelectedColor);
				shiftTemplate.setIcon(mnSelectedIcon);
				shiftTemplate.setFromHour(mnFromHour);
				shiftTemplate.setFromMinute(mnFromMinute);
				shiftTemplate.setToHour(mnToHour);
				shiftTemplate.setToMinute(mnToMinute);
				shiftTemplate.setUnPaidBreak(nUnPaidBreak);
				shiftTemplate.setSpecialWage(dSpecialWage);
				shiftTemplate.setTip(dTip);
				shiftTemplate.setNotes(strNotes);
				shiftTemplate.setPaidMinutes(nPaidMinutes);

				boolean bIdenticalShiftTemplate = DBHelper.getInstance(ActivityShiftTemplatesAdd.this).isThereAnyIdenticalShiftTemplate(shiftTemplate);
				if (bIdenticalShiftTemplate)
				{
					AlertDialog.Builder dialogSaveAnyways = new AlertDialog.Builder(ActivityShiftTemplatesAdd.this);
					dialogSaveAnyways.setTitle(R.string.save_anyways);
					dialogSaveAnyways.setMessage(R.string.msg_st_already_exists);
					dialogSaveAnyways.setPositiveButton(R.string.save, new DialogInterface.OnClickListener()
					{
						public void onClick(DialogInterface dialog, int id)
						{
							saveShiftTemplateAnyways(shiftTemplate);
						}
					});
					dialogSaveAnyways.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()
					{
						public void onClick(DialogInterface dialog, int id)
						{
							finish();
						}
					});
					dialogSaveAnyways.show();
				}
				else
				{
					saveShiftTemplateAnyways(shiftTemplate);
				}
			}
		}
		catch (NumberFormatException nfe)
		{
			Toast.makeText(ActivityShiftTemplatesAdd.this, getString(R.string.msg_invalid_entry), Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	private void saveShiftTemplateAnyways(ShiftTemplate shiftTemplate)
	{
		if (mbEditMode)
		{
			shiftTemplate.setID(mnShiftTemplateID);
			DBHelper.getInstance(ActivityShiftTemplatesAdd.this).deleteShiftTemplateRemainders(mnShiftTemplateID);

			if (DBHelper.getInstance(ActivityShiftTemplatesAdd.this).updateShiftTemplate(shiftTemplate))
			{
				for (int i = 0; i < malRemainderSpinners.size(); i++)
				{
					int nIndex = malRemainderSpinners.get(i).getSelectedItemPosition();
					ShiftTemplateRemainder srt = new ShiftTemplateRemainder();
					srt.setShiftTemplateID(mnShiftTemplateID);
					srt.setRemainderTimeIndex(nIndex);
					DBHelper.getInstance(ActivityShiftTemplatesAdd.this).addShiftTemplateRemainder(srt);
				}
				ActivityShiftTemplates.notifyShiftTemplatesChanged();
				ActivityHome.setShiftTrayChanged(true);

				finish();
			}
			else
			{
				Toast.makeText(ActivityShiftTemplatesAdd.this, getString(R.string.msg_st_edit_fail), Toast.LENGTH_LONG).show();
			}
		}
		else
		{
			if (DBHelper.getInstance(ActivityShiftTemplatesAdd.this).addShiftTemplate(shiftTemplate))
			{
				int nReturnValue = DBHelper.getInstance(ActivityShiftTemplatesAdd.this).getMaxShiftTemplateID();
				if (nReturnValue > 0)
				{
					for (int i = 0; i < malRemainderSpinners.size(); i++)
					{
						int nIndex = malRemainderSpinners.get(i).getSelectedItemPosition();
						ShiftTemplateRemainder srt = new ShiftTemplateRemainder();
						srt.setShiftTemplateID(nReturnValue);
						srt.setRemainderTimeIndex(nIndex);
						DBHelper.getInstance(ActivityShiftTemplatesAdd.this).addShiftTemplateRemainder(srt);
					}
				}
				ActivityShiftTemplates.notifyShiftTemplatesChanged();
				ActivityHome.setShiftTrayChanged(true);

				finish();
			}
			else
			{
				Toast.makeText(ActivityShiftTemplatesAdd.this, getString(R.string.msg_st_add_fail), Toast.LENGTH_LONG).show();
			}
		}
	}

	private void setShiftTemplateDetails(ShiftTemplate shiftTemplate)
	{
		mnShiftTemplateID = shiftTemplate.getID();

		mspnJobs.setSelection(malnJobIDs.indexOf(shiftTemplate.getJobID()));

		mnFromHour = shiftTemplate.getFromHour();
		mnFromMinute = shiftTemplate.getFromMinute();
		metFromTime.setText(Utils.constructTime(mnFromHour, mnFromMinute, mPreferences));

		mnToHour = shiftTemplate.getToHour();
		mnToMinute = shiftTemplate.getToMinute();
		metToTime.setText(Utils.constructTime(mnToHour, mnToMinute, mPreferences));

		metUnPaidBreak.setText(Utils.convertMinsIntoString(shiftTemplate.getUnPaidBreak()));

		Job job = DBHelper.getInstance(ActivityShiftTemplatesAdd.this).getJob(shiftTemplate.getJobID());
		metSpecialWage.setText(Utils.convertRateIntoString(job.getPay(), shiftTemplate.getSpecialWage()));

		metTip.setText(Utils.convertAmountIntoString(shiftTemplate.getTip()));

		metNotes.setText(shiftTemplate.getNotes());

		mnSelectedIcon = shiftTemplate.getIcon();
		mivIcon.setImageResource(Utils.getShiftTemplateIcon(mnSelectedIcon));

		setShiftTemplateColor(shiftTemplate.getColor());
	}

	private void setShiftTemplateColor(int nColor)
	{
		switch (nColor)
		{
		case Konstant.COLOR_PURPLE:
			mnSelectedColor = Konstant.COLOR_PURPLE;
			mivPurple.setImageResource(R.drawable.color_selected);
			mivCyan.setImageResource(0);
			mivGreen.setImageResource(0);
			mivMustard.setImageResource(0);
			mivOrange.setImageResource(0);
			mivRed.setImageResource(0);
			break;
		case Konstant.COLOR_CYAN:
			mnSelectedColor = Konstant.COLOR_CYAN;
			mivPurple.setImageResource(0);
			mivCyan.setImageResource(R.drawable.color_selected);
			mivGreen.setImageResource(0);
			mivMustard.setImageResource(0);
			mivOrange.setImageResource(0);
			mivRed.setImageResource(0);
			break;
		case Konstant.COLOR_GREEN:
			mnSelectedColor = Konstant.COLOR_GREEN;
			mivPurple.setImageResource(0);
			mivCyan.setImageResource(0);
			mivGreen.setImageResource(R.drawable.color_selected);
			mivMustard.setImageResource(0);
			mivOrange.setImageResource(0);
			mivRed.setImageResource(0);
			break;
		case Konstant.COLOR_MUSTARD:
			mnSelectedColor = Konstant.COLOR_MUSTARD;
			mivPurple.setImageResource(0);
			mivCyan.setImageResource(0);
			mivGreen.setImageResource(0);
			mivMustard.setImageResource(R.drawable.color_selected);
			mivOrange.setImageResource(0);
			mivRed.setImageResource(0);
			break;
		case Konstant.COLOR_ORANGE:
			mnSelectedColor = Konstant.COLOR_ORANGE;
			mivPurple.setImageResource(0);
			mivCyan.setImageResource(0);
			mivGreen.setImageResource(0);
			mivMustard.setImageResource(0);
			mivOrange.setImageResource(R.drawable.color_selected);
			mivRed.setImageResource(0);
			break;
		case Konstant.COLOR_RED:
			mnSelectedColor = Konstant.COLOR_RED;
			mivPurple.setImageResource(0);
			mivCyan.setImageResource(0);
			mivGreen.setImageResource(0);
			mivMustard.setImageResource(0);
			mivOrange.setImageResource(0);
			mivRed.setImageResource(R.drawable.color_selected);
			break;
		}
	}

	private void hideCustomKeyboard()
	{
		mKeyboardView.setVisibility(View.GONE);
		mKeyboardView.setEnabled(false);
	}

	private void showCustomKeyboard(View v)
	{
		mKeyboardView.setVisibility(View.VISIBLE);
		mKeyboardView.setEnabled(true);
		if (v != null)
			((InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	private boolean isCustomKeyboardVisible()
	{
		return mKeyboardView.getVisibility() == View.VISIBLE;
	}
}