/*
 * ActivityHome
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.billing.utils.IabHelper;
import com.android.vending.billing.utils.IabResult;
import com.android.vending.billing.utils.Inventory;

import eamp.droid.paycal.model.DailyPiggyDetails;
import eamp.droid.paycal.model.DailyShiftDetails;
import eamp.droid.paycal.model.DailyTallyDetails;
import eamp.droid.paycal.model.Job;
import eamp.droid.paycal.model.ShiftInstance;
import eamp.droid.paycal.model.ShiftInstanceContinuing;
import eamp.droid.paycal.model.ShiftInstanceReminder;
import eamp.droid.paycal.model.ShiftTemplate;

public class ActivityHome extends Activity
{
	// Custom Calendar View
	private Calendar mCalendar;
	private AdapterCalendar mCalendarAdapter;
	// private Handler mHandler;
	private ArrayList<String> malsItems;

	// Misc

	private static boolean mbShiftTrayChanged;

	private boolean mbShiftTrayExpanded;

	private int mnSelectedDatePosition;

	private int mnSelectedShiftTemplateIndex;

	private int mnSelectedYear;
	private int mnSelectedMOY;
	private int mnSelectedDOM;

	private int mnTodaysYear;
	private int mnTodaysMOY;
	private int mnTodaysDOM;

	private int mnCurrentYear;
	private int mnCurrentMOY;

	// Recently Added Shift
	private static int mnRecentShiftTemplateID = -1;
	private static int mnRecentShiftID = -1;
	private static int mnRecentShiftYear = -1;
	private static int mnRecentShiftMOY = -1;
	private static int mnRecentShiftDOM = -1;

	private ArrayList<Integer> malnShiftTemplateIDs;

	private ArrayList<ImageView> malivSTIcons;
	private ArrayList<ImageView> malivSTSelectors;

	private AdapterDailyDetails mDailyDetailsAdapter;

	private SharedPreferences mPreferences;
	private SharedPreferences.Editor mPrefEditor;

	// In-App-Billing

	private IabHelper mIabHelper;
	private IabHelper.QueryInventoryFinishedListener mInitialInventoryListener;

	// View Controls

	private TextView mtvCABTitle;
	private DatePickerDialog.OnDateSetListener mdslSelect;
	private DatePickerDialog mdpdSelect;

	private ImageButton mibCollapseExpand;
	private ImageButton mibAddShiftInstance;
	private ImageButton mibSettings;

	private RelativeLayout mrlShiftTray;
	private LinearLayout mllShiftTemplates;
	private ImageButton mibAddShiftTemplate;

	private GridView mgvCalendar;

	private RelativeLayout mrlTutorial;
	private ImageButton mibEditJobs;
	private ListView mlvDailyDetails;

	private ImageButton mibPreviousMonth;
	private ImageButton mibNextMonth;

	private RotateAnimation mAnimationCollapse;
	private RotateAnimation mAnimationExpand;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		ActionBar currentActionBar = getActionBar();
		currentActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		currentActionBar.setDisplayShowTitleEnabled(false);
		currentActionBar.setCustomView(R.layout.action_bar_items_3);

		mtvCABTitle = (TextView) findViewById(R.id.tv_ab3_title);

		mPreferences = getSharedPreferences(Konstant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		mPrefEditor = mPreferences.edit();

		mIabHelper = new IabHelper(this, Konstant.APPLICATION_PUBLIC_KEY);
		mIabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener()
		{
			public void onIabSetupFinished(IabResult result)
			{
				if (result.isSuccess())
				{
					mIabHelper.queryInventoryAsync(mInitialInventoryListener);
				}
			}
		});
		mInitialInventoryListener = new IabHelper.QueryInventoryFinishedListener()
		{
			public void onQueryInventoryFinished(IabResult result, Inventory inventory)
			{
				if (result.isSuccess())
				{
					boolean bIsPremium = false;
					bIsPremium = inventory.hasPurchase(Konstant.APPLICATION_SKU_FOR_PREMIUM);
					mPrefEditor.putBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, bIsPremium);
					mPrefEditor.commit();
				}
			}
		};

		mbShiftTrayChanged = false;
		mnSelectedShiftTemplateIndex = -1;

		malivSTIcons = new ArrayList<ImageView>();
		malivSTSelectors = new ArrayList<ImageView>();
		malnShiftTemplateIDs = new ArrayList<Integer>();

		Utils.mbIsTallyOn = mPreferences.getBoolean(Konstant.PREF_KEY_TALLY_ON, Konstant.PREF_DEF_TALLY_ON);
		Utils.msaPiggyDates = Utils.getPiggyDatesMappedByJobID(ActivityHome.this, mPreferences);
		Utils.msaTallyDates = Utils.getTallyDatesMappedByTallyID(ActivityHome.this, mPreferences);

		/*
		 * boolean bSigninRequired =
		 * mPreferences.getBoolean(Konstant.PREF_KEY_PASSCODE_ON,
		 * Konstant.PREF_DEF_PASSCODE_ON); if (bSigninRequired) { DialogSignin
		 * signinDialog = new DialogSignin(ActivityHome.this);
		 * signinDialog.displayDialog(); }
		 */

		UtilsCalendar.mlCalendarID = mPreferences.getLong(Konstant.PREF_KEY_CALENDAR_ID, Konstant.PREF_DEF_CALENDAR_ID);
		if (UtilsCalendar.mlCalendarID == Konstant.PREF_DEF_CALENDAR_ID)
		{
			Utils.setupCalendarID(ActivityHome.this, mPreferences);
		}

		mCalendar = Calendar.getInstance();

		mnCurrentYear = mCalendar.get(Calendar.YEAR);
		mnCurrentMOY = mCalendar.get(Calendar.MONTH);

		mnTodaysYear = mCalendar.get(Calendar.YEAR);
		mnTodaysMOY = mCalendar.get(Calendar.MONTH);
		mnTodaysDOM = mCalendar.get(Calendar.DAY_OF_MONTH);

		mnSelectedYear = mCalendar.get(Calendar.YEAR);
		mnSelectedMOY = mCalendar.get(Calendar.MONTH);
		mnSelectedDOM = mCalendar.get(Calendar.DAY_OF_MONTH);

		onNewIntent(getIntent());

		malsItems = new ArrayList<String>();
		mCalendarAdapter = new AdapterCalendar(this, mCalendar);

		mgvCalendar = (GridView) findViewById(R.id.cv_home_calendar);
		mgvCalendar.setAdapter(mCalendarAdapter);

		updateCalendar();

		mtvCABTitle.setText(android.text.format.DateFormat.format("MMMM yyyy", mCalendar));
		mtvCABTitle.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				launchSelectDatePicker();
			}
		});

		mdslSelect = new DatePickerDialog.OnDateSetListener()
		{
			@Override
			public void onDateSet(DatePicker view, int nYear, int nMOY, int nDOM)
			{
				CalendarDisplay calendarDisplay = new CalendarDisplay(Konstant.DISPLAY_SELECTED_MONTH, nYear, nMOY);
				calendarDisplay.execute();
			}
		};

		mibPreviousMonth = (ImageButton) findViewById(R.id.ib_home_previous_month);
		mibPreviousMonth.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				CalendarDisplay calendarDisplay = new CalendarDisplay(Konstant.DISPLAY_PREVIOUS_MONTH, -1, -1);
				calendarDisplay.execute();
			}
		});

		mibNextMonth = (ImageButton) findViewById(R.id.ib_home_next_month);
		mibNextMonth.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				CalendarDisplay calendarDisplay = new CalendarDisplay(Konstant.DISPLAY_NEXT_MONTH, -1, -1);
				calendarDisplay.execute();
			}
		});

		mgvCalendar.setOnItemClickListener(new OnItemClickListener()
		{
			public void onItemClick(AdapterView<?> parent, View v, int nPosition, long id)
			{
				processDateSelection(v, nPosition);
			}
		});

		mlvDailyDetails = (ListView) findViewById(R.id.lv_home_daily_details);

		mbShiftTrayExpanded = false;

		mrlShiftTray = (RelativeLayout) findViewById(R.id.rl_home_shift_tray);
		mrlShiftTray.setVisibility(View.GONE);

		mAnimationCollapse = new RotateAnimation(180.0f, 0.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		mAnimationCollapse.setInterpolator(new LinearInterpolator());
		mAnimationCollapse.setDuration(250);
		mAnimationCollapse.setFillEnabled(true);
		mAnimationCollapse.setFillAfter(true);
		mAnimationCollapse.setAnimationListener(new AnimationListener()
		{
			@Override
			public void onAnimationStart(Animation animation)
			{
			}

			@Override
			public void onAnimationRepeat(Animation animation)
			{

			}

			@Override
			public void onAnimationEnd(Animation animation)
			{
				mbShiftTrayExpanded = false;
				mrlShiftTray.setVisibility(View.GONE);
			}
		});

		mAnimationExpand = new RotateAnimation(0.0f, 180.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		mAnimationExpand.setInterpolator(new LinearInterpolator());
		mAnimationExpand.setDuration(250);
		mAnimationExpand.setFillEnabled(true);
		mAnimationExpand.setFillAfter(true);
		mAnimationExpand.setAnimationListener(new AnimationListener()
		{
			@Override
			public void onAnimationStart(Animation animation)
			{
			}

			@Override
			public void onAnimationRepeat(Animation animation)
			{

			}

			@Override
			public void onAnimationEnd(Animation animation)
			{
				mbShiftTrayExpanded = true;
				mrlShiftTray.setVisibility(View.VISIBLE);

			}
		});

		mibCollapseExpand = (ImageButton) findViewById(R.id.ib_ab3_exp_col);
		mibCollapseExpand.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (mbShiftTrayExpanded)
				{
					if (isAnyShiftTemplateSelected())
					{
						for (int i = 0; i < malivSTSelectors.size(); i++)
						{
							// Unselect the Shift Template
							malivSTSelectors.get(i).setVisibility(View.GONE);
							mnSelectedShiftTemplateIndex = -1;
						}
					}
					mibCollapseExpand.startAnimation(mAnimationCollapse);
				}
				else
				{
					if (mbShiftTrayChanged)
					{
						fillShiftTray();
						mbShiftTrayChanged = false;

					}
					mibCollapseExpand.startAnimation(mAnimationExpand);
				}
			}

		});

		mibAddShiftInstance = (ImageButton) findViewById(R.id.ib_ab3_add_shift_instance);
		mibAddShiftInstance.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (isAnyShiftTemplateSelected())
				{
					for (int i = 0; i < malivSTSelectors.size(); i++)
					{
						// Unselect the Shift Template
						malivSTSelectors.get(i).setVisibility(View.GONE);
						mnSelectedShiftTemplateIndex = -1;
					}
				}

				addNewShiftInstance();
			}
		});

		mibSettings = (ImageButton) findViewById(R.id.ib_ab3_settings);
		mibSettings.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (isAnyShiftTemplateSelected())
				{
					for (int i = 0; i < malivSTSelectors.size(); i++)
					{
						// Unselect the Shift Template
						malivSTSelectors.get(i).setVisibility(View.GONE);
						mnSelectedShiftTemplateIndex = -1;
					}
				}

				Intent intent = new Intent(ActivityHome.this, ActivitySettings.class);
				startActivity(intent);
			}
		});

		mllShiftTemplates = (LinearLayout) findViewById(R.id.ll_home_shift_tray);

		fillShiftTray();

		ArrayList<Integer> alnDummyDailyDetailsList = new ArrayList<Integer>();

		ArrayList<ShiftInstanceContinuing> alShiftInstanceContinuing = DBHelper.getInstance(ActivityHome.this).getShiftInstancesContinuing(mnSelectedYear, mnSelectedMOY, mnSelectedDOM);
		for (int i = 0; i < alShiftInstanceContinuing.size(); i++)
			alnDummyDailyDetailsList.add(i);

		// Shift Details
		DailyShiftDetails dailyShiftDetails = DBHelper.getInstance(ActivityHome.this).getDailyShiftDetails(mnSelectedYear, mnSelectedMOY, mnSelectedDOM);
		for (int i = 0; i < dailyShiftDetails.getJobNames().size(); i++)
			alnDummyDailyDetailsList.add(i);

		// Piggy Details
		DailyPiggyDetails dailyPiggyDetails = DBHelper.getInstance(ActivityHome.this).getDailyPiggyDetails(mnSelectedYear, mnSelectedMOY, mnSelectedDOM);
		for (int i = 0; i < dailyPiggyDetails.getJobNames().size(); i++)
			alnDummyDailyDetailsList.add(i);

		// Tally Details
		DailyTallyDetails dailyTallyDetails = getDailyTallyDetails();
		for (int i = 0; i < dailyTallyDetails.getTallyIDs().size(); i++)
			alnDummyDailyDetailsList.add(i);

		// Dummy last item for space
		alnDummyDailyDetailsList.add(0);

		mDailyDetailsAdapter = new AdapterDailyDetails(ActivityHome.this, ActivityHome.this, R.layout.list_item_daily_details, alnDummyDailyDetailsList, alShiftInstanceContinuing, dailyShiftDetails, dailyPiggyDetails, dailyTallyDetails, mPreferences);
		mlvDailyDetails.setAdapter(mDailyDetailsAdapter);

		mibAddShiftTemplate = (ImageButton) findViewById(R.id.ib_home_add_shift_template);
		mibAddShiftTemplate.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (isAnyShiftTemplateSelected())
				{
					for (int i = 0; i < malivSTSelectors.size(); i++)
					{
						// Unselect the Shift Template
						malivSTSelectors.get(i).setVisibility(View.GONE);
						mnSelectedShiftTemplateIndex = -1;
					}
				}

				addNewShiftTemplate();

			}
		});

		mrlTutorial = (RelativeLayout) findViewById(R.id.rl_tutorial);

		Animation animation = new AlphaAnimation(1.0f, 0.3f);
		animation.setDuration(500);
		animation.setInterpolator(new LinearInterpolator());
		animation.setRepeatCount(Animation.INFINITE);
		animation.setRepeatMode(Animation.REVERSE);

		mibEditJobs = (ImageButton) findViewById(R.id.ib_home_close_tutorialtutorial_edit_jobs);
		mibEditJobs.startAnimation(animation);
		mibEditJobs.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (isAnyShiftTemplateSelected())
				{
					for (int i = 0; i < malivSTSelectors.size(); i++)
					{
						// Unselect the Shift Template
						malivSTSelectors.get(i).setVisibility(View.GONE);
						mnSelectedShiftTemplateIndex = -1;
					}
				}

				Intent intent = new Intent(ActivityHome.this, ActivityJobs.class);
				startActivity(intent);
			}
		});

		boolean bIsJobsEmpty = DBHelper.getInstance(ActivityHome.this).isJobsEmpty();
		if (bIsJobsEmpty)
			mrlTutorial.setVisibility(View.VISIBLE);
		else
			mrlTutorial.setVisibility(View.GONE);

		boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);
		int nLaunchCount = mPreferences.getInt(Konstant.PREF_KEY_APP_LAUNCH_COUNT, Konstant.PREF_DEF_APP_LAUNCH_COUNT);
		nLaunchCount++;
		mPrefEditor.putInt(Konstant.PREF_KEY_APP_LAUNCH_COUNT, nLaunchCount);
		mPrefEditor.commit();

		if (!bIsPremium)
		{
			if (nLaunchCount % 5 == 0)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityHome.this, ActivityHome.this);
				dialogUpgrade.displayDialog();
			}
		}

	}

	@Override
	protected void onResume()
	{
		super.onResume();

		// Collapse Shift Tray
		mbShiftTrayExpanded = false;
		mrlShiftTray.setVisibility(View.GONE);
		mibCollapseExpand.setImageDrawable(getResources().getDrawable(R.drawable.shift_tray));

		if (isAnyShiftTemplateSelected())
		{
			for (int i = 0; i < malivSTSelectors.size(); i++)
			{
				// Unselect the Shift Template
				malivSTSelectors.get(i).setVisibility(View.GONE);
				mnSelectedShiftTemplateIndex = -1;
			}
		}

		refreshHomeScreen();
	}

	public void refreshHomeScreen()
	{
		boolean bIsJobsEmpty = DBHelper.getInstance(ActivityHome.this).isJobsEmpty();
		if (bIsJobsEmpty)
			mrlTutorial.setVisibility(View.VISIBLE);
		else
			mrlTutorial.setVisibility(View.GONE);

		if (mbShiftTrayChanged)
		{
			fillShiftTray();
			mbShiftTrayChanged = false;
		}

		refreshCalendar();

		mCalendarAdapter.setDateSelected(mnSelectedYear, (mnSelectedMOY - 1), mnSelectedDOM);
		mCalendarAdapter.notifyDataSetChanged();

		mDailyDetailsAdapter.notifyDataSetChanged();

		updateDailyDetails();
	}

	private void launchSelectDatePicker()
	{
		if ((mdpdSelect != null) && mdpdSelect.isShowing())
			mdpdSelect.cancel();

		mdpdSelect = new DatePickerDialog(ActivityHome.this, mdslSelect, mnCurrentYear, mnCurrentMOY, 1);
		try
		{
			Field[] datePickerDialogFields = mdpdSelect.getClass().getDeclaredFields();
			for (Field datePickerDialogField : datePickerDialogFields)
			{
				if (datePickerDialogField.getName().equals("mDatePicker"))
				{
					datePickerDialogField.setAccessible(true);
					DatePicker datePicker = (DatePicker) datePickerDialogField.get(mdpdSelect);
					Field datePickerFields[] = datePickerDialogField.getType().getDeclaredFields();
					for (Field datePickerField : datePickerFields)
					{
						if ("mDaySpinner".equals(datePickerField.getName()))
						{
							datePickerField.setAccessible(true);
							Object dayPicker = new Object();
							dayPicker = datePickerField.get(datePicker);
							((View) dayPicker).setVisibility(View.GONE);
						}
					}
				}
			}
		}
		catch (Exception ex)
		{
		}
		mdpdSelect.setTitle(getString(R.string.select));
		mdpdSelect.show();
	}

	private void addNewShiftTemplate()
	{
		ArrayList<Integer> alnJobIDs = DBHelper.getInstance(ActivityHome.this).getColIDsFromTabJobs();
		if (alnJobIDs.isEmpty())
		{
			Toast.makeText(ActivityHome.this, ActivityHome.this.getString(R.string.msg_st_no_job_error), Toast.LENGTH_LONG).show();

			Intent intent = new Intent(ActivityHome.this, ActivityJobsAdd.class);
			startActivity(intent);

			return;
		}
		Intent intent = new Intent(ActivityHome.this, ActivityShiftTemplatesAdd.class);
		startActivity(intent);
	}

	private void addNewShiftInstance()
	{
		ArrayList<Integer> alnJobIDs = DBHelper.getInstance(ActivityHome.this).getColIDsFromTabJobs();
		if (alnJobIDs.isEmpty())
		{
			Toast.makeText(ActivityHome.this, ActivityHome.this.getString(R.string.msg_si_no_job_error), Toast.LENGTH_LONG).show();

			Intent intent = new Intent(ActivityHome.this, ActivityJobsAdd.class);
			startActivity(intent);

			return;
		}
		Intent intent = new Intent(ActivityHome.this, ActivityShiftInstancesAdd.class);
		intent.putExtra(Konstant.KEY_SELECTED_DATE_YEAR, mnCurrentYear);
		intent.putExtra(Konstant.KEY_SELECTED_DATE_MOY, mnCurrentMOY);
		intent.putExtra(Konstant.KEY_SELECTED_DATE_DOM, mnSelectedDOM);
		startActivity(intent);
	}

	private void placeShiftInstance(int nSelectedShiftTemplateID)
	{
		int nJobID = DBHelper.getInstance(ActivityHome.this).getJobIDFromTabShiftTemplates(nSelectedShiftTemplateID);

		Job job = DBHelper.getInstance(ActivityHome.this).getJob(nJobID);

		Calendar currentCalendar = Calendar.getInstance();
		currentCalendar.set(mnSelectedYear, (mnSelectedMOY - 1), mnSelectedDOM);

		Calendar jobCalendar = Calendar.getInstance();
		jobCalendar.set(job.getPayBeganYear(), job.getPayBeganMOY(), job.getPayBeganDOM());

		if (currentCalendar.before(jobCalendar))
		{
			Toast.makeText(ActivityHome.this, getString(R.string.msg_no_shift_before_job_began), Toast.LENGTH_SHORT).show();
		}
		else
		{
			ShiftTemplate st = DBHelper.getInstance(ActivityHome.this).getShiftTemplate(nSelectedShiftTemplateID);
			int nColor = st.getColor();
			int nFromHour = st.getFromHour();
			int nFromMinute = st.getFromMinute();
			int nToHour = st.getToHour();
			int nToMinute = st.getToMinute();
			int nUnPaidBreak = st.getUnPaidBreak();
			double dSpecialWage = st.getSpecialWage();
			double dTip = st.getTip();
			String strNotes = st.getNotes();
			int nPaidMinutes = st.getPaidMinutes();

			int nStartYear = mnSelectedYear;
			int nStartMOY = mnSelectedMOY - 1;
			int nStartDOM = mnSelectedDOM;

			boolean bShiftAlreadyAdded = DBHelper.getInstance(ActivityHome.this).isShiftAlreadyAdded(nJobID, mnSelectedYear, mnSelectedMOY, mnSelectedDOM, nFromHour, nFromMinute, nToHour, nToMinute);
			if (bShiftAlreadyAdded)
			{
				Toast.makeText(ActivityHome.this, getString(R.string.msg_si_already_added), Toast.LENGTH_SHORT).show();
				return;
			}

			Calendar calendar = Calendar.getInstance();
			calendar.set(mnSelectedYear, mnSelectedMOY, mnSelectedDOM);
			if (nFromHour > nToHour)
				calendar.add(Calendar.DAY_OF_YEAR, 1);

			int nEndYear = calendar.get(Calendar.YEAR);
			int nEndMOY = calendar.get(Calendar.MONTH) - 1;
			int nEndDOM = calendar.get(Calendar.DAY_OF_MONTH);

			boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);

			long lCalendarEventID = -1;
			if (bIsPremium)
				lCalendarEventID = UtilsCalendar.addShiftEvent(getContentResolver(), job.getName(), nStartYear, nStartMOY, nStartDOM, nFromHour, nFromMinute, nEndYear, nEndMOY, nEndDOM, nToHour, nToMinute);

			if ((bIsPremium && (lCalendarEventID > 0)) || (!bIsPremium && (lCalendarEventID == -1)))
			{
				ShiftInstance shiftInstance = new ShiftInstance();
				shiftInstance.setJobID(nJobID);
				shiftInstance.setColor(nColor);
				shiftInstance.setDateYear(mnSelectedYear);
				shiftInstance.setDateMOY(mnSelectedMOY);
				shiftInstance.setDateDOM(mnSelectedDOM);
				shiftInstance.setFromHour(nFromHour);
				shiftInstance.setFromMinute(nFromMinute);
				shiftInstance.setToHour(nToHour);
				shiftInstance.setToMinute(nToMinute);
				shiftInstance.setUnPaidBreak(nUnPaidBreak);
				shiftInstance.setSpecialWage(dSpecialWage);
				shiftInstance.setTip(dTip);
				shiftInstance.setNotes(strNotes);
				shiftInstance.setCalendarEventID(lCalendarEventID);
				shiftInstance.setPaidMinutes(nPaidMinutes);

				if (DBHelper.getInstance(ActivityHome.this).addShiftInstance(shiftInstance))
				{
					int nGeneratedShiftInstanceID = DBHelper.getInstance(ActivityHome.this).getMaxShiftInstanceID();

					mnRecentShiftTemplateID = nSelectedShiftTemplateID;
					mnRecentShiftID = nGeneratedShiftInstanceID;
					mnRecentShiftYear = mnSelectedYear;
					mnRecentShiftMOY = mnSelectedMOY;
					mnRecentShiftDOM = mnSelectedDOM;

					if (nGeneratedShiftInstanceID > 0)
					{
						ArrayList<Integer> alnShiftTemplateReminderIndices = DBHelper.getInstance(ActivityHome.this).getColIndexFromTabShiftTemplateReminders(nSelectedShiftTemplateID);
						for (int i = 0; i < alnShiftTemplateReminderIndices.size(); i++)
						{
							int nReminderIndex = alnShiftTemplateReminderIndices.get(i);
							ShiftInstanceReminder siir = new ShiftInstanceReminder();
							siir.setShiftInstanceID(nGeneratedShiftInstanceID);
							siir.setRemainderTimeIndex(nReminderIndex);

							long lCalendarReminderID = UtilsCalendar.addShiftRemainder(getContentResolver(), lCalendarEventID, nReminderIndex);
							if (lCalendarReminderID > 0)
							{
								DBHelper.getInstance(ActivityHome.this).addShiftInstanceReminder(siir);
							}
							else
							{
								Toast.makeText(ActivityHome.this, getString(R.string.msg_si_add_fail), Toast.LENGTH_SHORT).show();
							}
						}

						if (nFromHour > nToHour)
						{
							ShiftInstanceContinuing shiftInstanceContinuing = new ShiftInstanceContinuing();
							shiftInstanceContinuing.setShiftInstanceID(nGeneratedShiftInstanceID);
							shiftInstanceContinuing.setJobID(nJobID);
							shiftInstanceContinuing.setColor(nColor);
							shiftInstanceContinuing.setDateYear(nEndYear);
							shiftInstanceContinuing.setDateMOY((nEndMOY + 1));
							shiftInstanceContinuing.setDateDOM(nEndDOM);
							shiftInstanceContinuing.setToHour(nToHour);
							shiftInstanceContinuing.setToMinute(nToMinute);

							DBHelper.getInstance(ActivityHome.this).addShiftInstanceContinuing(shiftInstanceContinuing);
						}
					}
				}
				else
				// [if DBHelper's addShiftInstance fails]
				{
					Toast.makeText(ActivityHome.this, getString(R.string.msg_si_add_fail), Toast.LENGTH_SHORT).show();
				}
			}
			else
			// [if (lCalendarEventID <= 0)]
			{
				Toast.makeText(ActivityHome.this, getString(R.string.msg_si_add_fail), Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void clearDateSelection()
	{
		int nNumOfChildren = mgvCalendar.getChildCount();
		for (int i = 0; i < nNumOfChildren; i++)
		{
			RelativeLayout rlDate = (RelativeLayout) mgvCalendar.getChildAt(i);
			if (rlDate != null)
			{
				TextView tvDate = (TextView) rlDate.getChildAt(0);
				if (tvDate != null)
				{
					tvDate.setSelected(false);
					tvDate.setTextColor(getResources().getColor(R.color.calendar_text_normal));
					if ((i == mnTodaysDOM) && (mnCurrentMOY == mnTodaysMOY) && (mnCurrentYear == mnTodaysYear))
						tvDate.setBackgroundResource(R.color.calendar_item_bg_today);
					else
						tvDate.setBackgroundResource(R.color.calendar_item_bg_normal);
				}

				ImageView ivPiggy = (ImageView) rlDate.getChildAt(1);
				if (ivPiggy != null)
				{
					ivPiggy.setImageResource(R.drawable.cal_piggy_normal);
				}

				ImageView ivTally = (ImageView) rlDate.getChildAt(2);
				if (ivTally != null)
				{
					ivTally.setImageResource(R.drawable.cal_tally_normal);
				}
			}
		}
	}

	private void fillShiftTray()
	{
		mllShiftTemplates.removeAllViews();

		malivSTIcons.clear();
		malivSTSelectors.clear();

		if (malnShiftTemplateIDs != null)
			malnShiftTemplateIDs.clear();
		malnShiftTemplateIDs = DBHelper.getInstance(ActivityHome.this).getColIDsFromTabShiftTemplates();

		final ArrayList<Integer> alnShiftTemplateColors = DBHelper.getInstance(ActivityHome.this).getColColorsFromTabShiftTemplates();
		ArrayList<Integer> alnShiftTemplateIcons = DBHelper.getInstance(ActivityHome.this).getColIconsFromTabShiftTemplates();
		for (int i = 0; i < alnShiftTemplateColors.size(); i++)
		{
			final int nPosition = i;

			int nWidth = getResources().getInteger(R.integer.size_shift_try_icon);
			int nHeight = getResources().getInteger(R.integer.size_shift_try_icon);
			int nPaddingXS = getResources().getInteger(R.integer.padding_xs);
			int nPaddingL = getResources().getInteger(R.integer.padding_xs);

			LinearLayout.LayoutParams lp4Layout = new LinearLayout.LayoutParams(nWidth, nHeight);
			lp4Layout.setMargins(nPaddingXS, 0, nPaddingXS, 0);
			lp4Layout.gravity = Gravity.CENTER;

			final RelativeLayout rlShiftTemplate = new RelativeLayout(ActivityHome.this);
			rlShiftTemplate.setBackgroundColor(getResources().getColor(Utils.getAndroidColor(alnShiftTemplateColors.get(i))));
			rlShiftTemplate.setLayoutParams(lp4Layout);

			LinearLayout.LayoutParams lp4IVSTIcons = new LinearLayout.LayoutParams(nWidth - 2 * nPaddingL, nHeight - 2 * nPaddingL);
			lp4IVSTIcons.gravity = Gravity.CENTER;

			ImageView ivSTIcon = new ImageView(ActivityHome.this);
			ivSTIcon.setLayoutParams(lp4IVSTIcons);
			ivSTIcon.setPadding(nPaddingL, nPaddingL, 0, 0);
			ivSTIcon.setScaleType(ScaleType.CENTER_INSIDE);
			ivSTIcon.setBackgroundResource(android.R.color.transparent);
			ivSTIcon.setImageResource(Utils.getShiftTemplateIcon(alnShiftTemplateIcons.get(i)));
			rlShiftTemplate.addView(ivSTIcon);

			LinearLayout.LayoutParams lp4IVSTSelector = new LinearLayout.LayoutParams(nWidth, nHeight);
			lp4IVSTSelector.gravity = Gravity.CENTER;

			ImageView ivSTSelector = new ImageView(ActivityHome.this);
			ivSTSelector.setLayoutParams(lp4IVSTSelector);
			ivSTSelector.setScaleType(ScaleType.FIT_XY);
			ivSTSelector.setBackgroundColor(getResources().getColor(android.R.color.transparent));
			ivSTSelector.setBackgroundResource(R.drawable.template_selected);
			ivSTSelector.setVisibility(View.GONE);
			rlShiftTemplate.addView(ivSTSelector);

			rlShiftTemplate.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					selectShiftTemplate(nPosition, alnShiftTemplateColors);
				}
			});
			rlShiftTemplate.setOnLongClickListener(new OnLongClickListener()
			{
				@Override
				public boolean onLongClick(View v)
				{
					Intent intent = new Intent(ActivityHome.this, ActivityShiftTemplatesAdd.class);
					intent.putExtra(Konstant.KEY_SELECTED_SHIFT_TEMPLATE_ID, malnShiftTemplateIDs.get(nPosition));
					ActivityHome.this.startActivity(intent);
					return false;
				}
			});

			mllShiftTemplates.addView(rlShiftTemplate);

			malivSTIcons.add(ivSTIcon);
			malivSTSelectors.add(ivSTSelector);
		}
	}

	private DailyTallyDetails getDailyTallyDetails()
	{
		DailyTallyDetails dailyTallyDetails = new DailyTallyDetails();
		ArrayList<Integer> alnTallyIDs = new ArrayList<Integer>();
		ArrayList<Integer> alnTallyCounts = new ArrayList<Integer>();
		ArrayList<String> alsTallyTimeUnits = new ArrayList<String>();
		ArrayList<Integer> alnNumberOfPaydays = new ArrayList<Integer>();
		ArrayList<String> alsTallyAmounts = new ArrayList<String>();

		int nCurrentYear = mnSelectedYear;
		int nCurrentMOY = mnSelectedMOY - 1;
		int nCurrentDOM = mnSelectedDOM;

		String strCurrentDate = nCurrentYear + "-" + nCurrentMOY + "-" + nCurrentDOM;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CANADA);
		Date dateCurrent = null;
		try
		{
			dateCurrent = sdf.parse(mnSelectedYear + "-" + mnSelectedMOY + "-" + mnSelectedDOM);

			if (Utils.isTallyDay(strCurrentDate))
			{
				for (int i = 0; i < Utils.msaTallyDates.size(); i++)
				{
					int nTallyID = Utils.msaTallyDates.keyAt(i);
					ArrayList<String> alsTallyDates = Utils.msaTallyDates.valueAt(i);
					if (alsTallyDates.contains(strCurrentDate))
					{
						int nPreviousIndex = alsTallyDates.indexOf(strCurrentDate) - 1;
						if (nPreviousIndex >= 0)
						{
							String strPreviousTallyDate = alsTallyDates.get(nPreviousIndex);

							int nPreviousYear = Integer.parseInt(strPreviousTallyDate.substring(0, strPreviousTallyDate.indexOf("-")));
							int nPreviousMOY = Integer.parseInt(strPreviousTallyDate.substring(strPreviousTallyDate.indexOf("-") + 1, strPreviousTallyDate.lastIndexOf("-")));
							int nPreviousDOM = Integer.parseInt(strPreviousTallyDate.substring(strPreviousTallyDate.lastIndexOf("-") + 1));

							Calendar calPreviousTally = Calendar.getInstance();
							calPreviousTally.set(nPreviousYear, nPreviousMOY, nPreviousDOM);

							double dTallyTotalAmount = 0;
							int nNumberOfPaydays = 0;
							while (calPreviousTally.getTime().before(dateCurrent))
							{
								int nTempYear = calPreviousTally.get(Calendar.YEAR);
								int nTempMOY = calPreviousTally.get(Calendar.MONTH);
								int nTempDOM = calPreviousTally.get(Calendar.DAY_OF_MONTH);
								String strDate = nTempYear + "-" + nTempMOY + "-" + nTempDOM;

								if (Utils.isPiggyDay(strDate))
								{
									DailyPiggyDetails tempDailyPiggyDetails = DBHelper.getInstance(ActivityHome.this).getDailyPiggyDetails(nTempYear, (nTempMOY + 1), nTempDOM);
									for (int j = 0; j < tempDailyPiggyDetails.getJobIDs().size(); j++)
									{
										nNumberOfPaydays++;

										double dPiggyAmount = tempDailyPiggyDetails.getRegularAmounts().get(j) + tempDailyPiggyDetails.getOvertimeAmounts().get(j) + tempDailyPiggyDetails.getSpecialAmounts().get(j);
										if (mPreferences.getBoolean(Konstant.PREF_KEY_DEDUCTIONS_ON, Konstant.PREF_DEF_DEDUCTIONS_ON))
											dPiggyAmount -= tempDailyPiggyDetails.getDeductions().get(j);
										if (mPreferences.getBoolean(Konstant.PREF_KEY_TIPS_ON, Konstant.PREF_DEF_TIPS_ON))
											dPiggyAmount += tempDailyPiggyDetails.getTips().get(j);
										if (dPiggyAmount <= 0)
											dPiggyAmount = 0;
										dTallyTotalAmount += dPiggyAmount;
									}
								}

								calPreviousTally.add(Calendar.DAY_OF_YEAR, 1);
							}

							alnNumberOfPaydays.add(nNumberOfPaydays);
							alnTallyIDs.add(nTallyID);

							int nCount = DBHelper.getInstance(ActivityHome.this).getEveryCountFromTabTallies(nTallyID);
							alnTallyCounts.add(nCount);

							String strTimeUnit = "";
							int nTimeUnit = DBHelper.getInstance(ActivityHome.this).getTimeUnitFromTabTallies(nTallyID);
							if (nCount == 1)
							{
								if (nTimeUnit == 0)
									strTimeUnit = "Week";
								else
									strTimeUnit = "Month";
							}
							else
							{
								if (nTimeUnit == 0)
									strTimeUnit = "Weeks";
								else
									strTimeUnit = "Months";
							}
							alsTallyTimeUnits.add(strTimeUnit);

							DecimalFormat df = new DecimalFormat("0.00");
							alsTallyAmounts.add("$" + df.format(dTallyTotalAmount));
						}
					}
				}
			}
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}

		dailyTallyDetails.setTallyIDs(alnTallyIDs);
		dailyTallyDetails.setTallyCounts(alnTallyCounts);
		dailyTallyDetails.setTallyTimeUnits(alsTallyTimeUnits);
		dailyTallyDetails.setNumberOfPaydays(alnNumberOfPaydays);
		dailyTallyDetails.setTallyAmounts(alsTallyAmounts);

		return dailyTallyDetails;
	}

	private void selectShiftTemplate(int nPosition, ArrayList<Integer> alnShiftTemplateColors)
	{
		for (int i = 0; i < malivSTSelectors.size(); i++)
		{
			if (i != nPosition)
			{
				malivSTSelectors.get(i).setVisibility(View.GONE);
			}
			else if ((i == nPosition) && (mnSelectedShiftTemplateIndex == nPosition))
			{
				malivSTSelectors.get(i).setVisibility(View.GONE);
				mnSelectedShiftTemplateIndex = -1;
			}
			else
			{
				malivSTSelectors.get(i).setVisibility(View.VISIBLE);
				mnSelectedShiftTemplateIndex = nPosition;
			}
		}
	}

	private boolean isAnyShiftTemplateSelected()
	{
		boolean bSelected = false;
		if (mnSelectedShiftTemplateIndex != -1)
			bSelected = true;

		return bSelected;
	}

	private void showSelectedMonth(int nYear, int nMOY)
	{
		clearDateSelection();

		mCalendar.set(nYear, nMOY, 1);

		mnCurrentYear = mCalendar.get(Calendar.YEAR);
		mnCurrentMOY = mCalendar.get(Calendar.MONTH);

		refreshCalendar();

		mnSelectedYear = nYear;
		mnSelectedMOY = nMOY + 2;
		mnSelectedDOM = -1;

		mnSelectedDatePosition = -1;
		mDailyDetailsAdapter.clearDataset();
	}

	private void showPreviousMonth()
	{
		clearDateSelection();
		if (mCalendar.get(Calendar.MONTH) == mCalendar.getActualMinimum(Calendar.MONTH))
		{
			mCalendar.set((mCalendar.get(Calendar.YEAR) - 1), mCalendar.getActualMaximum(Calendar.MONTH), 1);
		}
		else
		{
			mCalendar.set(Calendar.MONTH, mCalendar.get(Calendar.MONTH) - 1);
		}

		mnCurrentYear = mCalendar.get(Calendar.YEAR);
		mnCurrentMOY = mCalendar.get(Calendar.MONTH);

		refreshCalendar();

		mnSelectedYear = -1;
		mnSelectedDOM = -1;
		mnSelectedMOY = -1;
		mnSelectedDatePosition = -1;
		mDailyDetailsAdapter.clearDataset();
	}

	private void showNextMonth()
	{
		clearDateSelection();
		if (mCalendar.get(Calendar.MONTH) == mCalendar.getActualMaximum(Calendar.MONTH))
		{
			mCalendar.set((mCalendar.get(Calendar.YEAR) + 1), mCalendar.getActualMinimum(Calendar.MONTH), 1);
		}
		else
		{
			mCalendar.set(Calendar.MONTH, mCalendar.get(Calendar.MONTH) + 1);
		}

		mnCurrentYear = mCalendar.get(Calendar.YEAR);
		mnCurrentMOY = mCalendar.get(Calendar.MONTH);

		refreshCalendar();

		mnSelectedYear = -1;
		mnSelectedDOM = -1;
		mnSelectedMOY = -1;
		mnSelectedDatePosition = -1;
		mDailyDetailsAdapter.clearDataset();
	}

	private void unselectDate(int nPosition)
	{
		RelativeLayout rlDate = (RelativeLayout) mgvCalendar.getChildAt(nPosition);
		if (rlDate != null)
		{
			TextView tvDate = (TextView) rlDate.getChildAt(0);
			if (tvDate != null)
			{
				tvDate.setSelected(false);
				tvDate.setTextColor(getResources().getColor(R.color.calendar_text_normal));
				if (((nPosition + 1) == mnTodaysDOM) && (mnCurrentMOY == mnTodaysMOY) && (mnCurrentYear == mnTodaysYear))
					tvDate.setBackgroundResource(R.color.calendar_item_bg_today);
				else
					tvDate.setBackgroundResource(R.color.calendar_item_bg_normal);
			}

			ImageView ivPiggy = (ImageView) rlDate.getChildAt(1);
			if (ivPiggy != null)
			{
				ivPiggy.setImageResource(R.drawable.cal_piggy_normal);
			}

			ImageView ivTally = (ImageView) rlDate.getChildAt(2);
			if (ivTally != null)
			{
				ivTally.setImageResource(R.drawable.cal_tally_normal);
			}
			rlDate.invalidate();
		}
	}

	private void updateCalendar()
	{
		malsItems.clear();
		for (int i = 0; i < 31; i++)
		{
			Random r = new Random();

			if (r.nextInt(10) > 6)
			{
				malsItems.add(Integer.toString(i));
			}
		}

		mCalendarAdapter.setItems(malsItems);
		mCalendarAdapter.notifyDataSetChanged();
	}

	private void processDateSelection(View v, int nPosition)
	{
		TextView tvSelectedDate = (TextView) v.findViewById(R.id.citv_date);
		if (tvSelectedDate instanceof TextView && !tvSelectedDate.getText().equals(""))
		{
			unselectDate(mnSelectedDatePosition);

			mnSelectedDatePosition = nPosition;

			tvSelectedDate.setSelected(true);
			tvSelectedDate.setTextColor(getResources().getColor(R.color.calendar_text_selected));
			tvSelectedDate.setBackgroundResource(R.color.calendar_item_bg_selected);

			ImageView ivPiggy = (ImageView) v.findViewById(R.id.iv_ci_payday);
			if (ivPiggy != null)
			{
				ivPiggy.setImageResource(R.drawable.cal_piggy_selected);
			}
			ImageView ivTally = (ImageView) v.findViewById(R.id.iv_ci_tally);
			if (ivTally != null)
			{
				ivTally.setImageResource(R.drawable.cal_tally_selected);
			}

			String day = tvSelectedDate.getText().toString();
			if (day.length() == 1)
			{
				day = "0" + day;
			}

			String strSelectedDate = android.text.format.DateFormat.format("yyyy-MM", mCalendar) + "-" + day;

			String[] dateArr = strSelectedDate.split("-");
			mnSelectedYear = Integer.parseInt(dateArr[0]);
			mnSelectedMOY = Integer.parseInt(dateArr[1]);
			mnSelectedDOM = Integer.parseInt(dateArr[2]);

			DateSelectionProcessor dateSelectionProcessor = new DateSelectionProcessor();
			dateSelectionProcessor.execute();
		}
	}

	private void updateDailyDetails()
	{
		ArrayList<Integer> alnDummyDailyDetailsList = new ArrayList<Integer>();

		ArrayList<ShiftInstanceContinuing> alShiftInstanceContinuing = DBHelper.getInstance(ActivityHome.this).getShiftInstancesContinuing(mnSelectedYear, mnSelectedMOY, mnSelectedDOM);
		for (int i = 0; i < alShiftInstanceContinuing.size(); i++)
			alnDummyDailyDetailsList.add(i);

		// Update Shift Details
		DailyShiftDetails dailyShiftDetails = DBHelper.getInstance(ActivityHome.this).getDailyShiftDetails(mnSelectedYear, mnSelectedMOY, mnSelectedDOM);
		for (int i = 0; i < dailyShiftDetails.getJobIDs().size(); i++)
			alnDummyDailyDetailsList.add(i);

		// Update Payday details
		DailyPiggyDetails dailyPiggyDetails = DBHelper.getInstance(ActivityHome.this).getDailyPiggyDetails(mnSelectedYear, mnSelectedMOY, mnSelectedDOM);
		for (int i = 0; i < dailyPiggyDetails.getJobIDs().size(); i++)
			alnDummyDailyDetailsList.add(i);

		// Update Tally Details
		DailyTallyDetails dailyTallyDetails = getDailyTallyDetails();
		for (int i = 0; i < dailyTallyDetails.getTallyIDs().size(); i++)
			alnDummyDailyDetailsList.add(i);

		// Dummy last item for space
		alnDummyDailyDetailsList.add(0);

		mDailyDetailsAdapter.changeDataset(alnDummyDailyDetailsList, alShiftInstanceContinuing, dailyShiftDetails, dailyPiggyDetails, dailyTallyDetails);
	}

	public static void setShiftTrayChanged(boolean bValue)
	{
		mbShiftTrayChanged = bValue;
	}

	public void refreshCalendar()
	{
		mCalendarAdapter.refreshDays();
		updateCalendar();

		mtvCABTitle.setText(android.text.format.DateFormat.format("MMMM yyyy", mCalendar));
	}

	public void onNewIntent(Intent intent)
	{
		mCalendar.set(mnSelectedYear, mnSelectedMOY, mnSelectedDOM);
	}

	private class DateSelectionProcessor extends AsyncTask<Void, Void, Void>
	{
		ProgressDialog mProgressDialog;

		DateSelectionProcessor()
		{
		}

		@Override
		protected void onPreExecute()
		{
			if (mnSelectedShiftTemplateIndex != -1)
			{
				mProgressDialog = new ProgressDialog(ActivityHome.this);
				mProgressDialog.setMessage(getString(R.string.msg_adding_shift));
				mProgressDialog.setCancelable(false);
				mProgressDialog.setIndeterminate(true);
				mProgressDialog.show();
			}
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			return null;
		}

		@Override
		protected void onProgressUpdate(Void... params)
		{
		}

		@Override
		protected void onPostExecute(Void param)
		{
			if (mnSelectedShiftTemplateIndex != -1)
			{
				int nSelectedShiftTemplateID = malnShiftTemplateIDs.get(mnSelectedShiftTemplateIndex);

				if ((mnRecentShiftID >= 0) && (mnRecentShiftTemplateID == nSelectedShiftTemplateID) && (mnRecentShiftYear == mnSelectedYear) && (mnRecentShiftMOY == mnSelectedMOY) && (mnRecentShiftDOM == mnSelectedDOM))
				{
					DBHelper.getInstance(ActivityHome.this).deleteShiftInstance(mnRecentShiftID);
					mnRecentShiftID = -1;

					Toast.makeText(ActivityHome.this, getString(R.string.msg_recent_shift_deleted), Toast.LENGTH_SHORT).show();
				}
				else
				{
					placeShiftInstance(nSelectedShiftTemplateID);
				}
			}

			mCalendarAdapter.setDateSelected(mnSelectedYear, (mnSelectedMOY - 1), mnSelectedDOM);
			mCalendarAdapter.notifyDataSetChanged();
			updateDailyDetails();

			mgvCalendar.post(new Runnable()
			{
				@Override
				public void run()
				{
					if (mProgressDialog != null)
						mProgressDialog.dismiss();
				}
			});
		}
	}

	private class CalendarDisplay extends AsyncTask<Void, Void, Void>
	{
		ProgressDialog mProgressDialog;

		int mnDisplayType;
		int mnYear;
		int mnMOY;

		CalendarDisplay(int nDisplayType, int nYear, int nMOY)
		{
			mnDisplayType = nDisplayType;
			mnYear = nYear;
			mnMOY = nMOY;
		}

		@Override
		protected void onPreExecute()
		{
			mProgressDialog = new ProgressDialog(ActivityHome.this);
			mProgressDialog.setMessage(getString(R.string.msg_displaying_month));
			mProgressDialog.setCancelable(false);
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			return null;
		}

		@Override
		protected void onProgressUpdate(Void... params)
		{
		}

		@Override
		protected void onPostExecute(Void param)
		{
			switch (mnDisplayType)
			{
			case Konstant.DISPLAY_SELECTED_MONTH:
				showSelectedMonth(mnYear, mnMOY);
				break;
			case Konstant.DISPLAY_PREVIOUS_MONTH:
				showPreviousMonth();
				break;
			case Konstant.DISPLAY_NEXT_MONTH:
				showNextMonth();
				break;
			}

			mgvCalendar.post(new Runnable()
			{
				@Override
				public void run()
				{
					if (mProgressDialog != null)
						mProgressDialog.dismiss();
				}
			});
		}
	}
}