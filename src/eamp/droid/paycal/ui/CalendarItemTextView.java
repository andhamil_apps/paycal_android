/*
 * CalendarItemTextView
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal.ui;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.TextView;

public class CalendarItemTextView extends TextView
{
	private Paint mPaint;
	private Rect mRect;

	private ArrayList<Integer> malnColors;
	private ArrayList<Integer> malnStarts;
	private ArrayList<Integer> malnEnds;

	public CalendarItemTextView(final Context context)
	{
		super(context);

		mPaint = new Paint();
		mRect = new Rect();

		malnColors = new ArrayList<Integer>();
		malnStarts = new ArrayList<Integer>();
		malnEnds = new ArrayList<Integer>();
	}

	public CalendarItemTextView(final Context context, final AttributeSet attrs)
	{
		super(context, attrs);

		mPaint = new Paint();
		mRect = new Rect();

		malnColors = new ArrayList<Integer>();
		malnStarts = new ArrayList<Integer>();
		malnEnds = new ArrayList<Integer>();
	}

	public CalendarItemTextView(final Context context, final AttributeSet attrs, final int defStyle)
	{
		super(context, attrs, defStyle);

		mPaint = new Paint();
		mRect = new Rect();

		malnColors = new ArrayList<Integer>();
		malnStarts = new ArrayList<Integer>();
		malnEnds = new ArrayList<Integer>();
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		int nWidth = this.getWidth();
		int nHeight = this.getHeight();
		
		if (malnColors != null)
		{
			for (int i = 0; i < malnColors.size(); i++)
			{
				mPaint.setColor(malnColors.get(i));
				mPaint.setAlpha(90);
				mPaint.setStyle(Style.FILL);
			
				int nStart = nHeight * malnStarts.get(i) / 48;
				int nEnd = nHeight * malnEnds.get(i) / 48;
				mRect.set(0, nStart, nWidth, nEnd);

				canvas.drawRect(mRect, mPaint);
			}
		}
		
		super.onDraw(canvas);
	}

	@Override
	protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec)
	{
		int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
		setMeasuredDimension(width, width);
	}

	@Override
	protected void onSizeChanged(final int w, final int h, final int oldw, final int oldh)
	{
		super.onSizeChanged(w, w, oldw, oldh);
	}

	public void setShiftInstances(ArrayList<Integer> alnColors, ArrayList<Integer> alnStarts, ArrayList<Integer> alnEnds)
	{
		malnColors = alnColors;
		malnStarts = alnStarts;
		malnEnds = alnEnds;
	}
}