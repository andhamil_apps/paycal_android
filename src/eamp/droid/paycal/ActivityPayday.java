/*
 * ActivityPayday
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.text.DecimalFormat;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class ActivityPayday extends Activity
{
	private int mnPaydayYear;
	private int mnPaydayMOY;
	private int mnPaydayDOM;

	private int mnRegularMinutes;
	private int mnOvertimeMinutes;
	private int mnSpecialMinutes;

	private double mdRegularAmount;
	private double mdOvertimeAmount;
	private double mdSpecialAmount;

	private double mdTip;

	private double mdDeductionRate;
	private double mdDeduction;

	private String mstrJobName;

	private SharedPreferences mPreferences;
	private SharedPreferences.Editor mPrefEditor;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_payday);

		ActionBar currentActionBar = getActionBar();
		currentActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		currentActionBar.setDisplayShowTitleEnabled(false);
		currentActionBar.setCustomView(R.layout.action_bar_items_1);

		mPreferences = getSharedPreferences(Konstant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		mPrefEditor = mPreferences.edit();

		final DecimalFormat df = new DecimalFormat("0.00");

		TextView tvActivityTitle = (TextView) findViewById(R.id.tv_ab1_title);

		TextView tvJobName = (TextView) findViewById(R.id.tv_payday_total_jobname);

		TextView tvRegularMinutes = (TextView) findViewById(R.id.tv_payday_regular_hours_payhours);
		TextView tvRegularAmount = (TextView) findViewById(R.id.tv_payday_regular_hours_amount);

		TextView tvOvertimeMinutes = (TextView) findViewById(R.id.tv_payday_overtime_hours_payhours);
		TextView tvOvertimeAmount = (TextView) findViewById(R.id.tv_payday_overtime_hours_amount);

		TextView tvSpecialMinutes = (TextView) findViewById(R.id.tv_payday_special_hours_payhours);
		TextView tvSpecialAmount = (TextView) findViewById(R.id.tv_payday_special_hours_amount);

		TextView tvDeductionRate = (TextView) findViewById(R.id.tv_payday_extra_deduction_rate);
		TextView tvDeductionAmount = (TextView) findViewById(R.id.tv_payday_extra_deduction_amount);
		TextView tvTipsAmount = (TextView) findViewById(R.id.tv_payday_extra_tips_amount);

		TextView tvTotalMinutes = (TextView) findViewById(R.id.tv_payday_total_payhours);
		final TextView tvTotalAmount = (TextView) findViewById(R.id.tv_payday_total_amount);

		final ImageView ivDeduction = (ImageView) findViewById(R.id.iv_payday_extra_deduction);

		boolean bDeductionsOn = mPreferences.getBoolean(Konstant.PREF_KEY_DEDUCTIONS_ON, Konstant.PREF_DEF_DEDUCTIONS_ON);
		if (bDeductionsOn)
			ivDeduction.setImageResource(R.drawable.payday_extra_visible);
		else
			ivDeduction.setImageResource(R.drawable.payday_extra_invisible);

		ivDeduction.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				boolean bDeductionsOn = mPreferences.getBoolean(Konstant.PREF_KEY_DEDUCTIONS_ON, Konstant.PREF_DEF_DEDUCTIONS_ON);
				if (bDeductionsOn)
				{
					mPrefEditor.putBoolean(Konstant.PREF_KEY_DEDUCTIONS_ON, false);
					mPrefEditor.commit();
					ivDeduction.setImageResource(R.drawable.payday_extra_invisible);

					double dTotalAmount = mdRegularAmount + mdOvertimeAmount + mdSpecialAmount;
					if (mPreferences.getBoolean(Konstant.PREF_KEY_TIPS_ON, Konstant.PREF_DEF_TIPS_ON))
						dTotalAmount += mdTip;
					tvTotalAmount.setText("$" + df.format(dTotalAmount));
				}
				else
				{
					mPrefEditor.putBoolean(Konstant.PREF_KEY_DEDUCTIONS_ON, true);
					mPrefEditor.commit();
					ivDeduction.setImageResource(R.drawable.payday_extra_visible);

					double dTotalAmount = mdRegularAmount + mdOvertimeAmount + mdSpecialAmount - mdDeduction;
					if (mPreferences.getBoolean(Konstant.PREF_KEY_TIPS_ON, Konstant.PREF_DEF_TIPS_ON))
						dTotalAmount += mdTip;
					tvTotalAmount.setText("$" + df.format(dTotalAmount));
				}
				ivDeduction.invalidate();
			}
		});

		final ImageView ivTips = (ImageView) findViewById(R.id.iv_payday_extra_tips);

		boolean bTipsOn = mPreferences.getBoolean(Konstant.PREF_KEY_TIPS_ON, Konstant.PREF_DEF_TIPS_ON);
		if (bTipsOn)
			ivTips.setImageResource(R.drawable.payday_extra_visible);
		else
			ivTips.setImageResource(R.drawable.payday_extra_invisible);

		ivTips.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				boolean bTipsOn = mPreferences.getBoolean(Konstant.PREF_KEY_TIPS_ON, Konstant.PREF_DEF_TIPS_ON);
				if (bTipsOn)
				{
					mPrefEditor.putBoolean(Konstant.PREF_KEY_TIPS_ON, false);
					mPrefEditor.commit();
					ivTips.setImageResource(R.drawable.payday_extra_invisible);

					double dTotalAmount = mdRegularAmount + mdOvertimeAmount + mdSpecialAmount;
					if (mPreferences.getBoolean(Konstant.PREF_KEY_DEDUCTIONS_ON, Konstant.PREF_DEF_DEDUCTIONS_ON))
						dTotalAmount -= mdDeduction;
					tvTotalAmount.setText("$" + df.format(dTotalAmount));
				}
				else
				{
					mPrefEditor.putBoolean(Konstant.PREF_KEY_TIPS_ON, true);
					mPrefEditor.commit();
					ivTips.setImageResource(R.drawable.payday_extra_visible);

					double dTotalAmount = mdRegularAmount + mdOvertimeAmount + mdSpecialAmount + mdTip;
					if (mPreferences.getBoolean(Konstant.PREF_KEY_DEDUCTIONS_ON, Konstant.PREF_DEF_DEDUCTIONS_ON))
						dTotalAmount -= mdDeduction;
					tvTotalAmount.setText("$" + df.format(dTotalAmount));
				}
			}
		});

		Intent intent = getIntent();
		if (intent != null)
		{
			mnPaydayYear = intent.getIntExtra(Konstant.KEY_PAYDAY_DATE_YEAR, -1);
			mnPaydayMOY = intent.getIntExtra(Konstant.KEY_PAYDAY_DATE_MOY, -1);
			mnPaydayDOM = intent.getIntExtra(Konstant.KEY_PAYDAY_DATE_DOM, -1);

			mstrJobName = intent.getStringExtra(Konstant.KEY_PAYDAY_JOB_NAME);

			mnRegularMinutes = intent.getIntExtra(Konstant.KEY_PAYDAY_REGULAR_MINUTES, -1);
			mnOvertimeMinutes = intent.getIntExtra(Konstant.KEY_PAYDAY_OVERTIME_MINUTES, -1);
			mnSpecialMinutes = intent.getIntExtra(Konstant.KEY_PAYDAY_SPECIAL_MINUTES, -1);

			mdRegularAmount = intent.getDoubleExtra(Konstant.KEY_PAYDAY_REGULAR_AMOUNT, 0);
			mdOvertimeAmount = intent.getDoubleExtra(Konstant.KEY_PAYDAY_OVERTIME_AMOUNT, 0);
			mdSpecialAmount = intent.getDoubleExtra(Konstant.KEY_PAYDAY_SPECIAL_AMOUNT, 0);

			mdTip = intent.getDoubleExtra(Konstant.KEY_PAYDAY_TIPS, 0);

			mdDeductionRate = intent.getDoubleExtra(Konstant.KEY_PAYDAY_DEDUCTION_RATE, 0);
			mdDeduction = intent.getDoubleExtra(Konstant.KEY_PAYDAY_DEDUCTION, 0);

			tvActivityTitle.setText(Utils.constructPaydayTitle(mnPaydayYear, mnPaydayMOY, mnPaydayDOM));

			tvJobName.setText(mstrJobName);

			tvRegularMinutes.setText(Utils.constructTimeInHrsAndMins(mnRegularMinutes));
			tvRegularAmount.setText("$" + df.format(mdRegularAmount));

			tvOvertimeMinutes.setText(Utils.constructTimeInHrsAndMins(mnOvertimeMinutes));
			tvOvertimeAmount.setText("$" + df.format(mdOvertimeAmount));

			tvSpecialMinutes.setText(Utils.constructTimeInHrsAndMins(mnSpecialMinutes));
			tvSpecialAmount.setText("$" + df.format(mdSpecialAmount));

			tvDeductionRate.setText("(" + df.format(mdDeductionRate) + "%)");
			tvDeductionAmount.setText("- $" + df.format(mdDeduction));

			tvTipsAmount.setText("$" + df.format(mdTip));

			int nTotalMinutes = mnRegularMinutes + mnOvertimeMinutes + mnSpecialMinutes;
			tvTotalMinutes.setText(Utils.constructTimeInHrsAndMins(nTotalMinutes));

			double dTotalAmount = mdRegularAmount + mdOvertimeAmount + mdSpecialAmount;

			if (mPreferences.getBoolean(Konstant.PREF_KEY_TIPS_ON, Konstant.PREF_DEF_TIPS_ON))
				dTotalAmount += mdTip;

			if (mPreferences.getBoolean(Konstant.PREF_KEY_DEDUCTIONS_ON, Konstant.PREF_DEF_DEDUCTIONS_ON))
				dTotalAmount -= mdDeduction;

			if (dTotalAmount < 0)
				dTotalAmount = 0;

			tvTotalAmount.setText("$" + df.format(dTotalAmount));
		}
	}
}