/*
 * AdapterCalendar
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.util.ArrayList;
import java.util.Calendar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import eamp.droid.paycal.model.DailyShiftDetails;
import eamp.droid.paycal.model.ShiftInstanceContinuing;
import eamp.droid.paycal.ui.CalendarItemTextView;

public class AdapterCalendar extends BaseAdapter
{
	private static final int FIRST_DAY_OF_WEEK = 0;

	private int mnSelectedYear;
	private int mnSelectedMOY;
	private int mnSelectedDOM;

	private String[] mstrarrDays;

	private ArrayList<String> malsItems;

	private Calendar mCalendarMonth;
	private Calendar mCalendarToday;

	private Context mContext;

	public AdapterCalendar(Context context, Calendar calendarMonth)
	{
		mContext = context;
		mCalendarMonth = calendarMonth;

		mCalendarToday = (Calendar) calendarMonth.clone();
		mCalendarMonth.set(Calendar.DAY_OF_MONTH, 1);

		this.malsItems = new ArrayList<String>();

		refreshDays();
	}

	public void setDateSelected(int nSelectedYear, int nSelectedMOY, int nSelectedDOM)
	{
		mnSelectedYear = nSelectedYear;
		mnSelectedMOY = nSelectedMOY;
		mnSelectedDOM = nSelectedDOM;
	}

	public void setItems(ArrayList<String> items)
	{
		for (int i = 0; i != items.size(); i++)
		{
			if (items.get(i).length() == 1)
			{
				items.set(i, "0" + items.get(i));
			}
		}
		this.malsItems = items;
	}

	public int getCount()
	{
		return mstrarrDays.length;
	}

	public Object getItem(int position)
	{
		return null;
	}

	public long getItemId(int position)
	{
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;

		if (convertView == null)
		{
			LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.calendar_item, null);
		}

		v.setBackgroundResource(R.color.calendar_item_bg_normal);

		CalendarItemTextView citvDate = (CalendarItemTextView) v.findViewById(R.id.citv_date);

		ImageView ivPayday = (ImageView) v.findViewById(R.id.iv_ci_payday);
		ivPayday.setVisibility(View.GONE);

		ImageView ivTally = (ImageView) v.findViewById(R.id.iv_ci_tally);
		ivTally.setVisibility(View.GONE);

		ArrayList<Integer> alnColors = new ArrayList<Integer>();
		ArrayList<Integer> alnStarts = new ArrayList<Integer>();
		ArrayList<Integer> alnStartMinutes = new ArrayList<Integer>();
		ArrayList<Integer> alnEnds = new ArrayList<Integer>();
		ArrayList<Integer> alnEndMinutes = new ArrayList<Integer>();

		if (mstrarrDays[position].equals(""))
		{
			citvDate.setClickable(false);
			citvDate.setFocusable(false);
			citvDate.setShiftInstances(alnColors, alnStarts, alnEnds);
			citvDate.setText("");
		}
		else
		{
			int nYear = mCalendarMonth.get(Calendar.YEAR);
			int nMOY = mCalendarMonth.get(Calendar.MONTH);
			int nDOM = Integer.parseInt(mstrarrDays[position]);
			String strCurrentDate = nYear + "-" + nMOY + "-" + nDOM;

			if (nYear == mCalendarToday.get(Calendar.YEAR) && nMOY == mCalendarToday.get(Calendar.MONTH) && nDOM == mCalendarToday.get(Calendar.DAY_OF_MONTH))
			{
				citvDate.setBackgroundResource(R.color.calendar_item_bg_today);
				citvDate.setTextColor(mContext.getResources().getColor(R.color.calendar_text_normal));
			}
			else if (nYear == mnSelectedYear && nMOY == mnSelectedMOY && nDOM == mnSelectedDOM)
			{
				citvDate.setBackgroundResource(R.color.calendar_item_bg_selected);
				citvDate.setTextColor(mContext.getResources().getColor(R.color.calendar_text_selected));
			}
			else
			{
				citvDate.setBackgroundResource(R.color.calendar_item_bg_normal);
				citvDate.setTextColor(mContext.getResources().getColor(R.color.calendar_text_normal));
			}

			if (Utils.isPiggyDay(strCurrentDate))
			{
				ivPayday.setVisibility(View.VISIBLE);
			}
			else
			{
				ivPayday.setVisibility(View.GONE);
			}

			if (Utils.mbIsTallyOn)
			{

				if (Utils.isTallyDay(strCurrentDate))
				{
					ivTally.setVisibility(View.VISIBLE);
				}
				else
				{
					ivTally.setVisibility(View.GONE);
				}
			}

			DailyShiftDetails dailyShiftDetails = DBHelper.getInstance(mContext).getDailyShiftDetails(nYear, (nMOY + 1), nDOM);
			alnColors = dailyShiftDetails.getColors();
			alnStarts = dailyShiftDetails.getFromHours();
			alnStartMinutes = dailyShiftDetails.getFromMinutes();
			alnEnds = dailyShiftDetails.getToHours();
			alnEndMinutes = dailyShiftDetails.getToMinutes();

			for (int i = 0; i < alnColors.size(); i++)
			{
				switch (alnColors.get(i))
				{
				case Konstant.COLOR_PURPLE:
					alnColors.set(i, mContext.getResources().getColor(R.color.color_purple));
					break;
				case Konstant.COLOR_CYAN:
					alnColors.set(i, mContext.getResources().getColor(R.color.color_cyan));
					break;
				case Konstant.COLOR_GREEN:
					alnColors.set(i, mContext.getResources().getColor(R.color.color_green));
					break;
				case Konstant.COLOR_MUSTARD:
					alnColors.set(i, mContext.getResources().getColor(R.color.color_mustard));
					break;
				case Konstant.COLOR_ORANGE:
					alnColors.set(i, mContext.getResources().getColor(R.color.color_orange));
					break;
				case Konstant.COLOR_RED:
					alnColors.set(i, mContext.getResources().getColor(R.color.color_red));
					break;
				}

				int nStart = alnStarts.get(i);
				if ((alnStartMinutes.get(i) >= 0) && (alnStartMinutes.get(i) < 15))
					alnStarts.set(i, (nStart * 2));
				else if ((alnStartMinutes.get(i) >= 15) && (alnStartMinutes.get(i) < 45))
					alnStarts.set(i, (nStart * 2 + 1));
				else
					alnStarts.set(i, ((nStart + 1) * 2));

				int nEnd = alnEnds.get(i);

				if (nStart > nEnd)
				{
					alnEnds.set(i, 48);
				}
				else
				{
					if ((alnEndMinutes.get(i) >= 0) && (alnEndMinutes.get(i) < 15))
						alnEnds.set(i, (nEnd * 2));
					else if ((alnEndMinutes.get(i) >= 15) && (alnEndMinutes.get(i) < 45))
						alnEnds.set(i, (nEnd * 2 + 1));
					else
						alnEnds.set(i, ((nEnd + 1) * 2));
				}
			}

			ArrayList<ShiftInstanceContinuing> alShiftsFromPreviousDay = DBHelper.getInstance(mContext).getShiftInstancesContinuing(nYear, (nMOY + 1), nDOM);
			for (int i = 0; i < alShiftsFromPreviousDay.size(); i++)
			{
				switch (alShiftsFromPreviousDay.get(i).getColor())
				{
				case Konstant.COLOR_PURPLE:
					alnColors.add(mContext.getResources().getColor(R.color.color_purple));
					break;
				case Konstant.COLOR_CYAN:
					alnColors.add(mContext.getResources().getColor(R.color.color_cyan));
					break;
				case Konstant.COLOR_GREEN:
					alnColors.add(mContext.getResources().getColor(R.color.color_green));
					break;
				case Konstant.COLOR_MUSTARD:
					alnColors.add(mContext.getResources().getColor(R.color.color_mustard));
					break;
				case Konstant.COLOR_ORANGE:
					alnColors.add(mContext.getResources().getColor(R.color.color_orange));
					break;
				case Konstant.COLOR_RED:
					alnColors.add(mContext.getResources().getColor(R.color.color_red));
					break;
				}
				alnStarts.add(0);

				int nEnd = alShiftsFromPreviousDay.get(i).getToHour();
				int nEndMinutes = alShiftsFromPreviousDay.get(i).getToMinute();
				if ((nEndMinutes >= 0) && (nEndMinutes < 15))
					alnEnds.add(nEnd * 2);
				else if ((nEndMinutes >= 15) && (nEndMinutes < 45))
					alnEnds.add(nEnd * 2 + 1);
				else
					alnEnds.add((nEnd + 1) * 2);
			}

			citvDate.setShiftInstances(alnColors, alnStarts, alnEnds);

			citvDate.setText(mstrarrDays[position]);

		}

		String date = mstrarrDays[position];

		if (date.length() == 1)
		{
			date = "0" + date;
		}
		String monthStr = "" + (mCalendarMonth.get(Calendar.MONTH) + 1);
		if (monthStr.length() == 1)
		{
			monthStr = "0" + monthStr;
		}

		return v;
	}

	public void refreshDays()
	{
		malsItems.clear();

		int lastDay = mCalendarMonth.getActualMaximum(Calendar.DAY_OF_MONTH);
		int firstDay = (int) mCalendarMonth.get(Calendar.DAY_OF_WEEK);

		if (firstDay == 1)
		{
			mstrarrDays = new String[lastDay + (FIRST_DAY_OF_WEEK * 6)];
		}
		else
		{
			mstrarrDays = new String[lastDay + firstDay - (FIRST_DAY_OF_WEEK + 1)];
		}

		int j = FIRST_DAY_OF_WEEK;

		if (firstDay > 1)
		{
			for (j = 0; j < firstDay - FIRST_DAY_OF_WEEK; j++)
			{
				mstrarrDays[j] = "";
			}
		}
		else
		{
			for (j = 0; j < FIRST_DAY_OF_WEEK * 6; j++)
			{
				mstrarrDays[j] = "";
			}
			j = FIRST_DAY_OF_WEEK * 6 + 1;
		}

		int dayNumber = 1;
		for (int i = j - 1; i < mstrarrDays.length; i++)
		{
			mstrarrDays[i] = "" + dayNumber;
			dayNumber++;
		}
	}
}