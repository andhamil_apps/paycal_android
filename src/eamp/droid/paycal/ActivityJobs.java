/*
 * ActivityJobs
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class ActivityJobs extends Activity
{
	private static Context mContext;

	private static ListView mlvJobs;
	private static TextView mtvNoJobFound;

	private ImageButton mibUpgrade;

	private AdapterJobs mJobsAdapter;

	private SharedPreferences mPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jobs);

		ActionBar currentActionBar = getActionBar();
		currentActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		currentActionBar.setDisplayShowTitleEnabled(false);
		currentActionBar.setCustomView(R.layout.action_bar_items_1);

		TextView tvActivityTitle = (TextView) findViewById(R.id.tv_ab1_title);
		tvActivityTitle.setText(getString(R.string.edit_jobs));

		mContext = ActivityJobs.this;

		mPreferences = getSharedPreferences(Konstant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);

		mibUpgrade = (ImageButton) findViewById(R.id.ib_jobs_upgrade);
		mibUpgrade.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityJobs.this, ActivityJobs.this);
				dialogUpgrade.displayDialog();
			}
		});

		TextView tvAddJob = (TextView) findViewById(R.id.tv_jobs_add_job);
		tvAddJob.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(ActivityJobs.this, ActivityJobsAdd.class);
				startActivity(intent);
			}
		});

		ArrayList<Integer> alnJobIDs = DBHelper.getInstance(ActivityJobs.this).getColIDsFromTabJobs();
		ArrayList<String> alsJobNames = DBHelper.getInstance(ActivityJobs.this).getColNamesFromTabJobs();
		ArrayList<Integer> alnJobColors = DBHelper.getInstance(ActivityJobs.this).getColColorsFromTabJobs();
		mJobsAdapter = new AdapterJobs(mPreferences, ActivityJobs.this, R.layout.list_item_job, alnJobIDs, alsJobNames, alnJobColors);

		mlvJobs = (ListView) findViewById(R.id.lv_jobs);
		mlvJobs.setAdapter(mJobsAdapter);

		mtvNoJobFound = (TextView) findViewById(R.id.tv_jobs_no_job_found);

		if (alnJobIDs.isEmpty())
		{
			mlvJobs.setVisibility(View.GONE);
			mtvNoJobFound.setVisibility(View.VISIBLE);
		}
		else
		{
			mtvNoJobFound.setVisibility(View.GONE);
			mlvJobs.setVisibility(View.VISIBLE);
		}

		checkIsAppPremium();
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		ArrayList<Integer> alnJobIDs = DBHelper.getInstance(ActivityJobs.this).getColIDsFromTabJobs();
		ArrayList<String> alsJobNames = DBHelper.getInstance(ActivityJobs.this).getColNamesFromTabJobs();
		ArrayList<Integer> alnJobColors = DBHelper.getInstance(ActivityJobs.this).getColColorsFromTabJobs();

		mJobsAdapter.changeDataset(alnJobIDs, alsJobNames, alnJobColors);

		checkIsAppPremium();
	}

	private void checkIsAppPremium()
	{
		boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);
		if (bIsPremium)
		{
			mibUpgrade.setVisibility(View.GONE);
		}
		else
		{
			mibUpgrade.setVisibility(View.VISIBLE);
		}
	}

	public static void notifyJobsChanged()
	{
		if ((mlvJobs != null) && (mtvNoJobFound != null) && (DBHelper.getInstance(mContext) != null))
		{
			ArrayList<Integer> alnJobIDs = DBHelper.getInstance(mContext).getColIDsFromTabJobs();
			if (alnJobIDs.isEmpty())
			{
				mlvJobs.setVisibility(View.GONE);
				mtvNoJobFound.setVisibility(View.VISIBLE);
			}
			else
			{
				mtvNoJobFound.setVisibility(View.GONE);
				mlvJobs.setVisibility(View.VISIBLE);
			}
		}
	}
}