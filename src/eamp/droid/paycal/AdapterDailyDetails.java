/*
 * AdapterDailyDetails
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.text.DecimalFormat;
import java.util.ArrayList;

import eamp.droid.paycal.model.DailyPiggyDetails;
import eamp.droid.paycal.model.DailyShiftDetails;
import eamp.droid.paycal.model.DailyTallyDetails;
import eamp.droid.paycal.model.Job;
import eamp.droid.paycal.model.ShiftInstance;
import eamp.droid.paycal.model.ShiftInstanceContinuing;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AdapterDailyDetails extends ArrayAdapter<Integer>
{
	private int mLayoutResourceId;
	private Activity mActivity;
	private Context mContext;

	private ArrayList<Integer> malnDummyList;

	private ArrayList<ShiftInstanceContinuing> malShiftInstanceContinuing;

	private DailyShiftDetails mDailyShiftDetails;
	private DailyPiggyDetails mDailyPiggyDetails;
	private DailyTallyDetails mDailyTallyDetails;

	private DailyDetailHolder mListItemHolder;

	private SharedPreferences mPreferences;

	public AdapterDailyDetails(Activity activity, Context context, int layoutResourceId, ArrayList<Integer> alnDummyList, ArrayList<ShiftInstanceContinuing> alShiftInstanceContinuing,
			DailyShiftDetails dailyShiftDetails, DailyPiggyDetails dailyPiggyDetails, DailyTallyDetails dailyTallyDetails, SharedPreferences preferences)
	{
		super(context, layoutResourceId, alnDummyList);
		mActivity = activity;
		mContext = context;
		mLayoutResourceId = layoutResourceId;

		malnDummyList = alnDummyList;
		malShiftInstanceContinuing = alShiftInstanceContinuing;
		mDailyShiftDetails = dailyShiftDetails;
		mDailyPiggyDetails = dailyPiggyDetails;
		mDailyTallyDetails = dailyTallyDetails;

		mPreferences = preferences;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		View row = convertView;
		if (row == null)
		{
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			row = inflater.inflate(mLayoutResourceId, parent, false);

			mListItemHolder = new DailyDetailHolder();

			mListItemHolder.rlDailyDetails = (RelativeLayout) row.findViewById(R.id.rl_lidd);

			mListItemHolder.ivShift = (ImageView) row.findViewById(R.id.iv_lidd_shift);
			mListItemHolder.ivPiggy = (ImageView) row.findViewById(R.id.iv_lidd_piggy);
			mListItemHolder.ivTally = (ImageView) row.findViewById(R.id.iv_lidd_tally);

			mListItemHolder.ivOvernightShift = (ImageView) row.findViewById(R.id.iv_lidd_overnight_shift);

			mListItemHolder.llShiftTimes = (LinearLayout) row.findViewById(R.id.ll_lidd_shift_times);
			mListItemHolder.tvFrom = (TextView) row.findViewById(R.id.tv_lidd_shift_from);
			mListItemHolder.tvTo = (TextView) row.findViewById(R.id.tv_lidd_shift_to);

			mListItemHolder.tvJobName = (TextView) row.findViewById(R.id.tv_lidd_job_name);

			mListItemHolder.tvPaidHours = (TextView) row.findViewById(R.id.tv_lidd_paid_hours);

			mListItemHolder.tvAmount = (TextView) row.findViewById(R.id.tv_lidd_amount);

			row.setTag(mListItemHolder);
		}
		else
		{
			mListItemHolder = (DailyDetailHolder) row.getTag();
		}

		int nNumberOfShiftsContinuing = malShiftInstanceContinuing.size();
		int nNumberOfShifts = mDailyShiftDetails.getJobIDs().size();
		int nNumberOfPiggys = mDailyPiggyDetails.getJobIDs().size();
		int nNumberOfTallies = mDailyTallyDetails.getTallyIDs().size();

		if (position < nNumberOfShiftsContinuing)
		{
			// The list item is a Shift Instance Continuing.

			int nShiftContinuingIndex = position;

			mListItemHolder.ivShift.setVisibility(View.VISIBLE);
			mListItemHolder.ivPiggy.setVisibility(View.GONE);
			mListItemHolder.ivTally.setVisibility(View.GONE);

			mListItemHolder.llShiftTimes.setVisibility(View.VISIBLE);

			mListItemHolder.rlDailyDetails.setBackgroundResource(R.color.bg_control);

			ShiftInstance shiftInstance = DBHelper.getInstance(mContext).getShiftInstance(malShiftInstanceContinuing.get(nShiftContinuingIndex).getShiftInstanceID());

			switch (shiftInstance.getColor())
			{
			case Konstant.COLOR_PURPLE:
				mListItemHolder.ivShift.setBackgroundResource(R.drawable.daily_shift_icon_purple);
				break;
			case Konstant.COLOR_CYAN:
				mListItemHolder.ivShift.setBackgroundResource(R.drawable.daily_shift_icon_cyan);
				break;
			case Konstant.COLOR_GREEN:
				mListItemHolder.ivShift.setBackgroundResource(R.drawable.daily_shift_icon_green);
				break;
			case Konstant.COLOR_MUSTARD:
				mListItemHolder.ivShift.setBackgroundResource(R.drawable.daily_shift_icon_mustard);
				break;
			case Konstant.COLOR_ORANGE:
				mListItemHolder.ivShift.setBackgroundResource(R.drawable.daily_shift_icon_orange);
				break;
			case Konstant.COLOR_RED:
				mListItemHolder.ivShift.setBackgroundResource(R.drawable.daily_shift_icon_red);
				break;
			}

			mListItemHolder.tvFrom.setText(Utils.constructTime(shiftInstance.getFromHour(), shiftInstance.getFromMinute(), mPreferences));
			mListItemHolder.tvTo.setText(Utils.constructTime(shiftInstance.getToHour(), shiftInstance.getToMinute(), mPreferences));

			Job job = DBHelper.getInstance(mContext).getJob(shiftInstance.getJobID());
			mListItemHolder.tvJobName.setText(job.getName());

			mListItemHolder.tvPaidHours.setText(Utils.constructTimeInHrsAndMins(shiftInstance.getPaidMinutes()));

			DecimalFormat df = new DecimalFormat("0.00");
			double dPaidAmount = malShiftInstanceContinuing.get(nShiftContinuingIndex).getAmount();
			mListItemHolder.tvAmount.setText("$" + df.format(dPaidAmount));

			mListItemHolder.ivOvernightShift.setImageResource(R.drawable.cal_shift_continues_from_yesterday);
			mListItemHolder.ivOvernightShift.setVisibility(View.VISIBLE);
		}
		else if ((position >= nNumberOfShiftsContinuing) && (position < (nNumberOfShiftsContinuing + nNumberOfShifts)))
		{
			// The list item is a Shift Instance.

			int nShiftIndex = position - nNumberOfShiftsContinuing;

			mListItemHolder.ivShift.setVisibility(View.VISIBLE);
			mListItemHolder.ivPiggy.setVisibility(View.GONE);
			mListItemHolder.ivTally.setVisibility(View.GONE);

			mListItemHolder.llShiftTimes.setVisibility(View.VISIBLE);

			mListItemHolder.rlDailyDetails.setBackgroundResource(R.color.bg_control);

			switch (mDailyShiftDetails.getColors().get(nShiftIndex))
			{
			case Konstant.COLOR_PURPLE:
				mListItemHolder.ivShift.setBackgroundResource(R.drawable.daily_shift_icon_purple);
				break;
			case Konstant.COLOR_CYAN:
				mListItemHolder.ivShift.setBackgroundResource(R.drawable.daily_shift_icon_cyan);
				break;
			case Konstant.COLOR_GREEN:
				mListItemHolder.ivShift.setBackgroundResource(R.drawable.daily_shift_icon_green);
				break;
			case Konstant.COLOR_MUSTARD:
				mListItemHolder.ivShift.setBackgroundResource(R.drawable.daily_shift_icon_mustard);
				break;
			case Konstant.COLOR_ORANGE:
				mListItemHolder.ivShift.setBackgroundResource(R.drawable.daily_shift_icon_orange);
				break;
			case Konstant.COLOR_RED:
				mListItemHolder.ivShift.setBackgroundResource(R.drawable.daily_shift_icon_red);
				break;
			}

			mListItemHolder.tvFrom.setText(Utils.constructTime(mDailyShiftDetails.getFromHours().get(nShiftIndex), mDailyShiftDetails.getFromMinutes().get(nShiftIndex), mPreferences));
			mListItemHolder.tvTo.setText(Utils.constructTime(mDailyShiftDetails.getToHours().get(nShiftIndex), mDailyShiftDetails.getToMinutes().get(nShiftIndex), mPreferences));

			mListItemHolder.tvJobName.setText(mDailyShiftDetails.getJobNames().get(nShiftIndex));

			int nPaidTimeInMinutes = mDailyShiftDetails.getRegularMinutes().get(nShiftIndex) + mDailyShiftDetails.getOvertimeMinutes().get(nShiftIndex)
					+ mDailyShiftDetails.getSpecialMinutes().get(nShiftIndex);
			mListItemHolder.tvPaidHours.setText(Utils.constructTimeInHrsAndMins(nPaidTimeInMinutes));
			DBHelper.getInstance(mContext).updateShiftInstancePaidMinutes(mDailyShiftDetails.getShiftInstanceIDs().get(nShiftIndex), nPaidTimeInMinutes);

			DecimalFormat df = new DecimalFormat("0.00");
			double dPaidAmount = mDailyShiftDetails.getRegularAmounts().get(nShiftIndex) + mDailyShiftDetails.getOvertimeAmounts().get(nShiftIndex)
					+ mDailyShiftDetails.getSpecialAmounts().get(nShiftIndex);
			if (mPreferences.getBoolean(Konstant.PREF_KEY_TIPS_ON, Konstant.PREF_DEF_TIPS_ON))
				dPaidAmount += mDailyShiftDetails.getTips().get(nShiftIndex);
			if (dPaidAmount < 0)
				dPaidAmount = 0;
			mListItemHolder.tvAmount.setText("$" + df.format(dPaidAmount));
			DBHelper.getInstance(mContext).updateShiftInstanceContinuingAmount(mDailyShiftDetails.getShiftInstanceIDs().get(nShiftIndex), dPaidAmount);

			if (mDailyShiftDetails.getFromHours().get(nShiftIndex) > mDailyShiftDetails.getToHours().get(nShiftIndex))
			{
				mListItemHolder.ivOvernightShift.setImageResource(R.drawable.cal_shift_continues_to_tomorrow);
				mListItemHolder.ivOvernightShift.setVisibility(View.VISIBLE);
			}
			else
			{
				mListItemHolder.ivOvernightShift.setVisibility(View.GONE);
			}
		}
		else if ((position >= (nNumberOfShiftsContinuing + nNumberOfShifts)) && (position < (nNumberOfShiftsContinuing + nNumberOfShifts + nNumberOfPiggys)))
		{
			// The list item is a Piggy.

			int nPiggyIndex = position - nNumberOfShiftsContinuing - nNumberOfShifts;

			mListItemHolder.ivShift.setVisibility(View.GONE);
			mListItemHolder.ivPiggy.setVisibility(View.VISIBLE);
			mListItemHolder.ivTally.setVisibility(View.GONE);

			mListItemHolder.llShiftTimes.setVisibility(View.GONE);

			mListItemHolder.rlDailyDetails.setBackgroundResource(R.color.bg_control);

			mListItemHolder.tvJobName.setText(mDailyPiggyDetails.getJobNames().get(nPiggyIndex));

			int nPaidTimeInMinutes = mDailyPiggyDetails.getRegularMinutes().get(nPiggyIndex) + mDailyPiggyDetails.getOvertimeMinutes().get(nPiggyIndex)
					+ mDailyPiggyDetails.getSpecialMinutes().get(nPiggyIndex);
			mListItemHolder.tvPaidHours.setText(Utils.constructTimeInHrsAndMins(nPaidTimeInMinutes));

			DecimalFormat df = new DecimalFormat("0.00");
			double dPaidAmount = mDailyPiggyDetails.getRegularAmounts().get(nPiggyIndex) + mDailyPiggyDetails.getOvertimeAmounts().get(nPiggyIndex)
					+ mDailyPiggyDetails.getSpecialAmounts().get(nPiggyIndex);
			if (mPreferences.getBoolean(Konstant.PREF_KEY_TIPS_ON, Konstant.PREF_DEF_TIPS_ON))
				dPaidAmount += mDailyPiggyDetails.getTips().get(nPiggyIndex);
			if (mPreferences.getBoolean(Konstant.PREF_KEY_DEDUCTIONS_ON, Konstant.PREF_DEF_DEDUCTIONS_ON))
				dPaidAmount -= mDailyPiggyDetails.getDeductions().get(nPiggyIndex);
			if (dPaidAmount < 0)
				dPaidAmount = 0;
			mListItemHolder.tvAmount.setText("$" + df.format(dPaidAmount));

			mListItemHolder.ivOvernightShift.setVisibility(View.GONE);
		}
		else if ((position >= (nNumberOfShiftsContinuing + nNumberOfShifts + nNumberOfPiggys)) && (position < (nNumberOfShiftsContinuing + nNumberOfShifts + nNumberOfPiggys + nNumberOfTallies)))
		{
			// The list item is a Tally.

			int nTallyIndex = position - nNumberOfShiftsContinuing - nNumberOfShifts - nNumberOfPiggys;

			mListItemHolder.ivShift.setVisibility(View.GONE);
			mListItemHolder.ivPiggy.setVisibility(View.GONE);
			mListItemHolder.ivTally.setVisibility(View.VISIBLE);

			mListItemHolder.llShiftTimes.setVisibility(View.GONE);

			mListItemHolder.rlDailyDetails.setBackgroundResource(R.color.bg_control);

			mListItemHolder.tvJobName.setText("The Past " + mDailyTallyDetails.getTallyCounts().get(nTallyIndex) + " " + mDailyTallyDetails.getTallyTimeUnits().get(nTallyIndex));

			int nNumberOfPaydays = mDailyTallyDetails.getNumberOfPaydays().get(nTallyIndex);
			if (nNumberOfPaydays == 1)
				mListItemHolder.tvPaidHours.setText("1 payday");
			else
				mListItemHolder.tvPaidHours.setText(nNumberOfPaydays + " paydays");

			mListItemHolder.tvAmount.setText(mDailyTallyDetails.getTallyAmounts().get(nTallyIndex));

			mListItemHolder.ivOvernightShift.setVisibility(View.GONE);
		}
		else if (position == (malnDummyList.size() - 1))
		{
			// This is the last item.

			mListItemHolder.rlDailyDetails.setBackgroundResource(R.color.bg_activity);
			mListItemHolder.ivShift.setVisibility(View.GONE);
			mListItemHolder.ivPiggy.setVisibility(View.GONE);
			mListItemHolder.ivTally.setVisibility(View.GONE);
			mListItemHolder.llShiftTimes.setVisibility(View.GONE);
			mListItemHolder.tvJobName.setText("");
			mListItemHolder.tvPaidHours.setText("");
			mListItemHolder.tvAmount.setText("");

			mListItemHolder.ivOvernightShift.setVisibility(View.GONE);
		}

		row.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				int nNumberOfShiftsContinuing = malShiftInstanceContinuing.size();
				int nNumberOfShifts = mDailyShiftDetails.getJobIDs().size();
				int nNumberOfPiggys = mDailyPiggyDetails.getJobIDs().size();
				int nNumberOfTallies = mDailyTallyDetails.getTallyIDs().size();

				if (position < nNumberOfShiftsContinuing)
				{
					// The list item is a Shift Instance Continuing.

					int nShiftContinuingIndex = position;

					Intent intent = new Intent(mContext, ActivityShiftInstancesAdd.class);
					intent.putExtra(Konstant.KEY_SELECTED_SII_ID, malShiftInstanceContinuing.get(nShiftContinuingIndex).getShiftInstanceID());
					mContext.startActivity(intent);
				}
				else if ((position >= nNumberOfShiftsContinuing) && (position < (nNumberOfShiftsContinuing + nNumberOfShifts)))
				{
					// The list item is a Shift Instance.

					int nShiftIndex = position - nNumberOfShiftsContinuing;

					Intent intent = new Intent(mContext, ActivityShiftInstancesAdd.class);
					intent.putExtra(Konstant.KEY_SELECTED_SII_ID, mDailyShiftDetails.getShiftInstanceIDs().get(nShiftIndex));
					mContext.startActivity(intent);
				}
				else if ((position >= (nNumberOfShiftsContinuing + nNumberOfShifts)) && (position < (nNumberOfShiftsContinuing + nNumberOfShifts + nNumberOfPiggys)))
				{
					// The list item is a Piggy.

					boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);
					if (bIsPremium)
					{
						int nPiggyIndex = position - nNumberOfShiftsContinuing - nNumberOfShifts;

						Intent intent = new Intent(mContext, ActivityPayday.class);

						intent.putExtra(Konstant.KEY_PAYDAY_DATE_YEAR, mDailyPiggyDetails.getPaydayYear());
						intent.putExtra(Konstant.KEY_PAYDAY_DATE_MOY, mDailyPiggyDetails.getPaydayMOY());
						intent.putExtra(Konstant.KEY_PAYDAY_DATE_DOM, mDailyPiggyDetails.getPaydayDOM());

						intent.putExtra(Konstant.KEY_PAYDAY_JOB_NAME, mDailyPiggyDetails.getJobNames().get(nPiggyIndex));

						intent.putExtra(Konstant.KEY_PAYDAY_DEDUCTION_RATE, mDailyPiggyDetails.getDeductionRates().get(nPiggyIndex));
						intent.putExtra(Konstant.KEY_PAYDAY_DEDUCTION, mDailyPiggyDetails.getDeductions().get(nPiggyIndex));
						intent.putExtra(Konstant.KEY_PAYDAY_TIPS, mDailyPiggyDetails.getTips().get(nPiggyIndex));

						intent.putExtra(Konstant.KEY_PAYDAY_REGULAR_MINUTES, mDailyPiggyDetails.getRegularMinutes().get(nPiggyIndex));
						intent.putExtra(Konstant.KEY_PAYDAY_OVERTIME_MINUTES, mDailyPiggyDetails.getOvertimeMinutes().get(nPiggyIndex));
						intent.putExtra(Konstant.KEY_PAYDAY_SPECIAL_MINUTES, mDailyPiggyDetails.getSpecialMinutes().get(nPiggyIndex));

						intent.putExtra(Konstant.KEY_PAYDAY_REGULAR_AMOUNT, mDailyPiggyDetails.getRegularAmounts().get(nPiggyIndex));
						intent.putExtra(Konstant.KEY_PAYDAY_OVERTIME_AMOUNT, mDailyPiggyDetails.getOvertimeAmounts().get(nPiggyIndex));
						intent.putExtra(Konstant.KEY_PAYDAY_SPECIAL_AMOUNT, mDailyPiggyDetails.getSpecialAmounts().get(nPiggyIndex));

						mContext.startActivity(intent);
					}
					else
					{
						DialogUpgrade dialogUpgrade = new DialogUpgrade(mActivity, mContext);
						dialogUpgrade.displayDialog();
					}
				}
				else if ((position >= (nNumberOfShiftsContinuing + nNumberOfShifts + nNumberOfPiggys))
						&& (position < (nNumberOfShiftsContinuing + nNumberOfShifts + nNumberOfPiggys + nNumberOfTallies)))
				{
					// The list item is a Tally.
				}
				else if (position == (malnDummyList.size() - 1))
				{
					// This is the last item.
				}
			}
		});

		row.setOnLongClickListener(new OnLongClickListener()
		{
			@Override
			public boolean onLongClick(View v)
			{
				int nNumberOfShiftsContinuing = malShiftInstanceContinuing.size();
				int nNumberOfShifts = mDailyShiftDetails.getJobIDs().size();

				if (position < nNumberOfShiftsContinuing)
				{
					// The list item is a Shift Instance Continuing.

					int nShiftContinuingIndex = position;
					int nShiftInstanceID = malShiftInstanceContinuing.get(nShiftContinuingIndex).getShiftInstanceID();

					displayDeleteShiftDialog(nShiftInstanceID);
				}
				else if ((position >= nNumberOfShiftsContinuing) && (position < (nNumberOfShiftsContinuing + nNumberOfShifts)))
				{
					// The list item is a Shift Instance.

					int nShiftIndex = position - nNumberOfShiftsContinuing;
					int nShiftInstanceID = mDailyShiftDetails.getShiftInstanceIDs().get(nShiftIndex);

					displayDeleteShiftDialog(nShiftInstanceID);
				}

				return true;
			}
		});

		return row;
	}

	private void displayDeleteShiftDialog(final int nShiftInstanceID)
	{
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
		dialogBuilder.setTitle(R.string.app_name);
		dialogBuilder.setItems(R.array.shift_on_long_click, new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int nPosition)
			{
				if (nPosition == 0)
				{
					long lCalendarEventID = DBHelper.getInstance(mContext).getShiftInstance(nShiftInstanceID).getCalendarEventID();

					DBHelper.getInstance(mContext).deleteShiftInstance(nShiftInstanceID);
					DBHelper.getInstance(mContext).deleteShiftInstanceReminders(nShiftInstanceID);
					DBHelper.getInstance(mContext).deleteShiftInstanceContinuing(nShiftInstanceID);

					UtilsCalendar.deleteShiftEvent(mContext.getContentResolver(), lCalendarEventID);

					((ActivityHome) mContext).refreshHomeScreen();
				}
				else
				{
					Intent intent = new Intent(mContext, ActivityShiftInstancesAdd.class);
					intent.putExtra(Konstant.KEY_SELECTED_SII_ID, nShiftInstanceID);
					mContext.startActivity(intent);
				}
			}
		});

		dialogBuilder.show();
	}

	public void changeDataset(ArrayList<Integer> alnDummyList, ArrayList<ShiftInstanceContinuing> alShiftInstanceContinuing, DailyShiftDetails dailyShiftDetails, DailyPiggyDetails dailyPiggyDetails,
			DailyTallyDetails dailyTallyDetails)
	{
		mDailyShiftDetails = null;
		mDailyShiftDetails = dailyShiftDetails;

		mDailyPiggyDetails = null;
		mDailyPiggyDetails = dailyPiggyDetails;

		mDailyTallyDetails = null;
		mDailyTallyDetails = dailyTallyDetails;

		malShiftInstanceContinuing.clear();
		malShiftInstanceContinuing.addAll(alShiftInstanceContinuing);

		malnDummyList.clear();
		malnDummyList.addAll(alnDummyList);

		notifyDataSetChanged();
	}

	public void clearDataset()
	{
		mDailyShiftDetails = null;
		mDailyPiggyDetails = null;
		mDailyTallyDetails = null;
		malShiftInstanceContinuing.clear();
		malnDummyList.clear();

		notifyDataSetChanged();
	}

	static class DailyDetailHolder
	{
		RelativeLayout rlDailyDetails;

		ImageView ivShift;
		ImageView ivPiggy;
		ImageView ivTally;

		ImageView ivOvernightShift;

		LinearLayout llShiftTimes;
		TextView tvFrom;
		TextView tvTo;

		TextView tvJobName;
		TextView tvPaidHours;

		TextView tvAmount;

		boolean bAdded = true;
	}
}
