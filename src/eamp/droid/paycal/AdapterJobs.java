/*
 * AdapterJobs
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterJobs extends ArrayAdapter<String>
{
	private Context mContext;

	private ArrayList<Integer> malnJobIDs;
	private ArrayList<String> malsJobNames;
	private ArrayList<Integer> malnJobColors;

	private ExistingJob mListItemHolder;

	private int mLayoutResourceId;

	private SharedPreferences mPreferences;

	public AdapterJobs(SharedPreferences preferences, Context context, int layoutResourceId, ArrayList<Integer> alnJobIDs, ArrayList<String> alsJobNames, ArrayList<Integer> alnJobColors)
	{
		super(context, layoutResourceId, alsJobNames);

		mPreferences = preferences;
		mContext = context;
		mLayoutResourceId = layoutResourceId;

		malnJobIDs = alnJobIDs;
		malsJobNames = alsJobNames;
		malnJobColors = alnJobColors;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		View row = convertView;
		if (row == null)
		{
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			row = inflater.inflate(mLayoutResourceId, parent, false);
			mListItemHolder = new ExistingJob();
			mListItemHolder.ivColor = (ImageView) row.findViewById(R.id.iv_lij_color);
			mListItemHolder.tvName = (TextView) row.findViewById(R.id.tv_lij_name);
			mListItemHolder.ivDelete = (ImageView) row.findViewById(R.id.iv_lij_delete);
			row.setTag(mListItemHolder);
		}
		else
		{
			mListItemHolder = (ExistingJob) row.getTag();
		}

		mListItemHolder.ivDelete.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
				builder.setTitle(R.string.delete_job);
				builder.setMessage(R.string.msg_prompt_job_delete);
				builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int id)
					{
						DBHelper.getInstance(mContext).deleteJob(malnJobIDs.get(position));

						SharedPreferences.Editor prefEditor = mPreferences.edit();
						prefEditor.remove(Konstant.PREF_KEY_PIGGY_DATES_FOR + malnJobIDs.get(position));
						prefEditor.commit();

						Utils.msaPiggyDates = Utils.getPiggyDatesMappedByJobID(mContext, mPreferences);

						malnJobIDs.remove(position);
						malsJobNames.remove(position);
						malnJobColors.remove(position);

						notifyDataSetChanged();

						ActivityJobs.notifyJobsChanged();
					}
				});
				builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int id)
					{
					}
				});
				builder.create();
				builder.show();
			}
		});

		row.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(mContext, ActivityJobsAdd.class);
				intent.putExtra(Konstant.KEY_SELECTED_JOB_ID, malnJobIDs.get(position));
				mContext.startActivity(intent);
			}
		});

		int nColor = R.color.color_green;
		switch (malnJobColors.get(position))
		{
		case Konstant.COLOR_PURPLE:
			nColor = R.color.color_purple;
			break;
		case Konstant.COLOR_CYAN:
			nColor = R.color.color_cyan;
			break;
		case Konstant.COLOR_GREEN:
			nColor = R.color.color_green;
			break;
		case Konstant.COLOR_MUSTARD:
			nColor = R.color.color_mustard;
			break;
		case Konstant.COLOR_ORANGE:
			nColor = R.color.color_orange;
			break;
		case Konstant.COLOR_RED:
			nColor = R.color.color_red;
			break;
		}
		mListItemHolder.ivColor.setBackgroundResource(nColor);
		mListItemHolder.tvName.setText(malsJobNames.get(position));
		mListItemHolder.ivDelete.setImageResource(R.drawable.btn_delete);

		return row;
	}

	public void changeDataset(ArrayList<Integer> alnIDs, ArrayList<String> alsNames, ArrayList<Integer> alnColors)
	{
		malnJobIDs.clear();
		malnJobIDs.addAll(alnIDs);

		malsJobNames.clear();
		malsJobNames.addAll(alsNames);

		malnJobColors.clear();
		malnJobColors.addAll(alnColors);

		notifyDataSetChanged();
	}

	static class ExistingJob
	{
		ImageView ivColor;
		TextView tvName;
		ImageView ivDelete;
		boolean bAdded = true;
	}
}
