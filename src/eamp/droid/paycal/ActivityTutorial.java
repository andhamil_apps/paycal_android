/*
 * ActivityTutorial
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

public class ActivityTutorial extends FragmentActivity
{
	private static ViewPager mViewPager;

	private PagerAdapter mPagerAdapter;
	
	private int mnActivityLaunchFrom;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tutorial);
		
		ActionBar currentActionBar = getActionBar();
		currentActionBar.hide();
		
		mPagerAdapter = new EnFragmentStatePagerAdapter(getFragmentManager());

		mViewPager = (ViewPager) findViewById(R.id.vp_tutorial);
		mViewPager.setAdapter(mPagerAdapter);
		
		mnActivityLaunchFrom = getIntent().getIntExtra(Konstant.KEY_TUTORIAL_LAUNCH_FROM, Konstant.ACTIVITY_SPLASH);
	}
	
	private class EnFragmentStatePagerAdapter extends FragmentStatePagerAdapter
	{
		public EnFragmentStatePagerAdapter(FragmentManager fm)
		{
			super(fm);
		}

		@Override
		public Fragment getItem(int position)
		{
			return FragmentTutorial.createFragment(position, ActivityTutorial.this, mnActivityLaunchFrom);
		}

		@Override
		public int getCount()
		{
			return 4;
		}
	}
}
