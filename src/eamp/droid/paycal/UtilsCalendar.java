/*
 * UtilsCalendar
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.util.Calendar;
import java.util.TimeZone;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.provider.CalendarContract.Reminders;

public class UtilsCalendar
{
	public static long mlCalendarID;
	
	public static long addShiftEvent(ContentResolver crEvent, String strJobName, int nStartYear, int nStartMOY, int nStartDOM, int nStartHour, int nStartMinute, int nEndYear, int nEndMOY,
			int nEndDOM, int nEndHour, int nEndMinute)
	{
		long lEventID = -1;

		long startMillis = 0;
		long endMillis = 0;

		Calendar beginTime = Calendar.getInstance();
		beginTime.set(nStartYear, nStartMOY, nStartDOM, nStartHour, nStartMinute);
		startMillis = beginTime.getTimeInMillis();

		Calendar endTime = Calendar.getInstance();
		endTime.set(nEndYear, nEndMOY, nEndDOM, nEndHour, nEndMinute);
		endMillis = endTime.getTimeInMillis();

		ContentValues cvEvent = new ContentValues();
		
		cvEvent.put(Events.CALENDAR_ID, mlCalendarID);
		cvEvent.put(Events.TITLE, strJobName);
		cvEvent.put(Events.ACCESS_LEVEL, Events.ACCESS_DEFAULT);
		
		cvEvent.put(Events.DTSTART, startMillis);
		cvEvent.put(Events.DTEND, endMillis);
		
		TimeZone timeZone = TimeZone.getDefault();
		cvEvent.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());
		
		cvEvent.put(Events.HAS_ALARM, false);
		
		Uri uriEvent = crEvent.insert(Events.CONTENT_URI, cvEvent);

		lEventID = Long.parseLong(uriEvent.getLastPathSegment());

		return lEventID;
	}

	public static long addShiftRemainder(ContentResolver crReminder, long lEventID, int nReminderIndex)
	{

		long lReminderID = -1;

		ContentValues values = new ContentValues();

		String strTime = Konstant.REMAINDERS[nReminderIndex].substring(0, Konstant.REMAINDERS[nReminderIndex].indexOf(" "));
		Integer nTime = Integer.parseInt(strTime);

		if (Konstant.REMAINDERS[nReminderIndex].endsWith("hour") || Konstant.REMAINDERS[nReminderIndex].endsWith("hours"))
		{
			values.put(Reminders.MINUTES, (nTime * 60));
		}
		else if (Konstant.REMAINDERS[nReminderIndex].endsWith("day") || Konstant.REMAINDERS[nReminderIndex].endsWith("days"))
		{
			values.put(Reminders.MINUTES, (nTime * 60 * 24));
		}
		else if (Konstant.REMAINDERS[nReminderIndex].endsWith("week") || Konstant.REMAINDERS[nReminderIndex].endsWith("weeks"))
		{
			values.put(Reminders.MINUTES, (nTime * 60 * 24 * 7));
		}
		else
		{
			values.put(Reminders.MINUTES, nTime);
		}

		values.put(Reminders.EVENT_ID, lEventID);
		values.put(Reminders.METHOD, Reminders.METHOD_DEFAULT);
		Uri uriReminder = crReminder.insert(Reminders.CONTENT_URI, values);

		lReminderID = Long.parseLong(uriReminder.getLastPathSegment());
		return lReminderID;
	}

	public static long deleteShiftEvent(ContentResolver crEvent, long lEventID)
	{
		int nResult = 0;

		String selection = "(" + Events._ID + " = ?)";
		String[] selectionArgs = new String[] { String.valueOf(lEventID) };
		nResult = crEvent.delete(CalendarContract.Events.CONTENT_URI, selection, selectionArgs);

		return nResult;
	}
}
