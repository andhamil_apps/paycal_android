/*
 * DialogSignin
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DialogSignin extends Dialog
{
	private Context mContext;

	private SharedPreferences mPreferences;

	public DialogSignin(Context context)
	{
		super(context);
		mContext = context;
		this.setTitle(mContext.getString(R.string.signin));
		this.setContentView(R.layout.dialog_signin);
		this.setCancelable(false);
		
		mPreferences = mContext.getSharedPreferences(Konstant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
	}

	public void displayDialog()
	{
		final EditText etPasscode = (EditText) findViewById(R.id.et_passcode);
		final Button btnSignin = (Button) findViewById(R.id.btn_signin);

		etPasscode.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if (s.equals(Konstant.PREF_DEF_PASSCODE))
				{
					btnSignin.setEnabled(false);
				}
				else
				{
					btnSignin.setEnabled(true);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
			}

			@Override
			public void afterTextChanged(Editable s)
			{
			}
		});

		btnSignin.setEnabled(false);
		btnSignin.setOnClickListener(new android.view.View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String strEnteredPasscode = etPasscode.getText().toString();
				String strSavedPasscode = mPreferences
						.getString(Konstant.PREF_KEY_PASSCODE, Konstant.PREF_DEF_PASSCODE);
				if (strEnteredPasscode.equals(strSavedPasscode))
				{
					dismiss();
				}
				else
				{
					Toast.makeText(mContext, mContext.getString(R.string.msg_passcode_wrong), Toast.LENGTH_SHORT)
							.show();
				}
			}
		});

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		getWindow().setAttributes(lp);
		show();
	}
}
