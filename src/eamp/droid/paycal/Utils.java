/*
 * Utils
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnCancelListener;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract.Calendars;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

public class Utils
{
	public static boolean mbIsTallyOn;

	public static SparseArray<ArrayList<String>> msaPiggyDates;
	public static SparseArray<ArrayList<String>> msaTallyDates;

	public static ArrayList<String> calculatePiggyDatesUsingWeeks(int nFromYear, int nFromMOY, int nFromDOM, int nCount)
	{
		ArrayList<String> alsTallyDates = new ArrayList<String>();

		Calendar currentCal = Calendar.getInstance();
		currentCal.set(nFromYear, nFromMOY, nFromDOM);

		currentCal.add(Calendar.DAY_OF_YEAR, -(7 * nCount));

		while (currentCal.get(Calendar.YEAR) < (nFromYear + Konstant.DEFAULT_NUMBER_OF_YEARS))
		{
			alsTallyDates.add(currentCal.get(Calendar.YEAR) + "-" + currentCal.get(Calendar.MONTH) + "-" + currentCal.get(Calendar.DAY_OF_MONTH));
			currentCal.add(Calendar.DAY_OF_YEAR, 7 * nCount);
		}

		return alsTallyDates;
	}

	public static ArrayList<String> calculateTallyDatesUsingWeeks(int nFromYear, int nFromMOY, int nFromDOM, int nCount, int nOn)
	{
		ArrayList<String> alsTallyDates = new ArrayList<String>();

		Calendar currentCal = Calendar.getInstance();
		currentCal.set(nFromYear, nFromMOY, nFromDOM);

		int nCalDay = currentCal.get(Calendar.DAY_OF_WEEK);
		currentCal.add(Calendar.DAY_OF_YEAR, (7 - nCalDay + nOn));

		while (currentCal.get(Calendar.YEAR) < (nFromYear + Konstant.DEFAULT_NUMBER_OF_YEARS))
		{
			alsTallyDates.add(currentCal.get(Calendar.YEAR) + "-" + currentCal.get(Calendar.MONTH) + "-" + currentCal.get(Calendar.DAY_OF_MONTH));
			currentCal.add(Calendar.DAY_OF_YEAR, 7 * nCount);
		}

		return alsTallyDates;
	}

	public static ArrayList<String> calculatePiggyDatesUsingMonths(int nFromYear, int nFromMOY, int nFromDOM)
	{
		ArrayList<String> alsTallyDates = new ArrayList<String>();

		Calendar currentCal = Calendar.getInstance();
		currentCal.set(nFromYear, nFromMOY, nFromDOM);

		boolean bLastDayOfMonth = false;
		if (currentCal.getActualMaximum(Calendar.DAY_OF_MONTH) == nFromDOM)
			bLastDayOfMonth = true;

		currentCal.add(Calendar.MONTH, -1);

		while (currentCal.get(Calendar.YEAR) < (nFromYear + Konstant.DEFAULT_NUMBER_OF_YEARS))
		{
			int nYear = currentCal.get(Calendar.YEAR);
			int nMOY = currentCal.get(Calendar.MONTH);
			int nDOM = currentCal.get(Calendar.DAY_OF_MONTH);
			alsTallyDates.add(nYear + "-" + nMOY + "-" + nDOM);

			currentCal.add(Calendar.MONTH, 1);

			if (bLastDayOfMonth)
				currentCal.set(Calendar.DAY_OF_MONTH, currentCal.getActualMaximum(Calendar.DAY_OF_MONTH));
			else
			{
				if (currentCal.getActualMaximum(Calendar.DAY_OF_MONTH) >= nFromDOM)
					currentCal.set(Calendar.DAY_OF_MONTH, nFromDOM);
			}
		}

		return alsTallyDates;
	}

	public static ArrayList<String> calculateTallyDatesUsingMonths(int nFromYear, int nFromMOY, int nFromDOM, int nCount, int nOn)
	{
		ArrayList<String> alsTallyDates = new ArrayList<String>();

		Calendar currentCal = Calendar.getInstance();
		currentCal.set(nFromYear, nFromMOY, nFromDOM);

		if ((nOn == 0 && nFromDOM > 1) || (nOn == 1 && nFromDOM > 15))
			currentCal.add(Calendar.MONTH, nCount);

		while (currentCal.get(Calendar.YEAR) < (nFromYear + Konstant.DEFAULT_NUMBER_OF_YEARS))
		{
			int nDay = 1;
			switch (nOn)
			{
			case 0:
				nDay = 1;
				break;
			case 1:
				nDay = 15;
				break;
			case 2:
				nDay = currentCal.getActualMaximum(Calendar.DAY_OF_MONTH);
			}
			alsTallyDates.add(currentCal.get(Calendar.YEAR) + "-" + currentCal.get(Calendar.MONTH) + "-" + nDay);
			currentCal.add(Calendar.MONTH, nCount);
		}

		return alsTallyDates;
	}

	public static String constructDate(int nYear, int nMOY, int nDOM)
	{
		String strDate = "";

		try
		{
			String strDOB = nYear + "/" + (nMOY + 1) + "/" + nDOM;
			SimpleDateFormat curFormater = new SimpleDateFormat("yyyy/MM/dd", Locale.CANADA);
			Date dateObj = curFormater.parse(strDOB);
			SimpleDateFormat postFormater = new SimpleDateFormat("MMMM dd, yyyy", Locale.CANADA);
			strDate = postFormater.format(dateObj);
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}

		return strDate;
	}

	public static String constructPaydayTitle(int nYear, int nMOY, int nDOM)
	{
		String strDate = "";

		try
		{
			String strDOB = nYear + "/" + (nMOY + 1) + "/" + nDOM;
			SimpleDateFormat curFormater = new SimpleDateFormat("yyyy/MM/dd", Locale.CANADA);
			Date dateObj = curFormater.parse(strDOB);
			SimpleDateFormat postFormater = new SimpleDateFormat("MMMM d", Locale.CANADA);
			strDate = postFormater.format(dateObj);
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}

		if (strDate.endsWith("11"))
			strDate += "th Pay";
		else if (strDate.endsWith("12"))
			strDate += "th Pay";
		else if (strDate.endsWith("13"))
			strDate += "th Pay";
		else if (strDate.endsWith("1"))
			strDate += "st Pay";
		else if (strDate.endsWith("2"))
			strDate += "nd Pay";
		else if (strDate.endsWith("3"))
			strDate += "rd Pay";
		else
			strDate += "th Pay";

		return strDate;
	}

	public static String constructDate(long nMilliSeconds)
	{
		String strDate = "";

		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(nMilliSeconds);
		int nYear = c.get(Calendar.YEAR);
		int nMOY = c.get(Calendar.MONTH) + 1;
		int nDOM = c.get(Calendar.DAY_OF_MONTH);

		try
		{
			String strDOB = nYear + "/" + nMOY + "/" + nDOM;
			SimpleDateFormat curFormater = new SimpleDateFormat("yyyy/MM/dd", Locale.CANADA);
			Date dateObj = curFormater.parse(strDOB);
			SimpleDateFormat postFormater = new SimpleDateFormat("MMMM dd, yyyy", Locale.CANADA);
			strDate = postFormater.format(dateObj);
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}

		return strDate;
	}

	public static String constructTime(int nHour, int nMinute, SharedPreferences preferences)
	{
		String strTime = "";
		if (preferences.getBoolean(Konstant.PREF_KEY_MILITARY_TIME_ON, Konstant.PREF_DEF_MILITARY_TIME_ON))
		{
			if (nHour >= 0 && nHour < 10)
				strTime += "0";
			strTime += nHour;
			strTime += ":";
			if (nMinute >= 0 && nMinute < 10)
				strTime += "0";
			strTime += nMinute;
		}
		else
		{
			if (nHour > 12)
			{
				nHour -= 12;

				if (nHour >= 0 && nHour < 10)
					strTime += "0";
				strTime += nHour;
				strTime += ":";
				if (nMinute >= 0 && nMinute < 10)
					strTime += "0";
				strTime += nMinute;
				strTime += " PM";
			}
			else if (nHour == 12)
			{
				if (nHour >= 0 && nHour < 10)
					strTime += "0";
				strTime += nHour;
				strTime += ":";
				if (nMinute >= 0 && nMinute < 10)
					strTime += "0";
				strTime += nMinute;
				strTime += " PM";
			}
			else if (nHour == 0)
			{
				nHour += 12;
				if (nHour >= 0 && nHour < 10)
					strTime += "0";
				strTime += nHour;
				strTime += ":";
				if (nMinute >= 0 && nMinute < 10)
					strTime += "0";
				strTime += nMinute;
				strTime += " AM";
			}
			else
			{
				if (nHour >= 0 && nHour < 10)
					strTime += "0";
				strTime += nHour;
				strTime += ":";
				if (nMinute >= 0 && nMinute < 10)
					strTime += "0";
				strTime += nMinute;
				strTime += " AM";
			}
		}
		return strTime;
	}

	public static String constructTimeInHrsAndMins(int nTimeInMins)
	{
		String strTime = "";

		int nHours = nTimeInMins / 60;
		int nMinutes = nTimeInMins % 60;

		if (nHours > 0)
		{
			if (nHours >= 0 && nHours < 10)
				strTime += "0";
			strTime += nHours;
			if (nHours == 1)
				strTime += " Hour ";
			else
				strTime += " Hours ";
		}
		if (nMinutes >= 0 && nMinutes < 10)
			strTime += "0";
		strTime += nMinutes;
		if (nMinutes == 1)
			strTime += " Minute";
		else
			strTime += " Minutes";

		return strTime;
	}

	public static String constructPaidHours(int nFromHour, int nFromMinute, int nToHour, int nToMinute, int nUnPaidBreak)
	{
		String strPaidHours = "";

		int nStart = nFromHour * 60 + nFromMinute;
		int nEnd = nToHour * 60 + nToMinute;

		int nPaidHours = 0;
		if (nStart <= nEnd)
		{
			nPaidHours = nEnd - nStart - nUnPaidBreak;
			int nHours = nPaidHours / 60;
			int nMinutes = nPaidHours % 60;

			if (nHours > 0)
			{
				if (nHours >= 0 && nHours < 10)
					strPaidHours += "0";
				strPaidHours += nHours;
				if (nHours == 1)
					strPaidHours += " Hour ";
				else
					strPaidHours += " Hours ";
			}
			if (nMinutes >= 0 && nMinutes < 10)
				strPaidHours += "0";
			strPaidHours += nMinutes;
			if (nMinutes == 1)
				strPaidHours += " Minute";
			else
				strPaidHours += " Minutes";
		}

		return strPaidHours;
	}

	public static double convertAmountIntoDouble(String strPay)
	{
		double dReturn = 0;

		String strWithout$ = strPay;
		if (strPay.contains("$"))
			strWithout$ = strPay.substring(strPay.indexOf("$") + 1);
		dReturn = Double.parseDouble(strWithout$);

		return dReturn;
	}

	public static String convertAmountIntoString(double dPay)
	{
		String strReturn = "";

		strReturn = "$" + dPay;

		return strReturn;
	}

	public static String convertAmountIntoString(String strPay)
	{
		String strReturn = "";

		strReturn = "$" + Double.parseDouble(strPay);

		return strReturn;
	}

	public static double convertPayDeductionIntoDouble(String strPayDeduction)
	{
		double dReturn = 0;

		String strWithoutH = strPayDeduction;
		if (strPayDeduction.contains("%"))
			strWithoutH = strPayDeduction.substring(0, strPayDeduction.indexOf("%"));
		dReturn = Double.parseDouble(strWithoutH);

		return dReturn;
	}

	public static String convertPayDeductionIntoString(double dPayDeduction)
	{
		String strReturn = "";

		strReturn = dPayDeduction + "%";

		return strReturn;
	}

	public static String convertPayDeductionIntoString(String strPayDeduction)
	{
		String strReturn = "";

		strReturn = Double.parseDouble(strPayDeduction) + "%";

		return strReturn;
	}

	public static double convertRateIntoDouble(String strOTRate)
	{
		double dReturn = 0;

		String strWithoutX = strOTRate;
		if (strOTRate.contains("x"))
			strWithoutX = strOTRate.substring(0, strOTRate.indexOf("x"));
		dReturn = Double.parseDouble(strWithoutX);

		return dReturn;
	}

	public static String convertRateIntoString(double dJobPay, double dOTRate)
	{
		String strReturn = "";

		DecimalFormat df = new DecimalFormat("0.00");

		double dOTPay = dJobPay * dOTRate;

		strReturn = df.format(dOTRate) + "x ($" + df.format(dOTPay) + ")";

		return strReturn;
	}

	public static String convertRateIntoString(String strJobPay, String strOTRate)
	{
		String strReturn = "";

		DecimalFormat df = new DecimalFormat("0.00");

		if (strJobPay.contains("$"))
			strJobPay = strJobPay.substring(strJobPay.indexOf("$") + 1);
		double dJobPay = Double.parseDouble(strJobPay);

		if (strOTRate.contains("x"))
			strOTRate = strOTRate.substring(0, strOTRate.indexOf("x"));
		double dOTRate = Double.parseDouble(strOTRate);

		double dOTPay = dJobPay * dOTRate;

		strReturn = df.format(dOTRate) + "x ($" + df.format(dOTPay) + ")";

		return strReturn;
	}

	public static int convertHrsIntoInt(String strOTHrs)
	{
		int nReturn = 0;

		String strWithoutH = strOTHrs;
		if (strOTHrs.contains("h"))
			strWithoutH = strOTHrs.substring(0, strOTHrs.indexOf("h"));
		nReturn = Integer.parseInt(strWithoutH);

		return nReturn;
	}

	public static String convertHrsIntoString(int nOTHrs)
	{
		String strReturn = "";

		strReturn = nOTHrs + "h";

		return strReturn;
	}

	public static String convertHrsIntoString(String strOTHrs)
	{
		String strReturn = "";

		strReturn = Integer.parseInt(strOTHrs) + "h";

		return strReturn;
	}

	public static int convertMinsIntoInt(String strOTMins)
	{
		int nReturn = 0;

		String strWithoutH = strOTMins;
		if (strOTMins.contains("m"))
			strWithoutH = strOTMins.substring(0, strOTMins.indexOf("m"));
		nReturn = Integer.parseInt(strWithoutH);

		return nReturn;
	}

	public static String convertMinsIntoString(int nOTMins)
	{
		String strReturn = "";

		strReturn = nOTMins + "m";

		return strReturn;
	}

	public static String convertMinsIntoString(String strOTMins)
	{
		String strReturn = "";

		strReturn = Integer.parseInt(strOTMins) + "m";

		return strReturn;
	}

	public static int getShiftTemplateIcon(int nIconCode)
	{
		switch (nIconCode)
		{
		case 1:
			return R.drawable.st_01_suitcase;
		case 2:
			return R.drawable.st_02_tags;
		case 3:
			return R.drawable.st_03_gear;
		case 4:
			return R.drawable.st_04_skull_n_bones;
		case 5:
			return R.drawable.st_05_weather;
		case 6:
			return R.drawable.st_06_planet;
		case 7:
			return R.drawable.st_07_cabinet;
		case 8:
			return R.drawable.st_08_coffee;
		case 9:
			return R.drawable.st_09_medical;
		case 10:
			return R.drawable.st_10_airplane;
		case 11:
			return R.drawable.st_11_fork_and_knife;
		case 12:
			return R.drawable.st_12_baby;
		case 13:
			return R.drawable.st_13_camera;
		case 14:
			return R.drawable.st_14_palette;
		case 15:
			return R.drawable.st_15_food;
		case 16:
			return R.drawable.st_16_scales;
		case 17:
			return R.drawable.st_17_martini;
		case 18:
			return R.drawable.st_18_imac;
		case 19:
			return R.drawable.st_19_pencil;
		case 20:
			return R.drawable.st_20_bank;
		case 21:
			return R.drawable.st_21_note;
		case 22:
			return R.drawable.st_22_factory;
		case 23:
			return R.drawable.st_23_clapboard;
		case 24:
			return R.drawable.st_24_ticket;
		}
		return -1;
	}

	public static boolean intToBoolean(int nValue)
	{
		if (nValue == 0)
			return true;
		else
			return false;
	}

	public static int booleanToInt(boolean bValue)
	{
		if (bValue)
			return 0;
		else
			return 1;
	}

	public static int getAndroidColor(int nIndex)
	{
		int nColor = 0;
		switch (nIndex)
		{
		case Konstant.COLOR_PURPLE:
			nColor = R.color.color_purple;
			break;
		case Konstant.COLOR_CYAN:
			nColor = R.color.color_cyan;
			break;
		case Konstant.COLOR_GREEN:
			nColor = R.color.color_green;
			break;
		case Konstant.COLOR_MUSTARD:
			nColor = R.color.color_mustard;
			break;
		case Konstant.COLOR_ORANGE:
			nColor = R.color.color_orange;
			break;
		case Konstant.COLOR_RED:
			nColor = R.color.color_red;
			break;

		}
		return nColor;
	}

	public static SparseArray<ArrayList<String>> getPiggyDatesMappedByJobID(Context context, SharedPreferences preferences)
	{
		SparseArray<ArrayList<String>> saPiggyDates = new SparseArray<ArrayList<String>>();
		ArrayList<Integer> alnJobID = DBHelper.getInstance(context).getColIDsFromTabJobs();
		for (int i = 0; i < alnJobID.size(); i++)
		{
			String strSerializedPiggyDates = preferences.getString(Konstant.PREF_KEY_PIGGY_DATES_FOR + alnJobID.get(i), "");
			List<String> lsPiggyDates = Arrays.asList(TextUtils.split(strSerializedPiggyDates, ","));
			ArrayList<String> alsPiggyDates = new ArrayList<String>(lsPiggyDates);

			saPiggyDates.put(alnJobID.get(i), alsPiggyDates);
		}
		return saPiggyDates;
	}

	public static SparseArray<ArrayList<String>> getTallyDatesMappedByTallyID(Context context, SharedPreferences preferences)
	{
		SparseArray<ArrayList<String>> saTallyDates = new SparseArray<ArrayList<String>>();
		ArrayList<Integer> alnTallyID = DBHelper.getInstance(context).getColIDsFromTabTallies();
		for (int i = 0; i < alnTallyID.size(); i++)
		{
			String strSerializedTallyDates = preferences.getString(Konstant.PREF_KEY_TALLY_DATES_FOR + alnTallyID.get(i), "");
			List<String> lsTallyDates = Arrays.asList(TextUtils.split(strSerializedTallyDates, ","));
			ArrayList<String> alsTallyDates = new ArrayList<String>(lsTallyDates);

			saTallyDates.put(alnTallyID.get(i), alsTallyDates);
		}
		return saTallyDates;
	}

	public static boolean isValidDate(String input)
	{
		String formatString = "MM/dd/yyyy";

		try
		{
			SimpleDateFormat format = new SimpleDateFormat(formatString, Locale.CANADA);
			format.setLenient(false);
			format.parse(input);
		}
		catch (ParseException e)
		{
			return false;
		}
		catch (IllegalArgumentException e)
		{
			return false;
		}

		return true;
	}

	public static boolean isLeapYear(int year)
	{
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		return cal.getActualMaximum(Calendar.DAY_OF_YEAR) > 365;
	}

	public static boolean isLastDayOfMonth(int nDOM, int nMOY)
	{
		boolean bReturn = false;
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, nMOY);
		int nMaxDaysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		if (nDOM == nMaxDaysInMonth)
			bReturn = true;
		return bReturn;
	}

	public static boolean isPiggyDay(String strDate)
	{
		boolean bReturn = false;
		for (int i = 0; i < msaPiggyDates.size(); i++)
		{
			ArrayList<String> alsPiggyDates = Utils.msaPiggyDates.valueAt(i);
			if (alsPiggyDates.contains(strDate))
				if (alsPiggyDates.indexOf(strDate) != 0)
					return true;
		}
		return bReturn;
	}

	public static boolean isPiggyDay(String strDate, int nJobID)
	{
		boolean bReturn = false;
		for (int i = 0; i < msaPiggyDates.size(); i++)
		{
			int nCurrentJobID = msaPiggyDates.keyAt(i);
			if (nCurrentJobID == nJobID)
			{
				ArrayList<String> alsPiggyDates = msaPiggyDates.valueAt(i);
				if (alsPiggyDates.contains(strDate))
					if (alsPiggyDates.indexOf(strDate) != 0)
						return true;
			}
		}
		return bReturn;
	}

	public static boolean isTallyDay(String strDate)
	{
		boolean bReturn = false;
		for (int i = 0; i < msaTallyDates.size(); i++)
		{
			ArrayList<String> alsTallyDates = Utils.msaTallyDates.valueAt(i);
			if (alsTallyDates.contains(strDate))
				if (alsTallyDates.indexOf(strDate) != 0)
					return true;
		}
		return bReturn;
	}

	public static void setupCalendarID(final Context context, final SharedPreferences preferences)
	{
		// Android Calendar
		final String[] EVENT_PROJECTION = new String[] {
		/* 00 */Calendars._ID,
		/* 01 */Calendars.ACCOUNT_NAME,
		/* 02 */Calendars.CALENDAR_DISPLAY_NAME,
		/* 03 */Calendars.OWNER_ACCOUNT };

		// The indices for the projection array above.
		final int PROJECTION_ID_INDEX = 0;
		final int PROJECTION_DISPLAY_NAME_INDEX = 2;

		AlertDialog.Builder dialogAccounts = new AlertDialog.Builder(context);
		dialogAccounts.setTitle(R.string.choose_account);
		dialogAccounts.setOnCancelListener(new OnCancelListener()
		{
			@Override
			public void onCancel(DialogInterface dialog)
			{
				Toast.makeText(context, context.getString(R.string.msg_no_reminder_without_calendar), Toast.LENGTH_SHORT).show();
			}
		});

		final AlertDialog.Builder dialogCalendars = new AlertDialog.Builder(context);
		dialogCalendars.setTitle(R.string.choose_calendar);
		dialogCalendars.setOnCancelListener(new OnCancelListener()
		{
			@Override
			public void onCancel(DialogInterface dialog)
			{
				Toast.makeText(context, context.getString(R.string.msg_no_reminder_without_calendar), Toast.LENGTH_SHORT).show();
			}
		});

		AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
		Account[] accounts = accountManager.getAccounts();

		final ArrayList<String> alsAccountNames = new ArrayList<String>();
		for (int i = 0; i < accounts.length; i++)
		{
			if (accounts[i].type.equals("com.google"))
			{
				String strAccountName = accounts[i].name;
				alsAccountNames.add(strAccountName);
			}
		}

		String[] arrAccountNames = alsAccountNames.toArray(new String[alsAccountNames.size()]);
		if (arrAccountNames.length > 0)
		{
			dialogAccounts.setItems(arrAccountNames, new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					String strSelectedAccountName = alsAccountNames.get(which);

					Cursor cur = null;
					ContentResolver cr = context.getContentResolver();
					Uri uri = Calendars.CONTENT_URI;
					String selection = "((" + Calendars.ACCOUNT_NAME + " = ?) AND (" + Calendars.ACCOUNT_TYPE + " = ?) AND (" + Calendars.OWNER_ACCOUNT + " = ?))";
					String[] selectionArgs = new String[] { strSelectedAccountName, "com.google", strSelectedAccountName };

					cur = cr.query(uri, EVENT_PROJECTION, selection, selectionArgs, null);

					final ArrayList<Long> alnCalendarIDs = new ArrayList<Long>();
					ArrayList<String> alsDisplayNames = new ArrayList<String>();
					while (cur.moveToNext())
					{
						alnCalendarIDs.add(cur.getLong(PROJECTION_ID_INDEX));
						alsDisplayNames.add(cur.getString(PROJECTION_DISPLAY_NAME_INDEX));
					}

					String[] arrDisplayNames = alsDisplayNames.toArray(new String[alsDisplayNames.size()]);
					if (arrDisplayNames.length > 0)
					{
						dialogCalendars.setItems(arrDisplayNames, new DialogInterface.OnClickListener()
						{
							public void onClick(DialogInterface dialog, int which)
							{
								long lCalendarID = alnCalendarIDs.get(which);

								SharedPreferences.Editor prefEditor = preferences.edit();
								prefEditor.putLong(Konstant.PREF_KEY_CALENDAR_ID, lCalendarID);
								prefEditor.commit();

								dialog.dismiss();
							}
						});

						dialogCalendars.show();
					}
					else
					{
						Toast.makeText(context, context.getString(R.string.msg_no_calendar_found), Toast.LENGTH_SHORT).show();
					}
					dialog.dismiss();
				}
			});

			dialogAccounts.show();
		}
		else
		{
			Toast.makeText(context, context.getString(R.string.msg_no_account_found), Toast.LENGTH_SHORT).show();
		}
		Log.i("ActivityHome", "setupCalendarID Exit");
	}
}
