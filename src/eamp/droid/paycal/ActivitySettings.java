/*
 * ActivitySettings
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

public class ActivitySettings extends Activity
{
	private static Context mContext;

	private static AdapterTallies mTalliesAdapter;
	private static ArrayList<Integer> malnTallyIDs;

	private static RelativeLayout mrlTallies;
	private static TextView mtvTallies;
	private static ListView mlvTallies;
	private static TextView mtvNoTallyFound;

	private Button mBtnAddTally;

	private TextView mtvUpgradeOnTally;
	private ImageButton mibUpgrade;

	private Switch mswtTally;

	private TextView mtvViewTutorial;

	// private Switch mswtPasscode;

	// private static RelativeLayout mrlPasscode;
	// private TextView mtvSetPasscode;
	// private TextView mtvChangePasscode;

	private SharedPreferences mPreferences;
	private SharedPreferences.Editor mPrefEditor;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		ActionBar currentActionBar = getActionBar();
		currentActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		currentActionBar.setDisplayShowTitleEnabled(false);
		currentActionBar.setCustomView(R.layout.action_bar_items_1);

		TextView tvActivityTitle = (TextView) findViewById(R.id.tv_ab1_title);
		tvActivityTitle.setText(getString(R.string.settings));

		mContext = ActivitySettings.this;

		mPreferences = getSharedPreferences(Konstant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		mPrefEditor = mPreferences.edit();

		mtvUpgradeOnTally = (TextView) findViewById(R.id.tv_settings_tallies_upgrade);
		mtvUpgradeOnTally.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivitySettings.this, ActivitySettings.this);
				dialogUpgrade.displayDialog();
			}
		});

		mibUpgrade = (ImageButton) findViewById(R.id.ib_settings_upgrade);
		mibUpgrade.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivitySettings.this, ActivitySettings.this);
				dialogUpgrade.displayDialog();
			}
		});

		mtvViewTutorial = (TextView) findViewById(R.id.tv_settings_viewtutorial);
		mtvViewTutorial.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent intentTutorial = new Intent(ActivitySettings.this, ActivityTutorial.class);
				intentTutorial.putExtra(Konstant.KEY_TUTORIAL_LAUNCH_FROM, Konstant.ACTIVITY_SETTINGS);
				startActivity(intentTutorial);
			}
		});

		Button btnEditJobs = (Button) findViewById(R.id.btn_settings_jobs);
		btnEditJobs.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(ActivitySettings.this, ActivityJobs.class);
				startActivity(intent);
			}
		});

		Button btnEditShiftTemplates = (Button) findViewById(R.id.btn_settings_shift_templates);
		btnEditShiftTemplates.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent intent = new Intent(ActivitySettings.this, ActivityShiftTemplates.class);
				startActivity(intent);
			}
		});

		mtvTallies = (TextView) findViewById(R.id.tv_settings_tallies);

		mswtTally = (Switch) findViewById(R.id.swt_settings_tallies);
		boolean bTallyOn = mPreferences.getBoolean(Konstant.PREF_KEY_TALLY_ON, Konstant.PREF_DEF_TALLY_ON);
		mswtTally.setChecked(bTallyOn);

		mBtnAddTally = (Button) findViewById(R.id.btn_settings_add_tally);
		if (bTallyOn)
			mBtnAddTally.setVisibility(View.VISIBLE);
		else
			mBtnAddTally.setVisibility(View.GONE);

		malnTallyIDs = DBHelper.getInstance(ActivitySettings.this).getColIDsFromTabTallies();
		ArrayList<String> alsTallyFromDates = DBHelper.getInstance(ActivitySettings.this).getColFromDateFromTabTallies();
		ArrayList<Integer> alnTallyCount = DBHelper.getInstance(ActivitySettings.this).getColEveryCountsFromTabTallies();
		ArrayList<Integer> alnTallyEvery = DBHelper.getInstance(ActivitySettings.this).getColTimeUnitsFromTabTallies();
		ArrayList<Integer> alnTallyOn = DBHelper.getInstance(ActivitySettings.this).getColOnDaysFromTabTallies();

		mTalliesAdapter = new AdapterTallies(mPreferences, ActivitySettings.this, R.layout.list_item_tallies, malnTallyIDs, alsTallyFromDates, alnTallyCount, alnTallyEvery, alnTallyOn);

		mrlTallies = (RelativeLayout) findViewById(R.id.rl_settings_tallies);

		mlvTallies = (ListView) findViewById(R.id.lv_settings_tallies);
		mlvTallies.setAdapter(mTalliesAdapter);

		mtvNoTallyFound = (TextView) findViewById(R.id.tv_settings_no_tally);

		if (bTallyOn)
		{
			if (malnTallyIDs.isEmpty())
			{
				mlvTallies.setVisibility(View.GONE);
				mtvNoTallyFound.setVisibility(View.VISIBLE);
			}
			else
			{
				mtvNoTallyFound.setVisibility(View.GONE);
				mlvTallies.setVisibility(View.VISIBLE);
			}
		}
		else
		{
			mrlTallies.setVisibility(View.GONE);
		}

		mswtTally.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean bTallyOn)
			{
				boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);
				if (bIsPremium)
				{
					mPrefEditor.putBoolean(Konstant.PREF_KEY_TALLY_ON, bTallyOn);
					mPrefEditor.commit();

					if (bTallyOn)
					{
						mrlTallies.setVisibility(View.VISIBLE);
						mBtnAddTally.setVisibility(View.VISIBLE);

						malnTallyIDs = DBHelper.getInstance(ActivitySettings.this).getColIDsFromTabTallies();
						if (malnTallyIDs.isEmpty())
						{
							mlvTallies.setVisibility(View.GONE);
							mtvNoTallyFound.setVisibility(View.VISIBLE);
						}
						else
						{
							mtvNoTallyFound.setVisibility(View.GONE);
							mlvTallies.setVisibility(View.VISIBLE);
						}
					}
					else
					{
						mrlTallies.setVisibility(View.GONE);
						mBtnAddTally.setVisibility(View.GONE);
					}

					Utils.mbIsTallyOn = mPreferences.getBoolean(Konstant.PREF_KEY_TALLY_ON, Konstant.PREF_DEF_TALLY_ON);
				}
			}
		});

		mBtnAddTally.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);
				if (bIsPremium)
				{
					DialogTally tallyDialog = new DialogTally(ActivitySettings.this);
					tallyDialog.setOnDismissListener(new OnDismissListener()
					{
						@Override
						public void onDismiss(DialogInterface dialog)
						{
							refreshTallies();
						}
					});
					tallyDialog.displayDialog(false);
				}
			}
		});

		/*
		 * mrlPasscode = (RelativeLayout)
		 * findViewById(R.id.rl_settings_passcode);
		 * 
		 * mswtPasscode = (Switch) findViewById(R.id.swt_settings_passcode);
		 * mtvSetPasscode = (TextView) findViewById(R.id.tv_set_passcode);
		 * mtvChangePasscode = (TextView) findViewById(R.id.tv_change_passcode);
		 * 
		 * boolean bPasscodeOn =
		 * mPreferences.getBoolean(Konstant.PREF_KEY_PASSCODE_ON,
		 * Konstant.PREF_DEF_PASSCODE_ON); setPasscodeOnUI(bPasscodeOn);
		 * 
		 * mswtPasscode.setChecked(bPasscodeOn);
		 * mswtPasscode.setOnCheckedChangeListener(new OnCheckedChangeListener()
		 * {
		 * 
		 * @Override public void onCheckedChanged(CompoundButton buttonView,
		 * boolean bPasscodeOn) {
		 * mPrefEditor.putBoolean(Konstant.PREF_KEY_PASSCODE_ON, bPasscodeOn);
		 * mPrefEditor.commit(); setPasscodeOnUI(bPasscodeOn); } });
		 * 
		 * mtvSetPasscode.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { DialogPasscode
		 * setPasscodeDialog = new DialogPasscode(ActivitySettings.this);
		 * setPasscodeDialog.setOnDismissListener(new OnDismissListener() {
		 * 
		 * @Override public void onDismiss(DialogInterface dialog) {
		 * setPasscodeOnUI(mswtPasscode.isChecked()); } });
		 * setPasscodeDialog.displayDialog(true); } });
		 * 
		 * mtvChangePasscode.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { DialogPasscode
		 * changePasscodeDialog = new DialogPasscode(ActivitySettings.this);
		 * changePasscodeDialog.setOnDismissListener(new OnDismissListener() {
		 * 
		 * @Override public void onDismiss(DialogInterface dialog) {
		 * setPasscodeOnUI(mswtPasscode.isChecked()); } });
		 * changePasscodeDialog.displayDialog(false); } });
		 */

		checkIsAppPremium();
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		checkIsAppPremium();
	}

	private void checkIsAppPremium()
	{
		boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);
		if (bIsPremium)
		{
			mtvTallies.setTextColor(getResources().getColor(R.color.fg_normal));
			mtvTallies.setOnClickListener(null);

			mswtTally.setVisibility(View.VISIBLE);
			mrlTallies.setVisibility(View.VISIBLE);
			mBtnAddTally.setVisibility(View.VISIBLE);

			mibUpgrade.setVisibility(View.GONE);
			mtvUpgradeOnTally.setVisibility(View.GONE);
		}
		else
		{
			mtvTallies.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivitySettings.this, ActivitySettings.this);
					dialogUpgrade.displayDialog();
				}
			});

			mswtTally.setVisibility(View.GONE);
			mrlTallies.setVisibility(View.GONE);
			mBtnAddTally.setVisibility(View.GONE);

			mibUpgrade.setVisibility(View.VISIBLE);
			mtvUpgradeOnTally.setVisibility(View.VISIBLE);
		}
	}

	/*
	 * private void setPasscodeOnUI(boolean bPasscodeOn) { if (bPasscodeOn) {
	 * mrlPasscode.setVisibility(View.VISIBLE); String strPasscode =
	 * mPreferences.getString(Konstant.PREF_KEY_PASSCODE,
	 * Konstant.PREF_DEF_PASSCODE); if
	 * (strPasscode.equals(Konstant.PREF_DEF_PASSCODE)) {
	 * mtvSetPasscode.setVisibility(View.VISIBLE);
	 * mtvChangePasscode.setVisibility(View.GONE); } else {
	 * mtvSetPasscode.setVisibility(View.GONE);
	 * mtvChangePasscode.setVisibility(View.VISIBLE); } } else {
	 * mrlPasscode.setVisibility(View.GONE);
	 * mtvSetPasscode.setVisibility(View.GONE);
	 * mtvChangePasscode.setVisibility(View.GONE); } }
	 */

	public static void refreshTallies()
	{
		malnTallyIDs = DBHelper.getInstance(mContext).getColIDsFromTabTallies();
		ArrayList<String> alsTallyFromDates = DBHelper.getInstance(mContext).getColFromDateFromTabTallies();
		ArrayList<Integer> alnTallyCount = DBHelper.getInstance(mContext).getColEveryCountsFromTabTallies();
		ArrayList<Integer> alnTallyEvery = DBHelper.getInstance(mContext).getColTimeUnitsFromTabTallies();
		ArrayList<Integer> alnTallyOn = DBHelper.getInstance(mContext).getColOnDaysFromTabTallies();

		mTalliesAdapter.changeDataset(malnTallyIDs, alsTallyFromDates, alnTallyCount, alnTallyEvery, alnTallyOn);
	}

	public static void notifyTalliesChanged()
	{
		if (DBHelper.getInstance(mContext) != null)
		{
			ArrayList<Integer> alnTallyIDs = DBHelper.getInstance(mContext).getColIDsFromTabTallies();
			if (alnTallyIDs.isEmpty())
			{
				mlvTallies.setVisibility(View.GONE);
				mtvNoTallyFound.setVisibility(View.VISIBLE);
			}
			else
			{
				mtvNoTallyFound.setVisibility(View.GONE);
				mlvTallies.setVisibility(View.VISIBLE);
			}
		}
	}
}