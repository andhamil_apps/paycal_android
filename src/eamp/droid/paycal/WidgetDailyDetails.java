/*
 * WidgetDailyDetails
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Locale;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.RemoteViews;
import eamp.droid.paycal.model.DailyPiggyDetails;
import eamp.droid.paycal.model.DailyShiftDetails;

public class WidgetDailyDetails extends AppWidgetProvider
{
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
	{
		SharedPreferences preferences = context.getSharedPreferences(Konstant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);

		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_daily_details);
		ComponentName watchWidget = new ComponentName(context, WidgetDailyDetails.class);

		Calendar calendar = Calendar.getInstance();
		int nYear = calendar.get(Calendar.YEAR);
		int nMOY = calendar.get(Calendar.MONTH);
		int nDOM = calendar.get(Calendar.DAY_OF_MONTH);

		String strMonth = DateFormatSymbols.getInstance().getMonths()[nMOY].toUpperCase(Locale.CANADA);

		remoteViews.setTextViewText(R.id.tv_widget_date, Integer.toString(nDOM));
		remoteViews.setTextViewText(R.id.tv_widget_month, strMonth.substring(0, 3));

		DecimalFormat df = new DecimalFormat("0.00");

		Utils.msaPiggyDates = Utils.getPiggyDatesMappedByJobID(context, preferences);
		DailyPiggyDetails dailyPiggyDetailsToday = DBHelper.getInstance(context).getDailyPiggyDetails(nYear, (nMOY + 1), nDOM);
		if (dailyPiggyDetailsToday.getJobIDs().size() > 0)
		{
			String strJobName = dailyPiggyDetailsToday.getJobNames().get(0);

			String strPaidMinutes = Utils.constructTimeInHrsAndMins(dailyPiggyDetailsToday.getRegularMinutes().get(0) + dailyPiggyDetailsToday.getOvertimeMinutes().get(0)
					+ dailyPiggyDetailsToday.getSpecialMinutes().get(0));

			double dPaidAmount = dailyPiggyDetailsToday.getRegularAmounts().get(0) + dailyPiggyDetailsToday.getOvertimeAmounts().get(0) + dailyPiggyDetailsToday.getSpecialAmounts().get(0);
			if (preferences.getBoolean(Konstant.PREF_KEY_TIPS_ON, Konstant.PREF_DEF_TIPS_ON))
				dPaidAmount += dailyPiggyDetailsToday.getTips().get(0);
			if (preferences.getBoolean(Konstant.PREF_KEY_DEDUCTIONS_ON, Konstant.PREF_DEF_DEDUCTIONS_ON))
				dPaidAmount -= dailyPiggyDetailsToday.getDeductions().get(0);
			if (dPaidAmount < 0)
				dPaidAmount = 0;
			String strAmount = "$" + df.format(dPaidAmount);

			remoteViews.setViewVisibility(R.id.iv_widget_piggy, View.VISIBLE);
			remoteViews.setViewVisibility(R.id.tv_widget_piggy_job_name, View.VISIBLE);
			remoteViews.setViewVisibility(R.id.tv_widget_piggy_paid_hours, View.VISIBLE);
			remoteViews.setViewVisibility(R.id.tv_widget_piggy_amount, View.VISIBLE);

			remoteViews.setTextViewText(R.id.tv_widget_piggy_job_name, strJobName);
			remoteViews.setTextViewText(R.id.tv_widget_piggy_paid_hours, strPaidMinutes);
			remoteViews.setTextViewText(R.id.tv_widget_piggy_amount, strAmount);

		}
		else
		{
			remoteViews.setViewVisibility(R.id.iv_widget_piggy, View.GONE);
			remoteViews.setViewVisibility(R.id.tv_widget_piggy_job_name, View.GONE);
			remoteViews.setViewVisibility(R.id.tv_widget_piggy_paid_hours, View.GONE);
			remoteViews.setViewVisibility(R.id.tv_widget_piggy_amount, View.GONE);
		}

		DailyShiftDetails dailyShiftDetailsToday = DBHelper.getInstance(context).getDailyShiftDetails(nYear, (nMOY + 1), nDOM);
		boolean bShiftToday = false;
		for (int i = 0; i < dailyShiftDetailsToday.getJobIDs().size(); i++)
		{
			Calendar calToShift = Calendar.getInstance();
			calToShift.set(Calendar.HOUR_OF_DAY, dailyShiftDetailsToday.getToHours().get(i));
			calToShift.set(Calendar.MINUTE, dailyShiftDetailsToday.getToMinutes().get(i));

			Calendar calFromShift = Calendar.getInstance();
			calFromShift.set(Calendar.HOUR_OF_DAY, dailyShiftDetailsToday.getFromHours().get(i));
			calFromShift.set(Calendar.MINUTE, dailyShiftDetailsToday.getFromMinutes().get(i));

			if (calendar.before(calToShift) || calendar.before(calFromShift))
			{
				String strFromTime = Utils.constructTime(dailyShiftDetailsToday.getFromHours().get(i), dailyShiftDetailsToday.getFromMinutes().get(i), preferences);
				String strToTime = Utils.constructTime(dailyShiftDetailsToday.getToHours().get(i), dailyShiftDetailsToday.getToMinutes().get(i), preferences);

				String strJobName = dailyShiftDetailsToday.getJobNames().get(i);

				String strPaidMinutes = Utils.constructTimeInHrsAndMins(dailyShiftDetailsToday.getRegularMinutes().get(i) + dailyShiftDetailsToday.getOvertimeMinutes().get(i)
						+ dailyShiftDetailsToday.getSpecialMinutes().get(i));

				double dPaidAmount = dailyShiftDetailsToday.getRegularAmounts().get(i) + dailyShiftDetailsToday.getOvertimeAmounts().get(i) + dailyShiftDetailsToday.getSpecialAmounts().get(i);
				if (preferences.getBoolean(Konstant.PREF_KEY_TIPS_ON, Konstant.PREF_DEF_TIPS_ON))
					dPaidAmount += dailyShiftDetailsToday.getTips().get(i);
				if (dPaidAmount < 0)
					dPaidAmount = 0;
				String strAmount = "$" + df.format(dPaidAmount);

				remoteViews.setTextViewText(R.id.tv_widget_today_job_name, strJobName);

				remoteViews.setViewVisibility(R.id.tv_widget_today_shift_times_from, View.VISIBLE);
				remoteViews.setViewVisibility(R.id.tv_widget_today_shift_times_to, View.VISIBLE);
				remoteViews.setViewVisibility(R.id.tv_widget_today_paid_hours, View.VISIBLE);
				remoteViews.setViewVisibility(R.id.tv_widget_today_amount, View.VISIBLE);

				remoteViews.setTextViewText(R.id.tv_widget_today_shift_times_from, strFromTime);
				remoteViews.setTextViewText(R.id.tv_widget_today_shift_times_to, strToTime);
				remoteViews.setTextViewText(R.id.tv_widget_today_paid_hours, strPaidMinutes);
				remoteViews.setTextViewText(R.id.tv_widget_today_amount, strAmount);

				bShiftToday = true;
				break;
			}
		}

		if (!bShiftToday)
		{
			remoteViews.setTextViewText(R.id.tv_widget_today_job_name, context.getString(R.string.no_shift_today));

			remoteViews.setViewVisibility(R.id.tv_widget_today_shift_times_from, View.GONE);
			remoteViews.setViewVisibility(R.id.tv_widget_today_shift_times_to, View.GONE);
			remoteViews.setViewVisibility(R.id.tv_widget_today_paid_hours, View.GONE);
			remoteViews.setViewVisibility(R.id.tv_widget_today_amount, View.GONE);
		}

		calendar.add(Calendar.DAY_OF_YEAR, 1);
		nYear = calendar.get(Calendar.YEAR);
		nMOY = calendar.get(Calendar.MONTH);
		nDOM = calendar.get(Calendar.DAY_OF_MONTH);

		DailyShiftDetails dailyShiftDetailsTomo = DBHelper.getInstance(context).getDailyShiftDetails(nYear, (nMOY + 1), nDOM);
		if (dailyShiftDetailsTomo.getJobIDs().size() > 0)
		{
			Calendar calShift = Calendar.getInstance();
			calShift.set(Calendar.HOUR_OF_DAY, dailyShiftDetailsTomo.getFromHours().get(0));
			calShift.set(Calendar.MINUTE, dailyShiftDetailsTomo.getFromMinutes().get(0));

			String strFromTime = Utils.constructTime(dailyShiftDetailsTomo.getFromHours().get(0), dailyShiftDetailsTomo.getFromMinutes().get(0), preferences);
			String strToTime = Utils.constructTime(dailyShiftDetailsTomo.getToHours().get(0), dailyShiftDetailsTomo.getToMinutes().get(0), preferences);

			String strJobName = dailyShiftDetailsTomo.getJobNames().get(0);

			String strPaidMinutes = Utils.constructTimeInHrsAndMins(dailyShiftDetailsTomo.getRegularMinutes().get(0) + dailyShiftDetailsTomo.getOvertimeMinutes().get(0)
					+ dailyShiftDetailsTomo.getSpecialMinutes().get(0));

			double dPaidAmount = dailyShiftDetailsTomo.getRegularAmounts().get(0) + dailyShiftDetailsTomo.getOvertimeAmounts().get(0) + dailyShiftDetailsTomo.getSpecialAmounts().get(0);
			if (preferences.getBoolean(Konstant.PREF_KEY_TIPS_ON, Konstant.PREF_DEF_TIPS_ON))
				dPaidAmount += dailyShiftDetailsTomo.getTips().get(0);
			if (dPaidAmount < 0)
				dPaidAmount = 0;
			String strAmount = "$" + df.format(dPaidAmount);

			remoteViews.setTextViewText(R.id.tv_widget_tomo_job_name, strJobName);

			remoteViews.setViewVisibility(R.id.tv_widget_tomo_shift_times_from, View.VISIBLE);
			remoteViews.setViewVisibility(R.id.tv_widget_tomo_shift_times_to, View.VISIBLE);
			remoteViews.setViewVisibility(R.id.tv_widget_tomo_paid_hours, View.VISIBLE);
			remoteViews.setViewVisibility(R.id.tv_widget_tomo_amount, View.VISIBLE);

			remoteViews.setTextViewText(R.id.tv_widget_tomo_shift_times_from, strFromTime);
			remoteViews.setTextViewText(R.id.tv_widget_tomo_shift_times_to, strToTime);
			remoteViews.setTextViewText(R.id.tv_widget_tomo_paid_hours, strPaidMinutes);
			remoteViews.setTextViewText(R.id.tv_widget_tomo_amount, strAmount);
		}
		else
		{
			remoteViews.setTextViewText(R.id.tv_widget_tomo_job_name, context.getString(R.string.no_shift_tomo));

			remoteViews.setViewVisibility(R.id.tv_widget_tomo_shift_times_from, View.GONE);
			remoteViews.setViewVisibility(R.id.tv_widget_tomo_shift_times_to, View.GONE);
			remoteViews.setViewVisibility(R.id.tv_widget_tomo_paid_hours, View.GONE);
			remoteViews.setViewVisibility(R.id.tv_widget_tomo_amount, View.GONE);
		}

		appWidgetManager.updateAppWidget(watchWidget, remoteViews);
	}
}