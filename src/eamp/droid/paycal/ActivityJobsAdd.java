/*
 * ActivityJobsAdd
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import eamp.droid.paycal.model.CurrencyFormat;
import eamp.droid.paycal.model.Job;

public class ActivityJobsAdd extends Activity
{
	private boolean mbEditMode;

	private int mnJobID;
	private int mnSelectedColor;

	private int mnJobBeganYear;
	private int mnJobBeganMOY;
	private int mnJobBeganDOM;

	private int mnPayDayYear;
	private int mnPayDayMOY;
	private int mnPayDayDOM;

	private int mnPayDayYearOriginal;
	private int mnPayDayMOYOriginal;
	private int mnPayDayDOMOriginal;
	private int mnPayLengthOriginal;

	private ImageButton mibUpgrade;
	private ImageButton mibUpgradeDummy;

	private EditText metJobName;
	private EditText metJobPay;
	private Spinner mspnPayUnit;

	private ImageView mivPurple;
	private ImageView mivCyan;
	private ImageView mivGreen;
	private ImageView mivMustard;
	private ImageView mivOrange;
	private ImageView mivRed;

	private EditText metJobBegan;
	private EditText metPayday;
	private Spinner mspnPayLength;

	private TextView mtvDeduction;
	private EditText metPayDeduction;
	private TextView mtvUpgradeOnDeduction;

	private TextView mtvOverTime;
	private Switch mswtOverTime;
	private TextView mtvUpgradeOnOvertime;
	private TextView mtvOTRate;
	private EditText metOTRate;
	private TextView mtvOTStarts;
	private EditText metOTHrs;
	private EditText metOTMins;
	private Spinner mspnOvertimeUnit;

	private DatePickerDialog.OnDateSetListener mdslJobBegan;
	private DatePickerDialog mdpdJobBegan;

	private DatePickerDialog.OnDateSetListener mdslPayday;
	private DatePickerDialog mdpdPayday;

	private SharedPreferences mPreferences;
	private SharedPreferences.Editor mPrefEditor;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jobs_add);
		
		ActionBar currentActionBar = getActionBar();
		currentActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		currentActionBar.setDisplayShowTitleEnabled(false);
		currentActionBar.setCustomView(R.layout.action_bar_items_2);

		mPreferences = getSharedPreferences(Konstant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		mPrefEditor = mPreferences.edit();

		mtvUpgradeOnDeduction = (TextView) findViewById(R.id.tv_addjob_deduction_upgrade);
		mtvUpgradeOnDeduction.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityJobsAdd.this, ActivityJobsAdd.this);
				dialogUpgrade.displayDialog();
			}
		});

		mtvUpgradeOnOvertime = (TextView) findViewById(R.id.tv_addjob_ot_upgrade);
		mtvUpgradeOnOvertime.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityJobsAdd.this, ActivityJobsAdd.this);
				dialogUpgrade.displayDialog();
			}
		});

		mibUpgrade = (ImageButton) findViewById(R.id.ib_addjob_upgrade);
		mibUpgrade.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityJobsAdd.this, ActivityJobsAdd.this);
				dialogUpgrade.displayDialog();
			}
		});
		mibUpgradeDummy = (ImageButton) findViewById(R.id.ib_addjob_upgrade_dummy);
		
		mnSelectedColor = Konstant.COLOR_GREEN;
		mbEditMode = false;

		TextView tvActivityTitle = (TextView) findViewById(R.id.tv_ab2_title);
		tvActivityTitle.setText(getString(R.string.add_job));

		ImageButton ibSave = (ImageButton) findViewById(R.id.ib_ab2_save);
		ibSave.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				saveJob();
			}
		});

		metJobName = (EditText) findViewById(R.id.et_addjob_job_name);
		metJobName.setText(Konstant.DEFAULT_JOB_NAME);
		metJobName.setSelection(metJobName.getText().toString().length());

		mivPurple = (ImageView) findViewById(R.id.iv_addjob_color_purple);
		mivCyan = (ImageView) findViewById(R.id.iv_addjob_color_cyan);
		mivGreen = (ImageView) findViewById(R.id.iv_addjob_color_green);
		mivGreen.setImageResource(R.drawable.color_selected);
		mivMustard = (ImageView) findViewById(R.id.iv_addjob_color_mustard);
		mivOrange = (ImageView) findViewById(R.id.iv_addjob_color_orange);
		mivRed = (ImageView) findViewById(R.id.iv_addjob_color_red);

		metJobPay = (EditText) findViewById(R.id.et_addjob_pay);
		metJobPay.setText(Utils.convertAmountIntoString(Konstant.DEFAULT_JOB_PAY));
		
		CurrencyFormat watcher = new CurrencyFormat();
		metJobPay.addTextChangedListener(watcher);
		
		metJobPay.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if (hasFocus)
				{
					metJobPay.setText(Double.toString(Utils.convertAmountIntoDouble(metJobPay.getText().toString())));
				}
				else
				{
					metJobPay.setText(Utils.convertAmountIntoString(metJobPay.getText().toString()));

					double dJobPay = Utils.convertAmountIntoDouble(metJobPay.getText().toString());
					double dOTRate = Utils.convertRateIntoDouble(metOTRate.getText().toString());
					metOTRate.setText(Utils.convertRateIntoString(dJobPay, dOTRate));
				}
			}
		});

		mspnPayUnit = (Spinner) findViewById(R.id.spn_add_job_pay_unit);
		mspnPayUnit.setSelection(Konstant.DEFAULT_JOB_PAY_UNIT);

		Calendar calendar = Calendar.getInstance();
		mnJobBeganYear = calendar.get(Calendar.YEAR);
		mnJobBeganMOY = calendar.get(Calendar.MONTH);
		mnJobBeganDOM = calendar.get(Calendar.DAY_OF_MONTH);

		metJobBegan = (EditText) findViewById(R.id.et_addjob_began);
		metJobBegan.setText(Utils.constructDate(mnJobBeganYear, mnJobBeganMOY, mnJobBeganDOM));

		mspnPayLength = (Spinner) findViewById(R.id.spn_addjob_pay_length);
		mspnPayLength.setSelection(Konstant.DEFAULT_JOB_PAY_LENGTH);

		calendar.add(Calendar.DAY_OF_YEAR, 7);
		mnPayDayYear = calendar.get(Calendar.YEAR);
		mnPayDayMOY = calendar.get(Calendar.MONTH);
		mnPayDayDOM = calendar.get(Calendar.DAY_OF_MONTH);

		metPayday = (EditText) findViewById(R.id.et_addjob_payday);
		metPayday.setText(Utils.constructDate(mnPayDayYear, mnPayDayMOY, mnPayDayDOM));

		mtvDeduction = (TextView) findViewById(R.id.tv_addjob_deduction);

		metPayDeduction = (EditText) findViewById(R.id.et_addjob_deduction);
		metPayDeduction.setText(Utils.convertPayDeductionIntoString(Konstant.DEFAULT_JOB_PAY_DEDUCTION));
		metPayDeduction.setTag(metPayDeduction.getKeyListener());
		metPayDeduction.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);
				if (bIsPremium)
				{
					if (hasFocus)
					{
						metPayDeduction.setText(Double.toString(Utils.convertPayDeductionIntoDouble(metPayDeduction.getText().toString())));
					}
					else
					{
						metPayDeduction.setText(Utils.convertPayDeductionIntoString(metPayDeduction.getText().toString()));
					}
				}
			}
		});
		metPayDeduction.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);
				if (!bIsPremium)
				{
					metPayDeduction.setText(Double.toString(Konstant.DEFAULT_JOB_PAY_DEDUCTION));
					DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityJobsAdd.this, ActivityJobsAdd.this);
					dialogUpgrade.displayDialog();
				}
			}
		});

		mtvOverTime = (TextView) findViewById(R.id.tv_addjob_ot);

		mswtOverTime = (Switch) findViewById(R.id.swt_addjob_ot);
		mswtOverTime.setChecked(Konstant.DEFAULT_JOB_OVERTIME_ON);

		mtvOTRate = (TextView) findViewById(R.id.tv_addjob_ot_rate);

		metOTRate = (EditText) findViewById(R.id.et_addjob_ot_rate);
		metOTRate.setText(Utils.convertRateIntoString(Konstant.DEFAULT_JOB_PAY, Konstant.DEFAULT_JOB_OVERTIME_RATE));
		metOTRate.setEnabled(mswtOverTime.isChecked());
		metOTRate.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if (hasFocus)
				{
					metOTRate.setText(Double.toString(Utils.convertRateIntoDouble(metOTRate.getText().toString())));
				}
				else
				{
					metOTRate.setText(Utils.convertRateIntoString(metJobPay.getText().toString(), metOTRate.getText().toString()));
				}
			}
		});

		mtvOTStarts = (TextView) findViewById(R.id.tv_addjob_ot_starts);

		metOTHrs = (EditText) findViewById(R.id.et_addjob_ot_hrs);
		metOTHrs.setText(Utils.convertHrsIntoString(Konstant.DEFAULT_JOB_OT_HRS));
		metOTHrs.setEnabled(mswtOverTime.isChecked());
		metOTHrs.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if (hasFocus)
				{
					metOTHrs.setText(Integer.toString(Utils.convertHrsIntoInt(metOTHrs.getText().toString())));
				}
				else
				{
					metOTHrs.setText(Utils.convertHrsIntoString(metOTHrs.getText().toString()));
				}
			}
		});

		metOTMins = (EditText) findViewById(R.id.et_addjob_ot_mins);
		metOTMins.setText(Utils.convertMinsIntoString(Konstant.DEFAULT_JOB_OT_MINS));
		metOTMins.setEnabled(mswtOverTime.isChecked());
		metOTMins.setOnFocusChangeListener(new OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if (hasFocus)
				{
					metOTMins.setText(Integer.toString(Utils.convertMinsIntoInt(metOTMins.getText().toString())));
				}
				else
				{
					metOTMins.setText(Utils.convertMinsIntoString(metOTMins.getText().toString()));
				}
			}
		});

		mspnOvertimeUnit = (Spinner) findViewById(R.id.spn_addjob_ot_unit);
		mspnOvertimeUnit.setSelection(Konstant.DEFAULT_JOB_OVERTIME_UNIT);
		mspnOvertimeUnit.setEnabled(mswtOverTime.isChecked());

		mivPurple.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setJobColor(Konstant.COLOR_PURPLE);
			}
		});

		mivCyan.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setJobColor(Konstant.COLOR_CYAN);
			}
		});

		mivGreen.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setJobColor(Konstant.COLOR_GREEN);
			}
		});

		mivMustard.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setJobColor(Konstant.COLOR_MUSTARD);
			}
		});

		mivOrange.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setJobColor(Konstant.COLOR_ORANGE);
			}
		});

		mivRed.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setJobColor(Konstant.COLOR_RED);
			}
		});

		mdslJobBegan = new DatePickerDialog.OnDateSetListener()
		{
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
			{
				Calendar calendarJobBegan = Calendar.getInstance();
				calendarJobBegan.set(year, monthOfYear, dayOfMonth);

				Calendar calendarPayDay = Calendar.getInstance();
				calendarPayDay.set(mnPayDayYear, mnPayDayMOY, mnPayDayDOM);

				if (calendarPayDay.before(calendarJobBegan))
				{
					Toast.makeText(ActivityJobsAdd.this, getString(R.string.msg_job_add_date_invalid), Toast.LENGTH_SHORT).show();
				}
				else
				{
					mnJobBeganYear = year;
					mnJobBeganMOY = monthOfYear;
					mnJobBeganDOM = dayOfMonth;
					metJobBegan.setText(Utils.constructDate(mnJobBeganYear, mnJobBeganMOY, mnJobBeganDOM));
				}
			}
		};

		metJobBegan.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				launchBeganDatePicker();
			}
		});
		metJobBegan.setText(Utils.constructDate(mnJobBeganYear, mnJobBeganMOY, mnJobBeganDOM));

		mdslPayday = new DatePickerDialog.OnDateSetListener()
		{
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
			{
				Calendar calendarJobBegan = Calendar.getInstance();
				calendarJobBegan.set(mnJobBeganYear, mnJobBeganMOY, mnJobBeganDOM);

				Calendar calendarPayDay = Calendar.getInstance();
				calendarPayDay.set(year, monthOfYear, dayOfMonth);

				if (calendarPayDay.before(calendarJobBegan))
				{
					Toast.makeText(ActivityJobsAdd.this, getString(R.string.msg_job_add_date_invalid), Toast.LENGTH_SHORT).show();
				}
				else
				{
					mnPayDayYear = year;
					mnPayDayMOY = monthOfYear;
					mnPayDayDOM = dayOfMonth;
					metPayday.setText(Utils.constructDate(mnPayDayYear, mnPayDayMOY, mnPayDayDOM));
				}
			}
		};

		metPayday.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				launchPaydayDatePicker();
			}
		});
		metPayday.setText(Utils.constructDate(mnPayDayYear, mnPayDayMOY, mnPayDayDOM));

		mswtOverTime.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);
				if (bIsPremium)
				{
					metOTRate.setEnabled(isChecked);
					metOTHrs.setEnabled(isChecked);
					metOTMins.setEnabled(isChecked);
					mspnOvertimeUnit.setEnabled(isChecked);
				}
			}
		});

		Button btnDeleteJob = (Button) findViewById(R.id.btn_addjob_delete_job);
		btnDeleteJob.setVisibility(View.GONE);
		btnDeleteJob.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(ActivityJobsAdd.this);
				builder.setTitle(R.string.delete_job);
				builder.setMessage(R.string.msg_prompt_job_delete);
				builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int id)
					{
						DBHelper.getInstance(ActivityJobsAdd.this).deleteJob(mnJobID);

						mPrefEditor.remove(Konstant.PREF_KEY_PIGGY_DATES_FOR + mnJobID);
						mPrefEditor.commit();

						ActivityJobs.notifyJobsChanged();
						finish();
					}
				});
				builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int id)
					{
					}
				});
				builder.create();
				builder.show();
			}
		});

		Intent intent = getIntent();
		if (intent != null)
		{
			if (intent.hasExtra(Konstant.KEY_SELECTED_JOB_ID))
			{
				tvActivityTitle.setText(getString(R.string.edit_job));

				int nJobId = intent.getIntExtra(Konstant.KEY_SELECTED_JOB_ID, -1);
				Job currentJob = DBHelper.getInstance(ActivityJobsAdd.this).getJob(nJobId);
				setJobDetails(currentJob);
				getActionBar().setTitle(getString(R.string.edit_job));
				mbEditMode = true;
				btnDeleteJob.setVisibility(View.VISIBLE);
			}
		}

		checkIsAppPremium();
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		checkIsAppPremium();
	}

	private void checkIsAppPremium()
	{
		boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);
		if (bIsPremium)
		{
			mtvDeduction.setTextColor(getResources().getColor(android.R.color.white));
			mtvDeduction.setOnClickListener(null);
			mtvOverTime.setTextColor(getResources().getColor(android.R.color.white));
			mtvOverTime.setOnClickListener(null);
			mtvOTRate.setTextColor(getResources().getColor(android.R.color.white));
			mtvOTRate.setOnClickListener(null);
			mtvOTStarts.setTextColor(getResources().getColor(android.R.color.white));
			mtvOTStarts.setOnClickListener(null);

			mswtOverTime.setEnabled(true);

			metPayDeduction.setVisibility(View.VISIBLE);

			metOTRate.setVisibility(View.VISIBLE);
			metOTHrs.setVisibility(View.VISIBLE);
			metOTMins.setVisibility(View.VISIBLE);
			mspnOvertimeUnit.setVisibility(View.VISIBLE);
			
			mibUpgrade.setVisibility(View.GONE);
			mibUpgradeDummy.setVisibility(View.GONE);
			mtvUpgradeOnDeduction.setVisibility(View.GONE);
			mtvUpgradeOnOvertime.setVisibility(View.GONE);
		}
		else
		{
			mtvDeduction.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityJobsAdd.this, ActivityJobsAdd.this);
					dialogUpgrade.displayDialog();
				}
			});
			mtvOverTime.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityJobsAdd.this, ActivityJobsAdd.this);
					dialogUpgrade.displayDialog();
				}
			});
			mtvOTRate.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityJobsAdd.this, ActivityJobsAdd.this);
					dialogUpgrade.displayDialog();
				}
			});
			mtvOTStarts.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityJobsAdd.this, ActivityJobsAdd.this);
					dialogUpgrade.displayDialog();
				}
			});
			
			mswtOverTime.setEnabled(false);

			metPayDeduction.setVisibility(View.GONE);

			metOTRate.setVisibility(View.GONE);
			metOTHrs.setVisibility(View.GONE);
			metOTMins.setVisibility(View.GONE);
			mspnOvertimeUnit.setVisibility(View.GONE);
			
			mibUpgrade.setVisibility(View.VISIBLE);
			mibUpgradeDummy.setVisibility(View.VISIBLE);
			mtvUpgradeOnDeduction.setVisibility(View.VISIBLE);
			mtvUpgradeOnOvertime.setVisibility(View.VISIBLE);
		}
	}

	private void launchBeganDatePicker()
	{
		if ((mdpdJobBegan != null) && mdpdJobBegan.isShowing())
			mdpdJobBegan.cancel();

		mdpdJobBegan = new DatePickerDialog(ActivityJobsAdd.this, mdslJobBegan, mnJobBeganYear, mnJobBeganMOY, mnJobBeganDOM);
		mdpdJobBegan.setTitle(getString(R.string.began));
		mdpdJobBegan.show();
	}

	private void launchPaydayDatePicker()
	{
		if ((mdpdPayday != null) && mdpdPayday.isShowing())
			mdpdPayday.cancel();

		mdpdPayday = new DatePickerDialog(ActivityJobsAdd.this, mdslPayday, mnPayDayYear, mnPayDayMOY, mnPayDayDOM);
		mdpdPayday.setTitle(getString(R.string.pay_day));
		mdpdPayday.show();
	}

	private boolean saveJob()
	{
		if (metJobName.getText().toString().isEmpty() || metJobPay.getText().toString().isEmpty())
		{
			Toast.makeText(ActivityJobsAdd.this, getString(R.string.msg_job_add_empty), Toast.LENGTH_SHORT).show();
			return false;
		}

		try
		{
			String strJobName = metJobName.getText().toString();
			int nColor = mnSelectedColor;

			double dPay = Utils.convertAmountIntoDouble(metJobPay.getText().toString());
			int nPayUnit = mspnPayUnit.getSelectedItemPosition();

			int nPayLength = mspnPayLength.getSelectedItemPosition();
			double dPayDeduction = Utils.convertPayDeductionIntoDouble(metPayDeduction.getText().toString());

			double dOvertimeRate = Utils.convertRateIntoDouble(metOTRate.getText().toString());

			int nOTHrs = Utils.convertHrsIntoInt(metOTHrs.getText().toString());
			int nOTMins = Utils.convertMinsIntoInt(metOTMins.getText().toString());
			int nOvertimeUnit = mspnOvertimeUnit.getSelectedItemPosition();

			Job job = new Job();
			job.setName(strJobName);
			job.setColor(nColor);
			job.setPay(dPay);
			job.setPayUnit(nPayUnit);
			job.setPayBeganYear(mnJobBeganYear);
			job.setPayBeganMOY(mnJobBeganMOY);
			job.setPayBeganDOM(mnJobBeganDOM);
			job.setPayLength(nPayLength);
			job.setPayDayYear(mnPayDayYear);
			job.setPayDayMOY(mnPayDayMOY);
			job.setPayDayDOM(mnPayDayDOM);
			job.setPayDeduction(dPayDeduction);
			job.setOvertimeRate(dOvertimeRate);
			job.setOTMinutes((nOTHrs * 60 + nOTMins));
			job.setOvertimeUnit(nOvertimeUnit);
			job.setOvertimeOn(mswtOverTime.isChecked());

			if (mbEditMode)
			{
				if (!DBHelper.getInstance(ActivityJobsAdd.this).isJobNameAvailable(mnJobID, strJobName))
				{
					Toast.makeText(ActivityJobsAdd.this, getString(R.string.msg_job_already_exists) + " " + strJobName + ".", Toast.LENGTH_LONG).show();
				}
				else
				{
					job.setID(mnJobID);
					if (DBHelper.getInstance(ActivityJobsAdd.this).updateJob(job))
					{
						if (!((mnPayDayYearOriginal == mnPayDayYear) && (mnPayDayMOYOriginal == mnPayDayMOY) && (mnPayDayDOMOriginal == mnPayDayDOM) && (mnPayLengthOriginal == nPayLength)))
						{
							Integer piggyCalciParams[] = { mnPayDayYear, mnPayDayMOY, mnPayDayDOM, nPayLength };
							PiggyDatesCalculator piggyCalci = new PiggyDatesCalculator();
							piggyCalci.execute(piggyCalciParams);
						}

						finish();
					}
					else
					{
						Toast.makeText(ActivityJobsAdd.this, getString(R.string.msg_job_edit_fail), Toast.LENGTH_LONG).show();
					}
				}
			}
			else
			{
				ArrayList<String> alsJobNames = DBHelper.getInstance(ActivityJobsAdd.this).getColNamesFromTabJobs();

				if (alsJobNames.contains(strJobName))
				{
					Toast.makeText(ActivityJobsAdd.this, getString(R.string.msg_job_already_exists) + " " + strJobName + ".", Toast.LENGTH_LONG).show();
				}
				else
				{
					if (DBHelper.getInstance(ActivityJobsAdd.this).addJob(job))
					{
						mnJobID = DBHelper.getInstance(ActivityJobsAdd.this).getMaxJobID();
						Toast.makeText(ActivityJobsAdd.this, getString(R.string.msg_job_add_pass), Toast.LENGTH_LONG).show();
						ActivityJobs.notifyJobsChanged();

						Integer piggyCalciParams[] = { mnPayDayYear, mnPayDayMOY, mnPayDayDOM, nPayLength };
						PiggyDatesCalculator piggyCalci = new PiggyDatesCalculator();
						piggyCalci.execute(piggyCalciParams);

						finish();
					}
					else
					{
						Toast.makeText(ActivityJobsAdd.this, getString(R.string.msg_job_add_fail), Toast.LENGTH_LONG).show();
					}
				}
			}

		}
		catch (NumberFormatException nfe)
		{
			Toast.makeText(ActivityJobsAdd.this, getString(R.string.msg_invalid_entry), Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

	private void setJobDetails(Job job)
	{
		mnPayDayYearOriginal = job.getPayDayYear();
		mnPayDayMOYOriginal = job.getPayDayMOY();
		mnPayDayDOMOriginal = job.getPayDayDOM();
		mnPayLengthOriginal = job.getPayLength();

		mnJobID = job.getID();

		metJobName.setText(job.getName());
		metJobName.setSelection(metJobName.getText().toString().length());

		metJobPay.setText(Utils.convertAmountIntoString(job.getPay()));
		mspnPayUnit.setSelection(job.getPayUnit());

		mnJobBeganYear = job.getPayBeganYear();
		mnJobBeganMOY = job.getPayBeganMOY();
		mnJobBeganDOM = job.getPayBeganDOM();
		metJobBegan.setText(Utils.constructDate(mnJobBeganYear, mnJobBeganMOY, mnJobBeganDOM));

		mspnPayLength.setSelection(job.getPayLength());

		mnPayDayYear = job.getPayDayYear();
		mnPayDayMOY = job.getPayDayMOY();
		mnPayDayDOM = job.getPayDayDOM();
		metPayday.setText(Utils.constructDate(mnPayDayYear, mnPayDayMOY, mnPayDayDOM));

		metPayDeduction.setText(Utils.convertPayDeductionIntoString(job.getPayDeduction()));

		mswtOverTime.setChecked(job.isOvertimeOn());

		metOTRate.setText(Utils.convertRateIntoString(job.getPay(), job.getOvertimeRate()));

		int nOTMinutes = job.getOTMinutes();
		int nOTHrs = nOTMinutes / 60;
		int OTMins = nOTMinutes % 60;
		metOTHrs.setText(Utils.convertHrsIntoString(nOTHrs));
		metOTMins.setText(Utils.convertMinsIntoString(OTMins));

		mspnOvertimeUnit.setSelection(job.getOvertimeUnit());

		setJobColor(job.getColor());
	}

	private void setJobColor(int nColor)
	{
		switch (nColor)
		{
		case Konstant.COLOR_PURPLE:
			mnSelectedColor = Konstant.COLOR_PURPLE;
			mivPurple.setImageResource(R.drawable.color_selected);
			mivCyan.setImageResource(0);
			mivGreen.setImageResource(0);
			mivMustard.setImageResource(0);
			mivOrange.setImageResource(0);
			mivRed.setImageResource(0);
			break;
		case Konstant.COLOR_CYAN:
			mnSelectedColor = Konstant.COLOR_CYAN;
			mivPurple.setImageResource(0);
			mivCyan.setImageResource(R.drawable.color_selected);
			mivGreen.setImageResource(0);
			mivMustard.setImageResource(0);
			mivOrange.setImageResource(0);
			mivRed.setImageResource(0);
			break;
		case Konstant.COLOR_GREEN:
			mnSelectedColor = Konstant.COLOR_GREEN;
			mivPurple.setImageResource(0);
			mivCyan.setImageResource(0);
			mivGreen.setImageResource(R.drawable.color_selected);
			mivMustard.setImageResource(0);
			mivOrange.setImageResource(0);
			mivRed.setImageResource(0);
			break;
		case Konstant.COLOR_MUSTARD:
			mnSelectedColor = Konstant.COLOR_MUSTARD;
			mivPurple.setImageResource(0);
			mivCyan.setImageResource(0);
			mivGreen.setImageResource(0);
			mivMustard.setImageResource(R.drawable.color_selected);
			mivOrange.setImageResource(0);
			mivRed.setImageResource(0);
			break;
		case Konstant.COLOR_ORANGE:
			mnSelectedColor = Konstant.COLOR_ORANGE;
			mivPurple.setImageResource(0);
			mivCyan.setImageResource(0);
			mivGreen.setImageResource(0);
			mivMustard.setImageResource(0);
			mivOrange.setImageResource(R.drawable.color_selected);
			mivRed.setImageResource(0);
			break;
		case Konstant.COLOR_RED:
			mnSelectedColor = Konstant.COLOR_RED;
			mivPurple.setImageResource(0);
			mivCyan.setImageResource(0);
			mivGreen.setImageResource(0);
			mivMustard.setImageResource(0);
			mivOrange.setImageResource(0);
			mivRed.setImageResource(R.drawable.color_selected);
			break;
		}
	}

	private class PiggyDatesCalculator extends AsyncTask<Integer, Void, Void>
	{
		PiggyDatesCalculator()
		{
		}

		@Override
		protected void onPreExecute()
		{
		}

		@Override
		protected Void doInBackground(Integer... params)
		{
			int nFromYear = params[0];
			int nFromMOY = params[1];
			int nFromDOM = params[2];
			int nPayLength = params[3];

			ArrayList<String> alsPiggyDates = null;

			// Delete old Piggy dates

			mPrefEditor.putString(Konstant.PREF_KEY_PIGGY_DATES_FOR + mnJobID, "");
			mPrefEditor.commit();

			switch (nPayLength)
			{
			case Konstant.JOB_PAY_LENGTH_WEEKLY:
				alsPiggyDates = Utils.calculatePiggyDatesUsingWeeks(nFromYear, nFromMOY, nFromDOM, 1);
				break;
			case Konstant.JOB_PAY_LENGTH_BIWEEKLY:
				alsPiggyDates = Utils.calculatePiggyDatesUsingWeeks(nFromYear, nFromMOY, nFromDOM, 2);
				break;
			case Konstant.JOB_PAY_LENGTH_QUADWEEKLY:
				alsPiggyDates = Utils.calculatePiggyDatesUsingWeeks(nFromYear, nFromMOY, nFromDOM, 4);
				break;
			case Konstant.JOB_PAY_LENGTH_MONTHLY:
				alsPiggyDates = Utils.calculatePiggyDatesUsingMonths(nFromYear, nFromMOY, nFromDOM);
				break;
			}

			// Save new Piggy dates

			mPrefEditor.putString(Konstant.PREF_KEY_PIGGY_DATES_FOR + mnJobID, TextUtils.join(",", alsPiggyDates));
			mPrefEditor.commit();

			return null;
		}

		@Override
		protected void onProgressUpdate(Void... params)
		{
		}

		@Override
		protected void onPostExecute(Void param)
		{
			Utils.msaPiggyDates = Utils.getPiggyDatesMappedByJobID(ActivityJobsAdd.this, mPreferences);
		}
	}
}