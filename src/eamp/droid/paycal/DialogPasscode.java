/*
 * DialogPassocode
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DialogPasscode extends Dialog
{
	private Context mContext;

	private SharedPreferences mPreferences;
	private SharedPreferences.Editor mPrefEditor;
	
	public DialogPasscode(Context context)
	{
		super(context);
		mContext = context;
		this.setContentView(R.layout.dialog_passcode);
		
		mPreferences = mContext.getSharedPreferences(Konstant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		mPrefEditor = mPreferences.edit();
	}

	public void displayDialog(final boolean bSetPasscode)
	{
		final EditText etCurrentPasscode = (EditText) findViewById(R.id.et_current_passcode);
		final EditText etNewPasscode = (EditText) findViewById(R.id.et_new_passcode);
		final EditText etConfirmPasscode = (EditText) findViewById(R.id.et_confirm_passcode);
		
		final Button btnSavePasscode = (Button) findViewById(R.id.btn_save_passcode);
		
		if(bSetPasscode)
		{
			etCurrentPasscode.setVisibility(View.GONE);
			this.setTitle(mContext.getString(R.string.passcode_set));
		}
		else
		{
			this.setTitle(mContext.getString(R.string.passcode_change));
		}

		etNewPasscode.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
			}
			
			@Override
			public void afterTextChanged(Editable s)
			{
				if(s.toString().trim().equals(Konstant.PREF_DEF_PASSCODE))
				{
					btnSavePasscode.setEnabled(false);
				}
				else
				{
					btnSavePasscode.setEnabled(true);
				}
			}
		});
		
		btnSavePasscode.setEnabled(false);
		btnSavePasscode.setOnClickListener(new android.view.View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String strNewPasscode = etNewPasscode.getText().toString();
				String strconfirmPasscode = etConfirmPasscode.getText().toString();
				if(bSetPasscode)
				{
					if(strNewPasscode.equals(strconfirmPasscode))
					{
						mPrefEditor.putString(Konstant.PREF_KEY_PASSCODE, strNewPasscode);
						mPrefEditor.commit();
						dismiss();
					}
					else
					{
						Toast.makeText(mContext, mContext.getString(R.string.msg_passcode_mismatch), Toast.LENGTH_SHORT).show();
					}
				}
				else
				{
					String strCurrentPasscode = etCurrentPasscode.getText().toString();
					String strSavedPasscode = mPreferences.getString(Konstant.PREF_KEY_PASSCODE, Konstant.PREF_DEF_PASSCODE);
					if(strCurrentPasscode.equals(strSavedPasscode))
					{
						if(strNewPasscode.equals(strconfirmPasscode))
						{
							mPrefEditor.putString(Konstant.PREF_KEY_PASSCODE, strNewPasscode);
							mPrefEditor.commit();
							dismiss();
						}
						else
						{
							Toast.makeText(mContext, mContext.getString(R.string.msg_passcode_mismatch), Toast.LENGTH_SHORT).show();
						}
					}
					else
					{
						Toast.makeText(mContext, mContext.getString(R.string.msg_passcode_wrong), Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
		
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		getWindow().setAttributes(lp);
		show();
	}
}