/*
 * FragmentTutorial
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class FragmentTutorial extends Fragment
{
	public static final String ARGUMENT_PAGENUMBER = "page";

	private static Context mContext;
	
	private int mPageNumber;

	private static int mnActivityLaunchFrom;
	
	private ImageView mivSample;
	private TextView mtvTitle;
	private TextView mtvDesc;
	private TextView mtvPremium;
	private ImageButton mibGotIt;

	private SharedPreferences mPreferences;
	private SharedPreferences.Editor mPrefEditor;

	public FragmentTutorial()
	{
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		mPageNumber = getArguments().getInt(ARGUMENT_PAGENUMBER);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_tutorial, container, false);

		mPreferences = mContext.getSharedPreferences(Konstant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		mPrefEditor = mPreferences.edit();
		
		mivSample = ((ImageView) rootView.findViewById(R.id.iv_tutorial));
		
		mtvTitle = ((TextView) rootView.findViewById(R.id.tv_ft_title));
		mtvDesc = ((TextView) rootView.findViewById(R.id.tv_ft_desc));
		mtvPremium = ((TextView) rootView.findViewById(R.id.tv_ft_premium));
		
		mibGotIt = ((ImageButton) rootView.findViewById(R.id.ib_ft_gotit));
		mibGotIt.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mPrefEditor.putBoolean(Konstant.PREF_KEY_IS_TUTORIAL_DONE, true);
				mPrefEditor.commit();
				
				switch(mnActivityLaunchFrom)
				{
				case Konstant.ACTIVITY_SPLASH:
					getActivity().finish();
					Intent intentHome = new Intent(getActivity(), ActivityHome.class);
					startActivity(intentHome);
					break;
				case Konstant.ACTIVITY_SETTINGS:
					getActivity().finish();
				}
			}
		});
		
		switch(mPageNumber)
		{
		case 0:
			mivSample.setImageResource(R.drawable.tutorial_01);
			mtvTitle.setText(mContext.getString(R.string.title_01));
			mtvDesc.setText(mContext.getString(R.string.desc_01));
			mtvPremium.setVisibility(View.VISIBLE);
			mibGotIt.setVisibility(View.GONE);
			break;
		case 1:
			mivSample.setImageResource(R.drawable.tutorial_02);
			mtvTitle.setText(mContext.getString(R.string.title_02));
			mtvDesc.setText(mContext.getString(R.string.desc_02));
			mtvPremium.setVisibility(View.GONE);
			mibGotIt.setVisibility(View.GONE);
			break;
		case 2:
			mivSample.setImageResource(R.drawable.tutorial_03);
			mtvTitle.setText(mContext.getString(R.string.title_03));
			mtvDesc.setText(mContext.getString(R.string.desc_03));
			mtvPremium.setVisibility(View.GONE);
			mibGotIt.setVisibility(View.GONE);
			break;
		case 3:
			mivSample.setImageResource(R.drawable.tutorial_04);
			mtvTitle.setText(mContext.getString(R.string.title_04));
			mtvDesc.setText(mContext.getString(R.string.desc_04));
			mtvPremium.setVisibility(View.VISIBLE);
			mibGotIt.setVisibility(View.VISIBLE);
			break;
		}
		
		return rootView;
	}
	
	public static Fragment createFragment(int pageNumber, Context context, int nActivityLaunchFrom)
	{
		FragmentTutorial fragment = new FragmentTutorial();
		mContext = context;
		
		mnActivityLaunchFrom = nActivityLaunchFrom;
		
		Bundle args = new Bundle();
		args.putInt(ARGUMENT_PAGENUMBER, pageNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public int getPageNumber()
	{
		return mPageNumber;
	}
}
