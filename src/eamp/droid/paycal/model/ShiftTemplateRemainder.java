/*
 * ShiftTemplateRemainder
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal.model;

public class ShiftTemplateRemainder
{
	private int mnID;
	private int mnShiftTemplateID;
	private int mnRemainderTimeIndex;

	public ShiftTemplateRemainder()
	{
		mnID = 0;
		mnShiftTemplateID = 0;
		mnRemainderTimeIndex = 0;
	}
	
	public int getID()
	{
		return mnID;
	}

	public void setID(int nID)
	{
		this.mnID = nID;
	}

	public int getShiftTemplateID()
	{
		return mnShiftTemplateID;
	}

	public void setShiftTemplateID(int nShiftTemplateID)
	{
		this.mnShiftTemplateID = nShiftTemplateID;
	}

	public int getRemainderTimeIndex()
	{
		return mnRemainderTimeIndex;
	}

	public void setRemainderTimeIndex(int nRemainderTimeIndex)
	{
		this.mnRemainderTimeIndex = nRemainderTimeIndex;
	}
}
