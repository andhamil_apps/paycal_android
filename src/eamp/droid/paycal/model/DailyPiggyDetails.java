/*
 * DailyPiggyDetails
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal.model;

import java.util.ArrayList;

public class DailyPiggyDetails
{
	private int mnPaydayYear;
	private int mnPaydayMOY;
	private int mnPaydayDOM;

	private ArrayList<Integer> malnJobIDs;

	private ArrayList<Double> maldDeductionRates;
	private ArrayList<Double> maldDeductions;
	private ArrayList<Double> maldTips;

	private ArrayList<String> malsJobNames;

	private ArrayList<Integer> malnRegularMinutes;
	private ArrayList<Integer> malnSpecialMinutes;
	private ArrayList<Integer> malnOvertimeMinutes;

	private ArrayList<Double> maldRegularAmounts;
	private ArrayList<Double> maldSpecialAmounts;
	private ArrayList<Double> maldOvertimeAmounts;

	public int getPaydayYear()
	{
		return mnPaydayYear;
	}

	public void setPaydayYear(int nPaydayYear)
	{
		this.mnPaydayYear = nPaydayYear;
	}

	public int getPaydayMOY()
	{
		return mnPaydayMOY;
	}

	public void setPaydayMOY(int nPaydayMOY)
	{
		this.mnPaydayMOY = nPaydayMOY;
	}

	public int getPaydayDOM()
	{
		return mnPaydayDOM;
	}

	public void setPaydayDOM(int nPaydayDOM)
	{
		this.mnPaydayDOM = nPaydayDOM;
	}

	public ArrayList<Integer> getJobIDs()
	{
		return malnJobIDs;
	}

	public void setJobIDs(ArrayList<Integer> alnJobIDs)
	{
		this.malnJobIDs = alnJobIDs;
	}

	public ArrayList<Double> getDeductionRates()
	{
		return maldDeductionRates;
	}

	public void setDeductionRates(ArrayList<Double> aldDeductionRates)
	{
		this.maldDeductionRates = aldDeductionRates;
	}
	
	public ArrayList<Double> getDeductions()
	{
		return maldDeductions;
	}

	public void setDeductions(ArrayList<Double> aldDeductions)
	{
		this.maldDeductions = aldDeductions;
	}

	public ArrayList<Double> getTips()
	{
		return maldTips;
	}

	public void setTips(ArrayList<Double> maldTips)
	{
		this.maldTips = maldTips;
	}

	public ArrayList<String> getJobNames()
	{
		return malsJobNames;
	}

	public void setJobNames(ArrayList<String> alsJobNames)
	{
		this.malsJobNames = alsJobNames;
	}

	public ArrayList<Integer> getRegularMinutes()
	{
		return malnRegularMinutes;
	}

	public void setRegularMinutes(ArrayList<Integer> alnRegularMinutes)
	{
		this.malnRegularMinutes = alnRegularMinutes;
	}

	public ArrayList<Integer> getSpecialMinutes()
	{
		return malnSpecialMinutes;
	}

	public void setSpecialMinutes(ArrayList<Integer> alnSpecialMinutes)
	{
		this.malnSpecialMinutes = alnSpecialMinutes;
	}

	public ArrayList<Integer> getOvertimeMinutes()
	{
		return malnOvertimeMinutes;
	}

	public void setOvertimeMinutes(ArrayList<Integer> alnOvertimeMinutes)
	{
		this.malnOvertimeMinutes = alnOvertimeMinutes;
	}

	public ArrayList<Double> getRegularAmounts()
	{
		return maldRegularAmounts;
	}

	public void setRegularAmounts(ArrayList<Double> aldRegularAmounts)
	{
		this.maldRegularAmounts = aldRegularAmounts;
	}

	public ArrayList<Double> getSpecialAmounts()
	{
		return maldSpecialAmounts;
	}

	public void setSpecialAmounts(ArrayList<Double> aldSpecialAmounts)
	{
		this.maldSpecialAmounts = aldSpecialAmounts;
	}

	public ArrayList<Double> getOvertimeAmounts()
	{
		return maldOvertimeAmounts;
	}

	public void setOvertimeAmounts(ArrayList<Double> aldOvertimeAmounts)
	{
		this.maldOvertimeAmounts = aldOvertimeAmounts;
	}
}