/*
 * ShiftInstanceReminder
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal.model;

public class ShiftInstanceReminder
{
	private int mnID;
	private int mnShiftInstanceID;
	private int mnReminderTimeIndex;

	public ShiftInstanceReminder()
	{
		mnID = 0;
		mnShiftInstanceID = 0;
		mnReminderTimeIndex = 0;
	}

	public int getID()
	{
		return mnID;
	}

	public void setID(int nID)
	{
		this.mnID = nID;
	}

	public int getShiftInstanceID()
	{
		return mnShiftInstanceID;
	}

	public void setShiftInstanceID(int nShiftInstanceID)
	{
		this.mnShiftInstanceID = nShiftInstanceID;
	}

	public int getRemainderTimeIndex()
	{
		return mnReminderTimeIndex;
	}

	public void setRemainderTimeIndex(int nRemainderTimeIndex)
	{
		this.mnReminderTimeIndex = nRemainderTimeIndex;
	}
}
