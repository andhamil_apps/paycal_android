/*
 * ShiftInstanceContinuing
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal.model;

public class ShiftInstanceContinuing
{
	private int mnID;

	private int mnShiftInstanceID;
	private int mnJobID;
	private int mnColor;

	private int mnDateYear;
	private int mnDateMOY;
	private int mnDateDOM;

	private int mnToHour;
	private int mnToMinute;

	private double mdAmount;
	
	public int getID()
	{
		return mnID;
	}

	public void setID(int nID)
	{
		this.mnID = nID;
	}

	public int getShiftInstanceID()
	{
		return mnShiftInstanceID;
	}

	public void setShiftInstanceID(int nShiftInstanceID)
	{
		this.mnShiftInstanceID = nShiftInstanceID;
	}

	public int getJobID()
	{
		return mnJobID;
	}

	public void setJobID(int nJobID)
	{
		this.mnJobID = nJobID;
	}

	public int getColor()
	{
		return mnColor;
	}

	public void setColor(int nColor)
	{
		this.mnColor = nColor;
	}

	public int getDateYear()
	{
		return mnDateYear;
	}

	public void setDateYear(int nDateYear)
	{
		this.mnDateYear = nDateYear;
	}

	public int getDateMOY()
	{
		return mnDateMOY;
	}

	public void setDateMOY(int nDateMOY)
	{
		this.mnDateMOY = nDateMOY;
	}

	public int getDateDOM()
	{
		return mnDateDOM;
	}

	public void setDateDOM(int nDateDOM)
	{
		this.mnDateDOM = nDateDOM;
	}

	public int getToHour()
	{
		return mnToHour;
	}

	public void setToHour(int nToHour)
	{
		this.mnToHour = nToHour;
	}

	public int getToMinute()
	{
		return mnToMinute;
	}

	public void setToMinute(int nToMinute)
	{
		this.mnToMinute = nToMinute;
	}

	public double getAmount()
	{
		return mdAmount;
	}

	public void setAmount(double dAmount)
	{
		this.mdAmount = dAmount;
	}
}
