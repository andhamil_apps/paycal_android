/*
 * Job
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal.model;

import eamp.droid.paycal.Konstant;

public class Job
{
	private int mnID;

	private String mstrName;
	private int mnColor;

	private double mdPay;
	private int mnPayUnit;

	private int mnPayBeganYear;
	private int mnPayBeganMOY;
	private int mnPayBeganDOM;
	private int mnPayLength;
	private int mnPayDayYear;
	private int mnPayDayMOY;
	private int mnPayDayDOM;
	private double mdPayDeduction;

	private double mdOvertimeRate;
	private int mnOTMinutes;
	private int mnOvertimeUnit;
	private boolean mbOvertimeOn;
	
	public Job()
	{
		mnID = 0;

		mstrName = Konstant.DEFAULT_JOB_NAME;
		mnColor = Konstant.DEFAULT_JOB_COLOR;

		mdPay = Konstant.DEFAULT_JOB_PAY;
		mnPayUnit = Konstant.DEFAULT_JOB_PAY_UNIT;

		mnPayBeganYear = Konstant.DEFAULT_JOB_PAY_BEGAN_YEAR;
		mnPayBeganMOY = Konstant.DEFAULT_JOB_PAY_BEGAN_MOY;
		mnPayBeganDOM = Konstant.DEFAULT_JOB_PAY_BEGAN_DOM;
		mnPayLength = Konstant.DEFAULT_JOB_PAY_LENGTH;
		mnPayDayYear = Konstant.DEFAULT_JOB_PAY_DAY_YEAR;
		mnPayDayMOY = Konstant.DEFAULT_JOB_PAY_DAY_MOY;
		mnPayDayDOM = Konstant.DEFAULT_JOB_PAY_DAY_DOM;
		mdPayDeduction = Konstant.DEFAULT_JOB_PAY_DEDUCTION;

		mdOvertimeRate = Konstant.DEFAULT_JOB_OVERTIME_RATE;
		mnOTMinutes = Konstant.DEFAULT_JOB_OT_HRS;
		mnOvertimeUnit = Konstant.DEFAULT_JOB_OVERTIME_UNIT;
	}

	public int getID()
	{
		return mnID;
	}

	public String getName()
	{
		return mstrName;
	}

	public int getColor()
	{
		return mnColor;
	}

	public double getPay()
	{
		return mdPay;
	}

	public int getPayUnit()
	{
		return mnPayUnit;
	}

	public int getPayBeganYear()
	{
		return mnPayBeganYear;
	}
	
	public int getPayBeganMOY()
	{
		return mnPayBeganMOY;
	}
	
	public int getPayBeganDOM()
	{
		return mnPayBeganDOM;
	}

	public int getPayLength()
	{
		return mnPayLength;
	}

	public int getPayDayYear()
	{
		return mnPayDayYear;
	}
	
	public int getPayDayMOY()
	{
		return mnPayDayMOY;
	}
	
	public int getPayDayDOM()
	{
		return mnPayDayDOM;
	}

	public double getPayDeduction()
	{
		return mdPayDeduction;
	}

	public double getOvertimeRate()
	{
		return mdOvertimeRate;
	}

	public int getOTMinutes()
	{
		return mnOTMinutes;
	}

	public int getOvertimeUnit()
	{
		return mnOvertimeUnit;
	}

	public void setID(int nID)
	{
		mnID = nID;
	}

	public void setName(String strName)
	{
		mstrName = strName;
	}

	public void setColor(int nColor)
	{
		mnColor = nColor;
	}

	public void setPay(double dPay)
	{
		mdPay = dPay;
	}

	public void setPayUnit(int nPayUnit)
	{
		mnPayUnit = nPayUnit;
	}

	public void setPayBeganYear(int nPayBeganYear)
	{
		mnPayBeganYear = nPayBeganYear;
	}

	public void setPayBeganMOY(int nPayBeganMOY)
	{
		mnPayBeganMOY = nPayBeganMOY;
	}
	
	public void setPayBeganDOM(int nPayBeganDOM)
	{
		mnPayBeganDOM = nPayBeganDOM;
	}
	public void setPayLength(int nPayLength)
	{
		mnPayLength = nPayLength;
	}

	public void setPayDayYear(int nPayDayYear)
	{
		mnPayDayYear = nPayDayYear;
	}

	public void setPayDayMOY(int nPayDayMOY)
	{
		mnPayDayMOY = nPayDayMOY;
	}
	
	public void setPayDayDOM(int nPayDayDOM)
	{
		mnPayDayDOM = nPayDayDOM;
	}
	
	public void setPayDeduction(double dPayDeduction)
	{
		mdPayDeduction = dPayDeduction;
	}

	public void setOvertimeRate(double dOvertimeRate)
	{
		mdOvertimeRate = dOvertimeRate;
	}

	public void setOTMinutes(int nOTMinutes)
	{
		mnOTMinutes = nOTMinutes;
	}

	public void setOvertimeUnit(int nOvertimeUnit)
	{
		mnOvertimeUnit = nOvertimeUnit;
	}

	public boolean isOvertimeOn()
	{
		return mbOvertimeOn;
	}

	public void setOvertimeOn(boolean bOvertimeOn)
	{
		this.mbOvertimeOn = bOvertimeOn;
	}
}
