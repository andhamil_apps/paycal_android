/*
 * DailyTallyDetails
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal.model;

import java.util.ArrayList;

public class DailyTallyDetails
{
	private ArrayList<Integer> malnTallyIDs;
	private ArrayList<Integer> malnTallyCounts;
	private ArrayList<String> malsTallyTimeUnits;
	private ArrayList<Integer> malnNumberOfPaydays;
	private ArrayList<String> malsTallyAmounts;

	public ArrayList<Integer> getTallyIDs()
	{
		return malnTallyIDs;
	}

	public void setTallyIDs(ArrayList<Integer> alnTallyIDs)
	{
		this.malnTallyIDs = alnTallyIDs;
	}
	
	public ArrayList<Integer> getTallyCounts()
	{
		return malnTallyCounts;
	}

	public void setTallyCounts(ArrayList<Integer> alnTallyCounts)
	{
		this.malnTallyCounts = alnTallyCounts;
	}

	public ArrayList<String> getTallyTimeUnits()
	{
		return malsTallyTimeUnits;
	}

	public void setTallyTimeUnits(ArrayList<String> alsTallyTimeUnits)
	{
		this.malsTallyTimeUnits = alsTallyTimeUnits;
	}
	
	public ArrayList<Integer> getNumberOfPaydays()
	{
		return malnNumberOfPaydays;
	}

	public void setNumberOfPaydays(ArrayList<Integer> alnNumberOfPaydays)
	{
		this.malnNumberOfPaydays = alnNumberOfPaydays;
	}

	public ArrayList<String> getTallyAmounts()
	{
		return malsTallyAmounts;
	}

	public void setTallyAmounts(ArrayList<String> alsTallyAmounts)
	{
		this.malsTallyAmounts = alsTallyAmounts;
	}
}
