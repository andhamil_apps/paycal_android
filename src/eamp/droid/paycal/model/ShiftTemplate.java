/*
 * ShiftTemplate
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal.model;

public class ShiftTemplate
{
	private int mnID;

	private int mnJobID;
	private int mnColor;
	private int mnIcon;

	private int mnFromHour;
	private int mnFromMinute;

	private int mnToHour;
	private int mnToMinute;

	private int mnUnPaidBreak;
	private double mdSpecialWage;
	private double mdTip;

	private String mstrNotes;

	private int mnPaidMinutes;
	
	public ShiftTemplate()
	{
		mnID = 0;

		mnJobID = 0;
		mnColor = 0;
		mnIcon = 0;

		mnFromHour = 0;
		mnFromMinute = 0;
		mnToHour = 0;
		mnToMinute = 0;

		mnUnPaidBreak = 0;
		mdSpecialWage = 0;
		mdTip = 0;

		mstrNotes = "";
	}

	public int getID()
	{
		return mnID;
	}

	public void setID(int nID)
	{
		this.mnID = nID;
	}

	public int getJobID()
	{
		return mnJobID;
	}

	public void setJobID(int nJobID)
	{
		this.mnJobID = nJobID;
	}

	public int getColor()
	{
		return mnColor;
	}

	public void setColor(int nColor)
	{
		this.mnColor = nColor;
	}

	public int getIcon()
	{
		return mnIcon;
	}

	public void setIcon(int nIcon)
	{
		this.mnIcon = nIcon;
	}

	public int getFromHour()
	{
		return mnFromHour;
	}

	public void setFromHour(int nFromHour)
	{
		this.mnFromHour = nFromHour;
	}

	public int getFromMinute()
	{
		return mnFromMinute;
	}

	public void setFromMinute(int nFromMinute)
	{
		this.mnFromMinute = nFromMinute;
	}

	public int getToHour()
	{
		return mnToHour;
	}

	public void setToHour(int nToHour)
	{
		this.mnToHour = nToHour;
	}

	public int getToMinute()
	{
		return mnToMinute;
	}

	public void setToMinute(int nToMinute)
	{
		this.mnToMinute = nToMinute;
	}

	public int getUnPaidBreak()
	{
		return mnUnPaidBreak;
	}

	public void setUnPaidBreak(int nUnPaidBreak)
	{
		this.mnUnPaidBreak = nUnPaidBreak;
	}

	public double getSpecialWage()
	{
		return mdSpecialWage;
	}

	public void setSpecialWage(double dSpecialWage)
	{
		this.mdSpecialWage = dSpecialWage;
	}

	public double getTip()
	{
		return mdTip;
	}

	public void setTip(double dTip)
	{
		this.mdTip = dTip;
	}

	public String getNotes()
	{
		return mstrNotes;
	}

	public void setNotes(String strNotes)
	{
		this.mstrNotes = strNotes;
	}

	public int getPaidMinutes()
	{
		return mnPaidMinutes;
	}

	public void setPaidMinutes(int nPaidMinutes)
	{
		this.mnPaidMinutes = nPaidMinutes;
	}
}