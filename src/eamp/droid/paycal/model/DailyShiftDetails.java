/*
 * DailyShiftDetails
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal.model;

import java.util.ArrayList;

public class DailyShiftDetails
{
	private ArrayList<Integer> malnShiftInstanceIDs;

	private ArrayList<Integer> malnJobIDs;
	private ArrayList<String> malsJobNames;
	private ArrayList<Integer> malnColors;

	private ArrayList<Integer> malnFromHours;
	private ArrayList<Integer> malnFromMinutes;
	private ArrayList<Integer> malnToHours;
	private ArrayList<Integer> malnToMinutes;

	private ArrayList<Integer> malnRegularMinutes;
	private ArrayList<Integer> malnSpecialMinutes;
	private ArrayList<Integer> malnOvertimeMinutes;

	private ArrayList<Double> maldRegularAmounts;
	private ArrayList<Double> maldOvertimeAmounts;
	private ArrayList<Double> maldSpecialAmounts;

	private ArrayList<Double> maldTips;

	public DailyShiftDetails()
	{
		setShiftInstanceIDs(new ArrayList<Integer>());

		setJobIDs(new ArrayList<Integer>());
		setJobNames(new ArrayList<String>());
		setColors(new ArrayList<Integer>());

		setFromHours(new ArrayList<Integer>());
		setFromMinutes(new ArrayList<Integer>());
		setToHours(new ArrayList<Integer>());
		setToMinutes(new ArrayList<Integer>());

		setRegularMinutes(new ArrayList<Integer>());
		setSpecialMinutes(new ArrayList<Integer>());
		setOvertimeMinutes(new ArrayList<Integer>());

		setRegularAmounts(new ArrayList<Double>());
		setOvertimeAmounts(new ArrayList<Double>());
		setSpecialAmounts(new ArrayList<Double>());

		setTips(new ArrayList<Double>());
	}

	public ArrayList<Integer> getShiftInstanceIDs()
	{
		return malnShiftInstanceIDs;
	}

	public void setShiftInstanceIDs(ArrayList<Integer> alnShiftInstanceIDs)
	{
		this.malnShiftInstanceIDs = alnShiftInstanceIDs;
	}

	public ArrayList<Integer> getJobIDs()
	{
		return malnJobIDs;
	}

	public void setJobIDs(ArrayList<Integer> alnJobIDs)
	{
		this.malnJobIDs = alnJobIDs;
	}

	public ArrayList<String> getJobNames()
	{
		return malsJobNames;
	}

	public void setJobNames(ArrayList<String> alsJobNames)
	{
		this.malsJobNames = alsJobNames;
	}

	public ArrayList<Integer> getColors()
	{
		return malnColors;
	}

	public void setColors(ArrayList<Integer> alnColors)
	{
		this.malnColors = alnColors;
	}

	public ArrayList<Integer> getFromHours()
	{
		return malnFromHours;
	}

	public void setFromHours(ArrayList<Integer> alnFromHours)
	{
		this.malnFromHours = alnFromHours;
	}

	public ArrayList<Integer> getFromMinutes()
	{
		return malnFromMinutes;
	}

	public void setFromMinutes(ArrayList<Integer> alnFromMinutes)
	{
		this.malnFromMinutes = alnFromMinutes;
	}

	public ArrayList<Integer> getToHours()
	{
		return malnToHours;
	}

	public void setToHours(ArrayList<Integer> alnToHours)
	{
		this.malnToHours = alnToHours;
	}

	public ArrayList<Integer> getToMinutes()
	{
		return malnToMinutes;
	}

	public void setToMinutes(ArrayList<Integer> alnToMinutes)
	{
		this.malnToMinutes = alnToMinutes;
	}

	public ArrayList<Integer> getRegularMinutes()
	{
		return malnRegularMinutes;
	}

	public void setRegularMinutes(ArrayList<Integer> alnRegularMinutes)
	{
		this.malnRegularMinutes = alnRegularMinutes;
	}

	public ArrayList<Integer> getSpecialMinutes()
	{
		return malnSpecialMinutes;
	}

	public void setSpecialMinutes(ArrayList<Integer> alnSpecialMinutes)
	{
		this.malnSpecialMinutes = alnSpecialMinutes;
	}

	public ArrayList<Integer> getOvertimeMinutes()
	{
		return malnOvertimeMinutes;
	}

	public void setOvertimeMinutes(ArrayList<Integer> alnOvertimeMinutes)
	{
		this.malnOvertimeMinutes = alnOvertimeMinutes;
	}

	public ArrayList<Double> getRegularAmounts()
	{
		return maldRegularAmounts;
	}

	public void setRegularAmounts(ArrayList<Double> aldRegularAmounts)
	{
		this.maldRegularAmounts = aldRegularAmounts;
	}

	public ArrayList<Double> getOvertimeAmounts()
	{
		return maldOvertimeAmounts;
	}

	public void setOvertimeAmounts(ArrayList<Double> aldOvertimeAmounts)
	{
		this.maldOvertimeAmounts = aldOvertimeAmounts;
	}

	public ArrayList<Double> getSpecialAmounts()
	{
		return maldSpecialAmounts;
	}

	public void setSpecialAmounts(ArrayList<Double> aldSpecialAmounts)
	{
		this.maldSpecialAmounts = aldSpecialAmounts;
	}

	public ArrayList<Double> getTips()
	{
		return maldTips;
	}

	public void setTips(ArrayList<Double> aldTips)
	{
		this.maldTips = aldTips;
	}
}
