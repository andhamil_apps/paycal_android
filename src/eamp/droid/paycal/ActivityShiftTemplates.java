/*
 * ActivityShiftTemplates
 * 
 * @ author: Karthik Palanivelu
 * @ company: Andhamil Inc.
 * @ contact: karthik@andhamil.com
 * 
 * � PayCal Inc., 2013
 * Confidential and proprietary.
 */

package eamp.droid.paycal;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import eamp.droid.paycal.model.ShiftTemplate;

public class ActivityShiftTemplates extends Activity
{
	private static Context mContext;

	private static ListView mlvShiftTemplates;
	private static TextView mtvNoShiftTemplate;

	private ImageButton mibUpgrade;

	private AdapterShiftTemplates mShiftTemplatesAdapter;

	private SharedPreferences mPreferences;
	private SharedPreferences.Editor mPrefEditor;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shift_templates);

		ActionBar currentActionBar = getActionBar();
		currentActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		currentActionBar.setDisplayShowTitleEnabled(false);
		currentActionBar.setCustomView(R.layout.action_bar_items_1);

		TextView tvActivityTitle = (TextView) findViewById(R.id.tv_ab1_title);
		tvActivityTitle.setText(getString(R.string.edit_shift_templates));

		mContext = ActivityShiftTemplates.this;

		mPreferences = getSharedPreferences(Konstant.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		mPrefEditor = mPreferences.edit();

		mibUpgrade = (ImageButton) findViewById(R.id.ib_st_upgrade);
		mibUpgrade.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogUpgrade dialogUpgrade = new DialogUpgrade(ActivityShiftTemplates.this, ActivityShiftTemplates.this);
				dialogUpgrade.displayDialog();
			}
		});

		TextView tvAddShiftTemplate = (TextView) findViewById(R.id.tv_shifttempl_add_shifttempl);
		tvAddShiftTemplate.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				ArrayList<Integer> alnJobIDs = DBHelper.getInstance(ActivityShiftTemplates.this).getColIDsFromTabJobs();
				if (alnJobIDs.isEmpty())
				{
					Toast.makeText(ActivityShiftTemplates.this, ActivityShiftTemplates.this.getString(R.string.msg_si_no_job_error), Toast.LENGTH_LONG).show();
					
					Intent intent = new Intent(ActivityShiftTemplates.this, ActivityJobsAdd.class);
					startActivity(intent);
					
					return;
				}
				Intent intent = new Intent(ActivityShiftTemplates.this, ActivityShiftTemplatesAdd.class);
				startActivity(intent);
			}
		});

		ArrayList<ShiftTemplate> alShiftTemplates = DBHelper.getInstance(ActivityShiftTemplates.this).getShiftTemplates();
		mShiftTemplatesAdapter = new AdapterShiftTemplates(ActivityShiftTemplates.this, R.layout.list_item_shift_templates, mPreferences, alShiftTemplates);

		mlvShiftTemplates = (ListView) findViewById(R.id.lv_shifttempl);
		mlvShiftTemplates.setAdapter(mShiftTemplatesAdapter);

		mtvNoShiftTemplate = (TextView) findViewById(R.id.tv_shifttempl_no_shifttempl_found);

		if (alShiftTemplates.isEmpty())
		{
			mlvShiftTemplates.setVisibility(View.GONE);
			mtvNoShiftTemplate.setVisibility(View.VISIBLE);
		}
		else
		{
			mtvNoShiftTemplate.setVisibility(View.GONE);
			mlvShiftTemplates.setVisibility(View.VISIBLE);
		}

		Switch swtMilitaryTime = (Switch) findViewById(R.id.swt_shifttempl_military);
		swtMilitaryTime.setChecked(mPreferences.getBoolean(Konstant.PREF_KEY_MILITARY_TIME_ON, Konstant.PREF_DEF_MILITARY_TIME_ON));
		swtMilitaryTime.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				mPrefEditor.putBoolean(Konstant.PREF_KEY_MILITARY_TIME_ON, isChecked);
				mPrefEditor.commit();

				mShiftTemplatesAdapter.notifyDataSetChanged();
			}
		});

		checkIsAppPremium();
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		ArrayList<ShiftTemplate> alShiftTemplates = DBHelper.getInstance(ActivityShiftTemplates.this).getShiftTemplates();
		mShiftTemplatesAdapter.changeDataset(alShiftTemplates);

		checkIsAppPremium();
	}

	private void checkIsAppPremium()
	{
		boolean bIsPremium = mPreferences.getBoolean(Konstant.PREF_KEY_IS_APP_USER_PREMIUM, Konstant.PREF_DEF_IS_APP_USER_PREMIUM);
		if (bIsPremium)
		{
			mibUpgrade.setVisibility(View.GONE);
		}
		else
		{
			mibUpgrade.setVisibility(View.VISIBLE);
		}
	}

	public static void notifyShiftTemplatesChanged()
	{
		if ((mtvNoShiftTemplate != null) && (mlvShiftTemplates != null))
		{
			ArrayList<Integer> alnIDs = DBHelper.getInstance(mContext).getColIDsFromTabShiftTemplates();
			if (alnIDs.isEmpty())
			{
				mlvShiftTemplates.setVisibility(View.GONE);
				mtvNoShiftTemplate.setVisibility(View.VISIBLE);
			}
			else
			{
				mtvNoShiftTemplate.setVisibility(View.GONE);
				mlvShiftTemplates.setVisibility(View.VISIBLE);
			}
		}
	}
}